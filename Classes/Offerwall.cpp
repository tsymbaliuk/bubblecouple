#include "Offerwall.h"

#include "cocos2d.h"

static OfferwallInterface *current_interface = NULL;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

using namespace cocos2d;

#include "platform/android/jni/JniHelper.h"
#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_initJNI(JNIEnv*, jobject, jobject);
JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsNumber(JNIEnv*, jobject, int);
JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsProcessStarted(JNIEnv*, jobject);
JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsProcessFailed(JNIEnv*, jobject);

#ifdef __cplusplus
}
#endif

static jmethodID mid;
static jclass mClass;
static jobject mObject;

JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_initJNI(JNIEnv* env, jobject thiz, jobject weak_this)
{
  jclass clazz = env->GetObjectClass(thiz);
  mClass = (jclass)env->NewGlobalRef(clazz);
  mObject = env->NewGlobalRef(weak_this);
}

JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsNumber(JNIEnv*, jobject, int new_amount)
{
  if (current_interface) {
    current_interface->starsNumberUpdated(new_amount);
  }
}

JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsProcessStarted(JNIEnv*, jobject)
{
  if (current_interface) {
    current_interface->starsNumberUpdateStarted();
  }
}

JNIEXPORT void JNICALL Java_org_cocos2dx_bubblecouple_TapjoyHelper_updateTapjoyStarsProcessFailed(JNIEnv*, jobject)
{
  if (current_interface) {
    current_interface->starsNumberUpdateFailed();
  }
}

class TapjoyHelperEnv {
public:
  TapjoyHelperEnv() {
    m_isAttached = false;
    int status = JniHelper::getJavaVM()->GetEnv((void**)&m_env, JNI_VERSION_1_6);
    if (status < 0) {
      CCLOG("TapjoyHelperEnv: JavaVM::GetEnv failed, assuming native thread");
      status = JniHelper::getJavaVM()->AttachCurrentThread(&m_env, NULL);
      if (status < 0) {
        CCLOG("TapjoyHelperEnv: failed to attach current thread");
        return;
      }
      m_isAttached = true;
    }
    return;
  }
  ~TapjoyHelperEnv() {
    if (m_isAttached) JniHelper::getJavaVM()->DetachCurrentThread();
  }
  JNIEnv* operator-> () { return m_env; }
protected:
  bool m_isAttached;
  JNIEnv* m_env;
};

static void TapjoyHelper_showOfferwall()
{
  TapjoyHelperEnv env;
  mid = env->GetStaticMethodID(mClass, "nativeShowOfferwall", "()V");
  if (mid != NULL) env->CallStaticVoidMethod(mClass, mid);
}

#endif /* CC_PLATFORM_ANDROID */

void showOfferwall(OfferwallInterface *interface_) {
  current_interface = interface_;
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
  TapjoyHelper_showOfferwall();
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
  if (interface_) {
    interface_->starsNumberUpdateStarted();
    interface_->starsNumberUpdated(100);
  }
#endif
}