#include "bubbles.h"

#include "view/view_interface.h"

Bubbles::Bubbles(b2World *world,
                 const Level &level,
                 ViewInterface *view_interface, 
                 SoundsInterface *sounds_interface) :
  steps_since_action(0),
  balls_number(level.balls_number),
  ball_radius(3.0 / balls_number),
  level(level),
  world(world),
  balls_portion(0),
  view_interface(view_interface),
  sounds_interface(sounds_interface) {
  world->SetContactListener(this);
  // First bubbles row and next bubbles row
  createBubbles();
  createBubbles();
}

Bubbles::~Bubbles() {
  for (std::list<b2Body *>::iterator it = next_balls.begin();
       it != next_balls.end(); ++it) {
    destroyBubbleBody(*it);
  }
  for (std::list<b2Body *>::iterator it = balls.begin();
       it != balls.end(); ++it) {
    destroyBubbleBody(*it);
  }
}

bool Bubbles::areColorsIntersect(
    BubbleParameters::Color c1, BubbleParameters::Color c1d,
    BubbleParameters::Color c2, BubbleParameters::Color c2d) {
  return (c1 == c2 || c1 == c2d || c1d == c2 || c1d == c2d);
}

bool Bubbles::isBallColored(
  b2Body *ball, 
  BubbleParameters::Color color, BubbleParameters::Color double_color) {
  BubbleParameters *bp = static_cast<BubbleParameters *>(ball->GetUserData());
  if (bp) {
    return areColorsIntersect(color,double_color,bp->color,bp->double_color);
  }
  return false;
}

bool Bubbles::areBallsLink(b2Body *ball1, b2Body *ball2) {
  BubbleParameters *bp1 = static_cast<BubbleParameters *>(ball1->GetUserData());
  BubbleParameters *bp2 = static_cast<BubbleParameters *>(ball2->GetUserData());
  return bp1 && bp2 && areColorsIntersect(bp1->color,bp1->double_color,
                                          bp2->color,bp2->double_color);
}

bool Bubbles::areBallsBlackAndWhite( b2Body *ball1, b2Body *ball2 ) {
  BubbleParameters *bp1 = static_cast<BubbleParameters *>(ball1->GetUserData());
  BubbleParameters *bp2 = static_cast<BubbleParameters *>(ball2->GetUserData());
  return bp1 && bp2 && 
    (
      (bp1->color == BubbleParameters::BlackColor 
       && bp2->color == BubbleParameters::WhiteColor)
      || (bp1->color == BubbleParameters::WhiteColor 
          && bp2->color == BubbleParameters::BlackColor)
    );
}

void Bubbles::createBubbles() {
  if (hasNextBalls()) {
    steps_since_action = 0;
  }
  ++balls_portion;
  for (std::list<b2Body *>::iterator it = next_balls.begin();
       it != next_balls.end(); ++it) {
    BubbleParameters *bp =
      static_cast<BubbleParameters *>((*it)->GetUserData());
    bp->is_availible = true;
    (*it)->SetActive(true);
    if (view_interface) {
      view_interface->ballUpdated(bp,ViewInterface::Availability);
    }
  }
  balls.splice(balls.begin(),next_balls);
  if (level.balls.empty()) {
    if (level.type == Level::Campaign) {
      return;
    } else if (level.type == Level::Classic) {
      level = LevelManager::generateLevel(
        LevelManager::LevelParameters::classicLevelParameters());
      level.type = Level::Classic;
    } else if (level.type == Level::Insane) {
      int level_id = level.id;
      level = LevelManager::generateLevel(
        LevelManager::LevelParameters::insaneLevelParameters(level.id));
      level.id = level_id;
      level.type = Level::Insane;
    }
  }
  for (size_t i=0; i<b2Min(level.balls[0].size(),
                           static_cast<size_t>(balls_number)); ++i) {
    BubbleParameters bi = level.available_balls[level.balls[0][i]];
    bi.position = b2Vec2(sqrt(2.0f)*ball_radius*(balls_number-1-i),
                         sqrt(2.0f)*ball_radius*(balls_number+i));
    bi.angle = 0;
    bi.is_availible = false;
    b2Body *body = createBubble(bi);
    next_balls.push_back(body);
  }
  level.balls.erase(level.balls.begin(),level.balls.begin()+1);
}

void Bubbles::destroyBubbleBody(b2Body *bubble) {
  BubbleParameters *bp =
      static_cast<BubbleParameters *>(bubble->GetUserData());
  if (view_interface) {
    view_interface->ballDestroyed(bp);
  }
  world->DestroyBody(bubble);
  delete bp;
}

void Bubbles::exploseBubbles(BubbleParameters::Color color) {
  for (std::list<b2Body *>::iterator it = balls.begin(); it != balls.end();) {
    if (isBallColored(*it,color,color)) {
      exploseBubble(*it);
      destroyBubbleBody(*it);
      balls.remove(*it);
    } else {
      ++it;
    }
  }
}

void Bubbles::exploseBubble(b2Body *bubble) {
  steps_since_action = 0;
  for (std::list<b2Body *>::iterator it = balls.begin();
       it != balls.end(); ++it) {
    if (bubble != *it) {
      b2Vec2 normal = (*it)->GetPosition() - bubble->GetPosition();
      float length = normal.Normalize();
	    float power = 7*pow(getBallRadius(bubble),2);
        (*it)->ApplyLinearImpulse(power*5/length*normal,(*it)->GetPosition());
    }
  }
}

int Bubbles::destroyBubbleInPointIfAvailible(
  b2Vec2 point, int &bubbles_number) {
  bubbles_number = 0;
  int d_score = 0;
  b2Body *body = getBubbleAtPoint(point);
  if (body) {
    std::set<b2Body *> contacted;
    getSameColorContacted(body,contacted);
    if (contacted.size() >= 2) {
      bubbles_number = contacted.size();
      d_score = pow(static_cast<float>(contacted.size()-1),2);
      if (isBallColored(
        body,BubbleParameters::BlackColor,BubbleParameters::BlackColor)) {
        d_score *= 10; // Black balls are more expensive
      }
      int become_smaller_count = 0, total_count = 0;
      float exploded_mass = 0;
      for (std::set<b2Body *>::iterator it = contacted.begin();
           it != contacted.end(); ++it) {
        BubbleParameters *bp =
          static_cast<BubbleParameters *>((*it)->GetUserData());
        total_count++;
        if (bp->is_exploding) {
          exploded_mass += (*it)->GetMass();
          total_count--;
          exploseBubble(*it);
        }
        if (bp->order > 1) {
          bp->order--;
          become_smaller_count++;
          total_count--;
          bp->position = (*it)->GetPosition();
          bp->angle = (*it)->GetAngle();
          bp->radius /= 1.3f;
          b2Body *bubble = createBubble(*bp);
          bubble->SetLinearVelocity((*it)->GetLinearVelocity());
          balls.push_back(bubble);
        }
        destroyBubbleBody(*it);
        balls.remove(*it);
      }
      if (sounds_interface) {
        if (become_smaller_count) {
          sounds_interface->bubbleBecomeSmaller();
        }
        if (exploded_mass) {
          sounds_interface->bubbleExploded();
        }
        if (total_count) {
          sounds_interface->bubbleDisappeared();
        }
      }
    } else {
      if (sounds_interface && contacted.size() == 1) {
        sounds_interface->aloneBubbleClicked(
          static_cast<BubbleParameters *>((*contacted.begin())->GetUserData()));
      }
    }
  }
  if (d_score != 0) {
    clicks.push_back(point);
    score += d_score;
  }
  return d_score;
}

bool Bubbles::hasEmptySpace() {
  static const  float epsilon = 0.01f; // fluctuation error
  for (int i=0; i<balls_number; ++i) {
  b2Vec2 center(sqrt(2.0)*ball_radius*(balls_number-1-i),
                sqrt(2.0)*ball_radius*(balls_number+i));
    for (std::list<b2Body *>::iterator it = balls.begin();
         it != balls.end(); ++it) {
      if (((*it)->GetPosition()-center).Length() + epsilon <
                                      ball_radius + getBallRadius(*it)) {
        return false;
      }
    }
  }
  return true;
}

bool Bubbles::hasMoves() {
  for (std::list<b2Body *>::const_iterator it = balls.begin();
       it != balls.end(); ++it) {
    for (b2ContactEdge* edge=(*it)->GetContactList(); edge; edge = edge->next) {
      if (edge->contact->IsTouching()) {
        b2Body *other = edge->contact->GetFixtureA()->GetBody();
        if (other == *it) {
          other = edge->contact->GetFixtureB()->GetBody();
        }
        if (areBallsLink(other,*it)) {
          if (other->GetType() == b2_dynamicBody) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

bool Bubbles::hasActivity() {
  if (steps_since_action < 30) {
    return true;
  }
  for (std::list<b2Body *>::iterator it = balls.begin();
       it != balls.end(); ++it) {
    if ((*it)->GetLinearVelocity().LengthSquared() > 0.0001f) {
      // return true if one of the bodies isn't sleeping
      return true;
    }
  }
  // return false if all bodies are sleeping
  return false;
}

void Bubbles::step(float dt) {
  if (!balls_to_explode.empty()) {
    for (std::set<b2Body *>::iterator it = balls_to_explode.begin();
      it != balls_to_explode.end(); ++it) {
      BubbleParameters *bp =
        static_cast<BubbleParameters *>((*it)->GetUserData());
      bp->is_exploding = true; // For view purposes
      exploseBubble(*it);
      destroyBubbleBody(*it);
      balls.remove(*it);
    }
    balls_to_explode.clear();
    if (sounds_interface) {
      sounds_interface->bubbleExploded();
    }
  }

  for (std::list<b2Body *>::iterator it = balls.begin();
    it != balls.end(); ++it) {
      BubbleParameters *bp =
        static_cast<BubbleParameters *>((*it)->GetUserData());
      if (bp->charge_density != 0) {
        std::list<b2Body *>::iterator jt = it;
        ++jt;
        for ( ; jt != balls.end(); ++jt) {
          BubbleParameters *bp_other =
            static_cast<BubbleParameters *>((*jt)->GetUserData());
          if (bp_other->charge_density != 0) {
            b2Vec2 normal = (*it)->GetPosition() - (*jt)->GetPosition();
            float length = normal.Normalize();
            float power = 5 * bp->radius * bp_other->radius / length;
            power *= power * bp->charge_density * bp_other->charge_density;
            (*it)->ApplyForceToCenter(-power*normal);
            (*jt)->ApplyForceToCenter(power*normal);
          }
        }
      }
  }

  const int32 velocityIterations = 6;
  const int32 positionIterations = 2;
  world->Step(dt, velocityIterations, positionIterations);
  for (std::list<b2Body *>::iterator it = balls.begin();
       it != balls.end(); ++it) {
    BubbleParameters *bp =
        static_cast<BubbleParameters *>((*it)->GetUserData());
    bp->position = (*it)->GetPosition();
    bp->angle = (*it)->GetAngle();
  }
  ++steps_since_action;
}

void Bubbles::getSameColorContacted(b2Body *body,
                                    std::set<b2Body *> &contacted) {
  contacted.insert(body);
  for (b2ContactEdge* edge = body->GetContactList(); edge; edge = edge->next) {
    if (edge->contact->IsTouching()) {
      b2Body *other = edge->contact->GetFixtureA()->GetBody();
      if (other == body) {
        other = edge->contact->GetFixtureB()->GetBody();
      }
      if (areBallsLink(other,body)) {
        if (other->GetType() == b2_dynamicBody) {
          if (contacted.find(other) == contacted.end()) {
            getSameColorContacted(other,contacted);
          }
        }
      }
    }
  }
}

b2Body *Bubbles::createBubble(const BubbleParameters &bp) {
  b2CircleShape shape;
  shape.m_radius = bp.radius;
  b2FixtureDef fixture_def;
  fixture_def.shape = &shape;
  fixture_def.friction = bp.friction;
  fixture_def.density = bp.density;
  if (bp.is_rubber) {
    fixture_def.restitution = 0.9f;
  }
  b2BodyDef body_def;
  if (bp.charge_density) {
    body_def.linearDamping = 1;
    body_def.angularDamping = 1;
  }
  body_def.type = b2_dynamicBody;
  body_def.position = bp.position;
  b2Body *body = world->CreateBody(&body_def);
  body->CreateFixture(&fixture_def);
  body->SetActive(bp.is_availible);
  BubbleParameters *bp_ptr = new BubbleParameters(bp);
  body->SetUserData(bp_ptr);
  if (view_interface) {
    view_interface->ballCreated(bp_ptr);
  }
  return body;
}

float Bubbles::getBallRadius(b2Body *ball) const {
  return static_cast<BubbleParameters *>(ball->GetUserData())->radius;
}

b2Body *Bubbles::getBubbleAtPoint(b2Vec2 point) const {
  for (std::list<b2Body *>::const_iterator it = balls.begin();
       it != balls.end(); ++it) {
    if (((*it)->GetPosition()
         - b2Vec2(point.x,point.y)).Length() < getBallRadius(*it)) {
      return *it;
    }
  }
  return NULL;
}

void Bubbles::BeginContact(b2Contact *contact) {
  b2Body* body1 = contact->GetFixtureA()->GetBody();
  b2Body* body2 = contact->GetFixtureB()->GetBody();
  if (sounds_interface) {
    BubbleParameters *bp1 = 
      static_cast<BubbleParameters *>(body1->GetUserData());
    BubbleParameters *bp2 = 
      static_cast<BubbleParameters *>(body2->GetUserData());
    if (!bp1) {
      sounds_interface->bubbleTouchedGround(
        b2Min(body2->GetLinearVelocity().Length()/10,1.0f),bp2->is_rubber);
    } else if (!bp2) {
      sounds_interface->bubbleTouchedGround(
        b2Min(body1->GetLinearVelocity().Length()/10,1.0f),bp1->is_rubber);
    } else {
      sounds_interface->bubbleTouchedBubble(
          b2Min(
            (body1->GetLinearVelocity()-body2->GetLinearVelocity()).Length()/10,
            1.0f),
          bp1->is_rubber || bp2->is_rubber
        );
    }
  }
  if (view_interface) {
    if (areBallsBlackAndWhite(body1,body2)) {
      balls_to_explode.insert(body1);
      balls_to_explode.insert(body2);
    } else if (areBallsLink(body1,body2)) {
      view_interface->sparkCreated(
            Spark(static_cast<BubbleParameters *>(body1->GetUserData()),
                  static_cast<BubbleParameters *>(body2->GetUserData())));
    }
  }
}

void Bubbles::EndContact(b2Contact *contact) {
  if (view_interface) {
    b2Body* body1 = contact->GetFixtureA()->GetBody();
    b2Body* body2 = contact->GetFixtureB()->GetBody();
    if (areBallsLink(body1,body2)) {
      view_interface->sparkDestroyed(
            Spark(static_cast<BubbleParameters *>(body1->GetUserData()),
                  static_cast<BubbleParameters *>(body2->GetUserData())));
    }
  }
}

Level::ResolveResult Bubbles::resolvePosition(int index) {
  std::vector<int> saved_positions(1,index), saved_positions_current;
  int step_i = 0;
  int win_positions = 0, loose_positions = 0;
  Level::ResolveResult rr;

  while (step_i < 20 && saved_positions.size()) {
    step_i++;
//    qDebug() << "step #" << step_i << "( "
//             << saved_positions.size() << "positions )";
    for (size_t p=0; p<saved_positions.size(); ++p) {
//      qDebug("Checking position %i of %i",p,saved_positions.size());
      restoreBubblesPosition(saved_positions[p]);
      // step is necessary to let box2d engine to create contacts
      // after the balls were created
      step(1.0f/60.0f);

      // Detecting groups of balls
      std::vector< std::set<b2Body *> > groups;
      for (std::list<b2Body *>::const_iterator it = balls.begin();
           it != balls.end(); ++it) {
        bool bubble_already_in_group = false;
        for (size_t j=0; j<groups.size(); ++j) {
          if (groups[j].count(*it) != 0) {
            bubble_already_in_group = true;
            break;
          }
        }
        if (bubble_already_in_group) continue;
        std::set<b2Body *> collided;
        getSameColorContacted(*it,collided);
        if (collided.size() > 1) {
          groups.push_back(collided);
        }
      }

      // Pointers to balls will become wrong after restoring position,
      // so saving positions of balls instead
      std::vector<b2Vec2> points_of_groups;
      for (size_t i=0; i<groups.size(); ++i) {
        points_of_groups.push_back((*groups[i].begin())->GetPosition());
      }

      for (size_t i=0; i<points_of_groups.size(); ++i) {
        restoreBubblesPosition(saved_positions[p]);
        step(1.0f/60.0f);
        int bubbles_destroyed;
        destroyBubbleInPointIfAvailible(points_of_groups[i],bubbles_destroyed);
        if (bubbles_destroyed <= 0) {
//          qDebug() << "Unexpected! Zero";
        }
        simulateUntilActive();
        if (balls.size() == 0) {
          win_positions++;
          if (rr.first_win_step == -1) {
            rr.first_win_step = step_i;
            rr.first_win_clicks = clicks;
            rr.max_score = score;
          }
        } else if (hasMoves()) {
          saved_positions_current.push_back(saveBubblesPosition());
        } else {
          if (balls.size() < rr.left_balls.size()) {
            rr.left_balls[balls.size()-1]++;
          }
          loose_positions++;
        }
      }
    }
//    qDebug() << "win positions:" << win_positions
//             << "loose positions:" << loose_positions
//             << "undefined positions:" << saved_positions_current.size();
    rr.loose_positions += loose_positions;
    loose_positions = 0;
    rr.win_positions += win_positions;
    win_positions = 0;
    saved_positions.swap(saved_positions_current);
    saved_positions_current.clear();
  }
  rr.undefined_positions = saved_positions_current.size();
//  qDebug() << "Total win positions:" << rr.win_positions
//           << "Total loose positions:" << rr.loose_positions
//           << "( 1 : " << rr.left_balls[0] << "; 2 : "
//           << rr.left_balls[1] << "; 3 : " << rr.left_balls[2] << ")"
//           << "undefined positions:" << rr.undefined_positions;
  return rr;
}

int Bubbles::saveBubblesPosition() {
  BubblesPosition position;
  position.score = score;
  position.clicks = clicks;
  for (std::list<b2Body *>::const_iterator it = balls.begin();
       it != balls.end(); ++it) {
    BubbleParameters bi =
        *static_cast<BubbleParameters *>((*it)->GetUserData());
    bi.position = (*it)->GetPosition();
    position.bubbles.push_back(bi);
  }
  for (std::list<b2Body *>::const_iterator it = next_balls.begin();
       it != next_balls.end(); ++it) {
    BubbleParameters bi =
        *static_cast<BubbleParameters *>((*it)->GetUserData());
    bi.position = (*it)->GetPosition();
    position.next_bubbles.push_back(bi);
  }
  position.level = level;
  positions.push_back(position);
  return positions.size()-1;
}

void Bubbles::simulateUntilActive() {
  step(1.0f/60.0f);
  while(hasActivity()) {
    for (int i=0; i<60; ++i) {
      step(1.0f/60.0f);
    }
    if (hasEmptySpace()) {
      createBubbles();
    }
  }
}

void Bubbles::clearBubblesPositions() {
  positions.clear();
}

void Bubbles::restoreBubblesPosition(int index) {
  // Clearing:
  for (std::list<b2Body *>::const_iterator it = balls.begin();
       it != balls.end(); ++it) {
    world->DestroyBody(*it);
  }
  balls.clear();
  for (std::list<b2Body *>::const_iterator it = next_balls.begin();
       it != next_balls.end(); ++it) {
    world->DestroyBody(*it);
  }
  next_balls.clear();
  // Creating:
  level = positions[index].level;
  for (size_t i=0; i<positions[index].bubbles.size(); ++i) {
    balls.push_back(createBubble(positions[index].bubbles[i]));
  }
  for (size_t i=0; i<positions[index].next_bubbles.size(); ++i) {
    b2Body *next_bubble = createBubble(positions[index].next_bubbles[i]);
    next_bubble->SetActive(false);
    next_balls.push_back(next_bubble);
  }
  // Restoring:
  score = positions[index].score;
  clicks = positions[index].clicks;
}

std::vector<Bubbles::BubblesPosition> Bubbles::positions;
