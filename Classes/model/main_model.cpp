#include "main_model.h"

#include "model/bonus_manager.h"
#include "view/view_interface.h"
#include "bubbles.h"
#include "game_progress.h"
#include "ground.h"

MainModel::MainModel() :
  paused(false), stopped(true), current_view_interface(NULL),
  current_sounds_interface(NULL), bubbles(NULL), bonus_manager(NULL),
  world(NULL), level_group_id(rand() % LEVEL_GROUPS_COUNT), level_id(-1),
  insane_current_level(0) {
  world = constructWorld();

  score_manager = new ScoreManager();
}

MainModel::~MainModel() {
  current_view_interface = NULL;
  current_sounds_interface = NULL;
  if (bubbles) {
    bubbles->clearViewInterface();
  }
  stopGame();
}

void MainModel::addScoreObserver(ScoreManager::Observer *observer) {
  score_manager->addObserver(observer);
}

Level::ResolveResult MainModel::resolveLevel(const Level &level) {
  b2World *world = MainModel::constructWorld();
  Bubbles *bubbles = new Bubbles(world,level,NULL,NULL);

  bubbles->simulateUntilActive();
  return bubbles->resolvePosition(bubbles->saveBubblesPosition());
}

void MainModel::removeScoreObserver(ScoreManager::Observer *observer) {
  score_manager->removeObserver(observer);
}

void MainModel::setViewInterface(ViewInterface *view_interface_) {
  current_view_interface = view_interface_;
}

void MainModel::setSoundsInterface( SoundsInterface *sounds_interface_ ) {
  current_sounds_interface = sounds_interface_;
}

void MainModel::mouseClicked(b2Vec2 pos) {
  if (!stopped && !paused) {
    int destroyed_bubbles_number = 0;
    int score =
        bubbles->destroyBubbleInPointIfAvailible(pos, destroyed_bubbles_number);
    if (score) {
      score_manager->increaseScore(score);
      is_active = true;
    }
    if (is_insane_mode && current_view_interface && level_id == -1) {
      decreaseInsaneBubblesNumber(destroyed_bubbles_number);
    }
  }
}

void MainModel::bonusPressed( Bonus type ) {
  if (bonus_manager && !stopped && !paused) {
    if (type == ExploserBonus) {
      decreaseInsaneBubblesNumber(bubbles->getBalls().size());
    }
    bonus_manager->handleBonus(type);
  }
}

void MainModel::bonusPicked( Bonus type ) {
  if (bonus_manager) {
    bonus_manager->bonusPicked(type);
  }
}

void MainModel::startClassic() {
  if (level_id == -1 && !is_insane_mode && paused) {
    resumeGame();
  } else {
    if (bonus_manager) {
      score_manager->removeObserver(bonus_manager);
      delete bonus_manager;
      bonus_manager = NULL;
    }

    Level level = LevelManager::generateLevel(
      LevelManager::LevelParameters::classicLevelParameters());
    level.type = Level::Classic;
    insane_current_level = 0;
    is_insane_mode = false;
    startLevel(level);
  }
}

void MainModel::startInsane() {
  if (level_id == -1 && is_insane_mode && paused && current_view_interface) {
    if (insane_left_bubbles == 0) {
      current_view_interface->gameFinished(insane_time_left,0);
    } else if (insane_time_left <= 0) {
      startInsaneImplicitly();
    } else {
      resumeGame();
    }
  } else {
    startInsaneImplicitly();
  }
}

void MainModel::startCampaign(int level_group, int level_number) {
  if (level_id == level_number && level_group_id == level_group && paused) {
    resumeGame();
  } else {
    insane_current_level = 0;
    if (bonus_manager) {
      score_manager->removeObserver(bonus_manager);
      delete bonus_manager;
      bonus_manager = NULL;
    }

    Level level = LevelManager::loadLevel(level_group,level_number);
    startLevel(level);
    if (current_view_interface && !level.new_info.empty()) {
      for (size_t i=0; i<level.new_info.size(); ++i) {
        current_view_interface->newInfoMessage(level.new_info[i]);
      }
    }
  }
}

void MainModel::skipAndStartNext() {
  if (level_id == -1 && is_insane_mode) {
    startInsaneImplicitly();
  } else if (level_group_id != -1) {
    if (level_id+1 < LEVELS_IN_GROUP) {
      stopGame();
      LevelProgress next_level_progress =
        GameProgress::getLevelProgress(level_group_id,level_id+1);
      next_level_progress.blocked = false;
      GameProgress::setLevelProgress(
        level_group_id,level_id+1,next_level_progress);
      startCampaign(level_group_id,level_id+1);
    }
  }
}

void MainModel::pauseGame() {
  score_manager->flush();
  paused = true;
}

void MainModel::resumeGame() {
  paused = false;
  if (current_view_interface) {
    current_view_interface->gameResumed();
  }
}

void MainModel::stopGame() {
  if (!stopped) {
    score_manager->flush();
    stopped = true;
    delete bubbles;
    bubbles = NULL;
    if (current_view_interface) {
      current_view_interface->worldChanged();
    }
  }
}

void MainModel::restartGame() {
  paused = false;
  if (level_id == -1) {
    if (is_insane_mode) {
      insane_current_level = 0;
      if (bonus_manager) {
        score_manager->removeObserver(bonus_manager);
        delete bonus_manager;
        bonus_manager = NULL;
      }
      startInsane();
    } else {
      startClassic();
    }
  } else {
    startCampaign(level_group_id,level_id);
  }
}

void MainModel::doStep(float dt) {
  if (!stopped && !paused) {
    newIteration();
    if (bonus_manager) {
      bonus_manager->step();
    }
    bubbles->step(dt);
    if (is_insane_mode && level_id == -1) {
      insane_time_left -= dt;
      if (current_view_interface) {
        if (insane_time_left < 0 ) {
          stopGame();
          current_view_interface->gameFinished(-1,-1);
          insane_current_level = 0;
        } else {
          current_view_interface->leftTimeChanged(insane_time_left);
        }
      }
    }
  }
  if (current_view_interface) {
    current_view_interface->worldChanged();
  }
}

b2World *MainModel::constructWorld() {
  b2World *world = new b2World(b2Vec2(0,-10));
  Ground::createGround(world);
  return world;
}

void MainModel::startLevel(const Level &level) {
  if (!stopped) {
    stopGame();
  }
  bubbles = 
    new Bubbles(world,level,current_view_interface,current_sounds_interface);
  if (level.type == Level::Insane) {
    if (!bonus_manager) {
      bonus_manager = new BonusManager(bubbles);
      score_manager->addObserver(bonus_manager);
    } else {
      bonus_manager->updateBubbles(bubbles);
    }
  }
  if (level.type == Level::Campaign) {
    level_group_id = level.group_id;
    level_id = level.id;
  } else {
    level_group_id = rand() % LEVEL_GROUPS_COUNT;
    level_id = -1;
  }

  stopped = false;
  is_active = true;
  resumeGame();

  levelChanged();
}

void MainModel::startInsaneImplicitly() {
  Level level = LevelManager::generateLevel(LevelManager::
    LevelParameters::insaneLevelParameters(insane_current_level));
  level.type = Level::Insane;
  is_insane_mode = true;
  insane_time_left = 60;
  insane_left_bubbles = 30+2*insane_current_level;
  if (insane_current_level%10 == 9) {
    insane_time_left *= 2;
    insane_left_bubbles *= 2;
  }
  level.id = insane_current_level;
  startLevel(level);
  current_view_interface->leftBubblesChanged(insane_left_bubbles);
}

void MainModel::newIteration() {
  static int step = 0;
  ++step;
  if (step % 60 == 0) {
    // this routine is too expensive and not so necessary
    // to execute it each step
    if (bubbles->hasNextBalls() && bubbles->hasEmptySpace()) {
      bubbles->createBubbles();
      is_active = true;
    } else {
      is_active = bubbles->hasActivity();
    }
    if (!is_active) {
      if (!bubbles->hasMoves()) {
        if (level_id == -1 && is_insane_mode && 
            bonus_manager && bonus_manager->hasBonuses()) {
          // That's not end
        } else {
          int balls_left = bubbles->getBalls().size();
          proceedGameEnd(balls_left);
        }
      }
    }
  }
}

void MainModel::proceedGameEnd(int balls_left) {
  if (level_id == -1 && is_insane_mode) {
    if (current_view_interface) {
      current_view_interface->gameFinished(-1,-1);
      insane_current_level = 0;
    }
  } else {
    int previous_stars_count = 0;
    if (level_id != -1 && balls_left <= 3) {
      LevelProgress level_progress =
          GameProgress::getLevelProgress(level_group_id,level_id);
      previous_stars_count = level_progress.star_count;
      int current_stars_count = 3 - balls_left;
      if (previous_stars_count < current_stars_count) {
        level_progress.star_count = current_stars_count;
        GameProgress::setLevelProgress(level_group_id,level_id,level_progress);
        if (level_id+1 < LEVELS_IN_GROUP) {
          LevelProgress next_level_progress =
            GameProgress::getLevelProgress(level_group_id,level_id+1);
          next_level_progress.blocked = false;
          GameProgress::setLevelProgress(
            level_group_id,level_id+1,next_level_progress);
        }
      }
    } else {
      previous_stars_count = -1;
    }
    if (current_view_interface) {
      current_view_interface->gameFinished(balls_left, previous_stars_count);
    }
  }
  pauseGame();
}

void MainModel::levelChanged() {
  if (current_view_interface) {
    if (level_id == -1 && is_insane_mode) {
      current_view_interface->
        levelChanged(level_group_id,1000 + insane_current_level);
    } else {
      current_view_interface->levelChanged(level_group_id,level_id);
    }
  }
}

void MainModel::decreaseInsaneBubblesNumber( int delta ) {
  insane_left_bubbles -= delta;
  if (insane_left_bubbles < 0) {
    insane_left_bubbles = 0;
  }
  current_view_interface->leftBubblesChanged(insane_left_bubbles);
  if (insane_left_bubbles == 0) {
    score_manager->increaseScore(insane_time_left);
    paused = true;
    current_view_interface->gameFinished(insane_time_left,0);
    ++insane_current_level;
  }
}
