#include "model/bonus_manager.h"

#include "model/bubbles.h"
#ifndef QT
#include "view/sounds_manager.h"
#endif

BonusManager::BonusManager( Bubbles *bubbles ) {
  for (int i=0; i<BonusesNumber; ++i) {
    time_to_end[i] = 0;
    available_bonuses[i] = false;
  }
  updateBubbles(bubbles);
}

void BonusManager::handleBonus( Bonus type ) {
  switch (type) {
    case ShufflerBonus: handleShuffler(); break;
    case EarthquakeBonus: handleEarthquake(); break;
    case ExploserBonus: handleExploser(); break;
    case BubbleRainBonus: handleBubbleRain(); break;
    case CreatorBonus: handleCreator(); break;
    case SplitterBonus: handleSplitter(); break;
    default: break;
  }
  available_bonuses[type] = false;
  if (bubbles->view_interface) {
    bubbles->view_interface->bonusNumberUpdated(available_bonuses);
  }
}

void BonusManager::bonusPicked( Bonus type ) {
  if (bubbles->view_interface) {
    if (!available_bonuses[type]) {
      available_bonuses[type] = true;
      bubbles->view_interface->bonusNumberUpdated(available_bonuses);
    }
  }
}

bool BonusManager::hasBonuses() const {
  for (int i=0; i<BonusesNumber; ++i) {
    if (available_bonuses[i]) return true;
  }
  return false;
}

void BonusManager::step() {
  if (time_to_end[EarthquakeBonus] > 0) {
    applyEarthquake();
  }
  if (time_to_end[CreatorBonus] % 30 == 1) {
    // Each 30 ticks
    applyCreator();
  }
  if (time_to_end[BubbleRainBonus] % 20 == 1) {
    // Each 20 ticks
    applyBubbleRain();
  }
  for (int i=0; i<BonusesNumber; ++i) {
    if (time_to_end[i] > 0) {
      --time_to_end[i];
    }
  }
}

void BonusManager::updateBubbles( Bubbles *bubbles_ ) {
  bubbles = bubbles_;
  if (bubbles->view_interface) {
    bubbles->view_interface->bonusNumberUpdated(available_bonuses);
  }
}

void BonusManager::handleShuffler() {
  applyShuffler();
}

void BonusManager::handleSplitter() {
  applySplitter();
}

void BonusManager::handleEarthquake() {
  time_to_end[EarthquakeBonus] = 200;
}

void BonusManager::handleExploser() {
  applyExploser();
}

void BonusManager::handleBubbleRain() {
  time_to_end[BubbleRainBonus] = 100;
}

void BonusManager::handleCreator() {
  time_to_end[CreatorBonus] = 200;
}

void BonusManager::applyShuffler() {
  for (std::list<b2Body *>::iterator it = bubbles->balls.begin();
       it != bubbles->balls.end(); ++it) {
    BubbleParameters *bp(static_cast<BubbleParameters *>((*it)->GetUserData()));
    if (!bp->isDouble() && 
        (bp->color >= BubbleParameters::RColor && 
         bp->color <= BubbleParameters::BColor)) {
      for (b2ContactEdge *c = (*it)->GetContactList(); c != NULL; c=c->next) {
        bubbles->EndContact(c->contact);
      }
      bp->color = 
        static_cast<BubbleParameters::Color>( (bp->color + 1 + rand()%2)%3 );
      bp->double_color = bp->color;
      if (bubbles->view_interface) {
        bubbles->view_interface->ballUpdated(bp,ViewInterface::Color);
      }
      for (b2ContactEdge *c = (*it)->GetContactList(); c != NULL; c=c->next) {
        if (c->contact->IsTouching()) {
          bubbles->BeginContact(c->contact);
        }
      }
    }
  }
}

void BonusManager::applySplitter() {
  std::list<b2Body *> bubbles_list = bubbles->balls;
  bubbles->balls.clear();
  for (std::list<b2Body *>::iterator it = bubbles_list.begin();
       it != bubbles_list.end(); ++it) {
    BubbleParameters *bp(static_cast<BubbleParameters *>((*it)->GetUserData()));
    bp->radius /= 1.3f;
    bp->position += b2Vec2(0,bp->radius/2);
    bubbles->balls.push_back(bubbles->createBubble(*bp));
    bp->position -= b2Vec2(0,bp->radius);
    bubbles->balls.push_back(bubbles->createBubble(*bp));
    bubbles->destroyBubbleBody(*it);
  }
}

void BonusManager::applyEarthquake() {
  for (std::list<b2Body *>::iterator it = bubbles->balls.begin();
       it != bubbles->balls.end(); ++it) {
    (*it)->ApplyLinearImpulse(b2Vec2(0.2f*(rand()%11-5),0.2f*(rand()%11-5)),
                              (*it)->GetPosition());
  }
}

void BonusManager::applyExploser() {
  for (std::list<b2Body *>::iterator it = bubbles->balls.begin();
       it != bubbles->balls.end(); ++it) {
    bubbles->destroyBubbleBody(*it);
  }
  bubbles->balls.clear();
}

void BonusManager::applyBubbleRain() {
  LevelManager::LevelParameters lp = 
    LevelManager::LevelParameters::classicLevelParameters();
  lp.columns_count = 1;
  lp.only_full_rows = true;
  Level level = LevelManager::generateLevel(lp);
  for (size_t i=0; i<level.balls.size(); ++i) {
    for (size_t j=0; j<level.balls[i].size(); ++j) {
      BubbleParameters bi = level.available_balls[level.balls[i][j]];
      bi.position = b2Vec2(rand()%11-5,j+10);
      bi.angle = 0;
      bi.is_availible = true;
      bubbles->balls.push_back(bubbles->createBubble(bi));
    }
  }
}

void BonusManager::applyCreator() {
  // Encapsulation broken! Hate singleton.
#ifndef QT
  SoundsManager::get().playSound(CreatorSound); 
#endif
  bubbles->createBubbles();
}

void BonusManager::onScoreChanged( int , int delta ) {
  if (bubbles->view_interface && delta > 0 && rand()%10 == 0) {
    
    int probability[BonusesNumber]; 
    probability[ShufflerBonus] = 100;
    probability[SplitterBonus] = 2;
    probability[EarthquakeBonus] = 30;
    probability[ExploserBonus] = 30;
    probability[BubbleRainBonus] = 10;
    probability[CreatorBonus] = 3;

    int total = 0;
    for (int i=0; i<BonusesNumber; ++i) {
      total += probability[i];
    }

    int r = rand()%total;
    for (int i=0; i<BonusesNumber; ++i) {
      r -= probability[i];
      if (r < 0) {
        bubbles->view_interface->bonusAppeared(static_cast<Bonus>(i));
        return;
      }
    }

  }
}
