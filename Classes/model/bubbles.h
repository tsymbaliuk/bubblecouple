#ifndef BUBBLES_H
#define BUBBLES_H

#include "Box2D/Box2D.h"
#include "level_manager.h"
#include <list>
#include <set>
#include <string>
#include <vector>

class ViewInterface;
class Bubbles : public b2ContactListener {
public:
  Bubbles(b2World *world, const Level &level, 
          ViewInterface *view_interface, SoundsInterface *sounds_interface);
  ~Bubbles();
  std::list<b2Body *> &getBalls() {return balls;}
  bool areColorsIntersect(
    BubbleParameters::Color c1, BubbleParameters::Color c1d,
    BubbleParameters::Color c2, BubbleParameters::Color c2d);
  bool isBallColored(
    b2Body *ball, 
    BubbleParameters::Color color, BubbleParameters::Color double_color);
  bool areBallsLink(b2Body *ball1, b2Body *ball2);
  bool areBallsBlackAndWhite(b2Body *ball1, b2Body *ball2);
  void createBubbles();
  // Doesn't removes from any containers, just destroys the body
  void destroyBubbleBody(b2Body *bubble);
  // Explose all the bubbles of the same color
  void exploseBubbles(BubbleParameters::Color color);
  // Explose bubble, but doesn't remove from any containers
  void exploseBubble(b2Body *bubble);
  int destroyBubbleInPointIfAvailible(b2Vec2 point, int &bubbles_number);
  // returns true if there is enough space to create new bubbles
  bool hasEmptySpace();
  bool hasNextBalls() {return !next_balls.empty();}
  bool hasMoves();
  bool hasActivity(); // returns true if at least one of the bubbles is active
  void step(float dt);
  // it is necessary for destruction when view_interface is already destroyed
  inline void clearViewInterface() {view_interface = NULL;}
  inline void clearSoundsInterface() {sounds_interface = NULL;}
private:
  void getSameColorContacted(b2Body *body, std::set<b2Body *> &contacted);
  b2Body *createBubble(const BubbleParameters &bp);
  float getBallRadius(b2Body *ball) const;
  b2Body *getBubbleAtPoint(b2Vec2 point) const;

  virtual void BeginContact(b2Contact* contact);
  virtual void EndContact(b2Contact* contact);

  int steps_since_action;
  const int balls_number; // number of balls in a row in level
  const float ball_radius; // default level ball radius
  Level level;
  std::list<b2Body *> balls;
  std::list<b2Body *> next_balls;
  // We can't destroy bodies during collision handling. It's a set due to 
  // situation in that bubble can be added here twice. 
  std::set<b2Body *> balls_to_explode;
  b2World *world;
  int balls_portion;
  std::vector<b2Vec2> clicks;
  int score;
  ViewInterface *view_interface;
  SoundsInterface *sounds_interface;
public:
  Level::ResolveResult resolvePosition(int index);
  struct BubblesPosition {
    BubblesPosition() : score(0) {}
    std::vector<BubbleParameters> bubbles;
    std::vector<BubbleParameters> next_bubbles;
    Level level;
    std::vector<b2Vec2> clicks;
    int score;
  };
  int saveBubblesPosition();
  void simulateUntilActive();
  static void clearBubblesPositions();
  void restoreBubblesPosition(int index);
  static std::vector<BubblesPosition> positions;

  friend class BonusManager;
};

#endif // BUBBLES_H
