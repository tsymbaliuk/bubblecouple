#ifndef SCORE_MANAGER_H
#define SCORE_MANAGER_H

#include <vector>

class ScoreManager {
public:
  ScoreManager();
  ScoreManager(int score_);
  void increaseScore(int delta);
  void flush();
  class Observer {
  public:
    Observer() {}
    virtual ~Observer() {}
    virtual void onScoreChanged(int new_score, int delta) = 0;
  };
  void addObserver(Observer *observer);
  void removeObserver(Observer *observer);
private:
  std::vector<Observer *> observers;
  int score;
};

#endif // SCORE_MANAGER_H
