#include "model/game_progress.h"

#include "model/level_manager.h"
#ifdef COCOS
#include "cocos2d.h"
#include "view/translator.h"
#include "view/global_info.h"
#endif

using namespace cocos2d;

LevelProgress LevelProgress::fromChar(char lp_char) {
  lp_char -= 'a';
  return LevelProgress(lp_char & 0x03, lp_char & 0x04 ? false : true);
}

char LevelProgress::toChar( LevelProgress lp ) {
  return 'a' + (lp.star_count & 0x03) + (lp.blocked ? 0 : 0x04);
}

LevelProgress GameProgress::getLevelProgress( size_t group, size_t level ) {
#ifdef COCOS
  if (!is_progress_loaded) {
    progress = loadProgress();
    is_progress_loaded = true;
  }
  LevelProgress to_return = LevelProgress::fromChar(progress[group][level]);
  if (group == 0 && level == 0 && to_return.blocked) {
    to_return.blocked = false;
  }
  return to_return;
#endif
#ifdef QT
  return LevelProgress();
#endif
}

void GameProgress::setLevelProgress(size_t group, size_t level,
                                    LevelProgress lp) {
#ifdef COCOS
  int total_stars = getTotalStarsCount();
  int stars_in_group = getBoxStarsCount(group);
  progress[group][level] = lp.toChar();
  char box_key[40];
  sprintf(box_key,"box_progress_%i",group);
  CCUserDefault::sharedUserDefault()->setStringForKey(box_key,progress[group]);
  starsNumberChanged(total_stars);
  starsInGroupNumberChanged(stars_in_group, group);
#endif
}

int GameProgress::getTotalStarsCount() {
  int total_stars = 0;
#ifdef COCOS
  for (size_t i=0; i<LEVEL_GROUPS_COUNT; ++i) {
    for (size_t j=0; j<LEVELS_IN_GROUP; ++j) {
      total_stars += getLevelProgress(i,j).star_count;
    }
  }
#endif
  return total_stars + getExternalStarsCount()
#ifdef ANDROID
    + getOfferwallStarsCount()
#endif
    ;
}

int GameProgress::getBoxStarsCount( size_t box_number ) {
  int total_stars = 0;
#ifdef COCOS
  for (size_t i=0; i<LEVELS_IN_GROUP; ++i) {
    total_stars += getLevelProgress(box_number,i).star_count;
  }
#endif
  return total_stars;
}

int GameProgress::getStarsForBoxUnlock( size_t box_number ) {
#ifdef ANDROID
  if (box_number == 1) {
    return 30-getTotalStarsCount();
  } else {
    return box_number*60-10-getTotalStarsCount();
  }
#else
  return box_number*40-10-getTotalStarsCount();
#endif
}

int GameProgress::getScore() {
#ifdef COCOS
  return CCUserDefault::sharedUserDefault()->getIntegerForKey( "score", 0 );
#endif
  return 0;
}

void GameProgress::setScore( int new_score ) {
#ifdef COCOS
  CCUserDefault::sharedUserDefault()->setIntegerForKey( "score", new_score );
#endif
}

int GameProgress::getInsaneLevelRecord() {
#ifdef COCOS
  return 
    CCUserDefault::sharedUserDefault()->getIntegerForKey( "insane_level", 0 );
#endif
  return 0;
}

void GameProgress::setInsaneLevelRecord( int level ) {
#ifdef COCOS
  CCUserDefault::sharedUserDefault()->setIntegerForKey( "insane_level", level );
#endif
}

std::vector<std::string> GameProgress::loadProgress() {
  std::vector<std::string> progress;
#ifdef COCOS
  for (size_t group=0; group<LEVEL_GROUPS_COUNT; ++group) {
    char box_key[40];
    sprintf(box_key,"box_progress_%i",group);
    std::string box_progress =
      CCUserDefault::sharedUserDefault()->getStringForKey(box_key);
    if (box_progress.length() <= LEVELS_IN_GROUP) {
      while (box_progress.length() != LEVELS_IN_GROUP) {
        box_progress += LevelProgress().toChar();
      }
    } else {
      box_progress.resize(LEVELS_IN_GROUP);
    }
    progress.push_back(box_progress);
  }
#endif
  return progress;
}

#ifdef ANDROID
int GameProgress::getOfferwallStarsCount() {
#ifdef COCOS
  return 
    CCUserDefault::sharedUserDefault()->getIntegerForKey("offerwall_stars", 0);
#endif
  return 0;
}

void GameProgress::setOfferwallStarsCount( int new_stars_count ) {
#ifdef COCOS
  CCUserDefault::sharedUserDefault()->setIntegerForKey( 
    "offerwall_stars", new_stars_count );
#endif
}
#endif

int GameProgress::getExternalStarsCount() {
#ifdef COCOS
  return
    CCUserDefault::sharedUserDefault()->getIntegerForKey( "external_stars", 0 );
#endif
  return 0;
}

void GameProgress::setExternalStarsCount( int new_stars_count ) {
#ifdef COCOS
  CCUserDefault::sharedUserDefault()->setIntegerForKey( 
    "external_stars", new_stars_count );
#endif
}

bool GameProgress::is_progress_loaded = false;

std::vector<std::string> GameProgress::progress;

void GameProgress::starsNumberChanged( int old_stars_count ) {
#ifdef COCOS
  int new_total_stars = getTotalStarsCount();
  char unlocked[100];
  if (old_stars_count < 30 && new_total_stars >= 30) {
    sprintf(unlocked,tr(LevelsUnlocked),tr(Town));
    GlobalInfo::get()->queueText(unlocked);
  } else if (old_stars_count < 70 && new_total_stars >= 70) {
    sprintf(unlocked,tr(LevelsUnlocked),tr(Mountains));
    GlobalInfo::get()->queueText(unlocked);
  }
#endif
}

void GameProgress::
  starsInGroupNumberChanged( int old_stars_in_group, size_t group ) {
#ifdef COCOS
  int new_stars_in_group = getBoxStarsCount(group);
  char unlocked[100];
  TrId unlocked_bubble_id = TotalVariables;
  switch(group) {
  case 0: 
    if (old_stars_in_group < LevelManager::stars_to_open_double_color_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_double_color_bubbles) {
        unlocked_bubble_id = DoubleBubble;
    } else if (old_stars_in_group < LevelManager::stars_to_open_rubber_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_rubber_bubbles) {
        unlocked_bubble_id = RubberBubble;
    }
    break; 
  case 1: 
    if (old_stars_in_group < LevelManager::stars_to_open_explode_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_explode_bubbles) {
        unlocked_bubble_id = ExplodingBubble;
    } else if (old_stars_in_group < LevelManager::stars_to_open_higher_order_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_higher_order_bubbles) {
        unlocked_bubble_id = RedEyeBubble;
    }
    break; 
  case 2: 
    if (old_stars_in_group < LevelManager::stars_to_open_black_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_black_bubbles) {
        unlocked_bubble_id = BlackBubble;
    } else if (old_stars_in_group < LevelManager::stars_to_open_white_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_white_bubbles) {
        unlocked_bubble_id = WhiteBubble;
    }
    break; 
  case 3: 
    if (old_stars_in_group < LevelManager::stars_to_open_magnetic_bubbles &&
      new_stars_in_group >= LevelManager::stars_to_open_magnetic_bubbles) {
        unlocked_bubble_id = MagneticBubble;
    }
    break; 
  }
  if (unlocked_bubble_id != TotalVariables) {
    std::string s = tr(unlocked_bubble_id);
    std::replace( s.begin(), s.end(), '\n', ' ');
    sprintf(unlocked,tr(BubbleInInsaneModUnlocked),s.c_str());
    GlobalInfo::get()->queueText(unlocked);
  }
#endif
}

