#ifndef LEVEL_MANAGER_H
#define LEVEL_MANAGER_H

#include "Box2D/Common/b2Draw.h"
#include <string>
#include <vector>
#include <map>
#include "model/bubble_parameters.h"
#include "view/view_interface.h"

const int LEVELS_IN_GROUP = 20;
const int LEVEL_GROUPS_COUNT = 4;

struct Level {
  Level();
  std::map<std::string,BubbleParameters> available_balls;
  std::vector< std::vector<std::string> > balls;
  std::vector< ViewInterface::InfoMessage > new_info;
  int id;
  int group_id;
  enum Type {
    Campaign,
    Classic,
    Insane
  } type;

  int balls_number; // Max number of balls in a row
  bool isLoaded() {return id != -1;}

  struct ResolveResult {
    ResolveResult() : win_positions(0), loose_positions(0),
      undefined_positions(0),
      left_balls(10,0), first_win_step(-1), max_score(0) {}
    int win_positions, loose_positions, undefined_positions;
    std::vector<int> left_balls; // Left balls number distribution
    int first_win_step;
    std::vector<b2Vec2> first_win_clicks;
    int max_score;
  };
};

class LevelManager
{
public:
  static Level loadLevel(unsigned group_id, unsigned level_id);
  static Level loadLevel(const std::string& path);

  struct LevelParameters {
    static LevelParameters classicLevelParameters();
    static LevelParameters insaneLevelParameters(int level);
    int balls_count; // Number of balls in a row
    int columns_count; // Number of columns
    bool only_full_rows; // Disallow incomplete rows
    int colors_count; // Number of different colors
    bool allow_double_color; // Two-colored balls
    bool allow_black_balls; // Black heavy balls
    bool allow_white_balls; // White light balls
    bool allow_explode_balls; // Balls that detonate after death
    bool allow_rubber_balls; // Jumping balls
    bool allow_magnetic_balls; // Balls with charge density
    // Order of ball is the number of times
    // that you should tap it to destroy
    bool allow_higher_orders;
  };

  static const int stars_to_open_double_color_bubbles;
  static const int stars_to_open_black_bubbles;
  static const int stars_to_open_white_bubbles;
  static const int stars_to_open_explode_bubbles;
  static const int stars_to_open_rubber_bubbles;
  static const int stars_to_open_higher_order_bubbles;
  static const int stars_to_open_magnetic_bubbles;

  static Level generateLevel(const LevelParameters &parameters);
  static void saveLevel(const Level& level, const std::string& path);
};

#endif // LEVEL_MANAGER_H
