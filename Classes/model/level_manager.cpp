#include "level_manager.h"

#include "pugixml/pugixml.h"
#ifdef QT
  #include <QFile>
#endif
#ifdef COCOS
  #include "cocos2d.h"
#endif
#include "model/game_progress.h"
#include <cstdlib>
#include <stdio.h>
#include <sstream>

Level::Level() : id(-1), group_id(-1), type(Campaign) {
}

Level LevelManager::loadLevel(unsigned group_id, unsigned level_id) {
  std::stringstream ss;
  ss << "levels/" << group_id << "/" << "level" << level_id << ".xml";
  Level level = LevelManager::loadLevel(ss.str());
  level.group_id = group_id;
  level.id = level_id;
  return level;
}

Level LevelManager::loadLevel(const std::string &path) {
  Level level;
  pugi::xml_document doc;

#ifdef QT
  QFile file((":/data/"+path).c_str());
  file.open(QIODevice::ReadOnly);
  QByteArray file_content = file.readAll();
  pugi::xml_parse_result result = doc.load(file_content.data());
#endif
#ifdef COCOS
  unsigned long bufferSize = 0;
  unsigned char* pBuffer = cocos2d::CCFileUtils::sharedFileUtils()->
    getFileData(path.c_str(), "r", &bufferSize);
  pugi::xml_parse_result result = doc.load_buffer(pBuffer, bufferSize);
  if (bufferSize) {
    delete []pBuffer;
  }
#endif
  if (result) {
    pugi::xml_node level_node = doc.child("level");
    pugi::xml_node new_info_node = level_node.child("new_info");
    for (pugi::xml_node type_node = new_info_node.child("type");
      type_node; type_node = type_node.next_sibling("type")) {
      level.new_info.push_back(
        static_cast<ViewInterface::InfoMessage>(type_node.text().as_uint(0)));
    }
    level.balls_number = level_node.child("balls_number").text().as_uint(0);
    pugi::xml_node ball_types_node = level_node.child("ball_types");
    for (pugi::xml_node ball_node = ball_types_node.child("ball");
         ball_node; ball_node = ball_node.next_sibling("ball")) {
        BubbleParameters::Color color(
          static_cast<BubbleParameters::Color>(
          ball_node.attribute("color").as_int(0)));
      BubbleParameters::Color double_color(
        static_cast<BubbleParameters::Color>(
          ball_node.attribute("double_color").as_int(color)));
      BubbleParameters bp(
            ball_node.attribute("density").as_float(1),
            ball_node.attribute("friction").as_float(0.2f),
            3.0f / level.balls_number,
            color,
            double_color,
            ball_node.attribute("order").as_uint(1),
            ball_node.attribute("charge_density").as_float(0)
            );
      bp.is_rubber = (ball_node.attribute("is_rubber").as_uint(0) == 1);
      bp.is_exploding = (ball_node.attribute("is_exploding").as_uint(0) == 1);
      level.available_balls[ball_node.text().as_string("")] = bp;
    }
    for (pugi::xml_node balls_portion_node = level_node.child("balls_portion");
         balls_portion_node;
         balls_portion_node=balls_portion_node.next_sibling("balls_portion")) {
      std::vector<std::string> balls_portion;
      for (pugi::xml_node ball_node = balls_portion_node.child("ball");
           ball_node; ball_node = ball_node.next_sibling("ball")) {
        balls_portion.push_back(ball_node.attribute("type").as_string(""));
      }
      level.balls.push_back(balls_portion);
    }
  }
  return level;
}

LevelManager::LevelParameters
  LevelManager::LevelParameters::classicLevelParameters() {
  LevelParameters lp;
  lp.balls_count = 6;
  lp.colors_count = 3;
  lp.columns_count = 10;
  lp.only_full_rows = false;
  lp.allow_double_color = lp.allow_black_balls
    = lp.allow_white_balls = lp.allow_explode_balls
    = lp.allow_rubber_balls = lp.allow_higher_orders 
    = lp.allow_magnetic_balls = false;
  return lp;
}

LevelManager::LevelParameters 
  LevelManager::LevelParameters::insaneLevelParameters(int level) {
  LevelParameters lp;
  lp.balls_count = 6;
  if (level % 11 == 4) {
    lp.balls_count = 5;
  }
  if (level % 13 == 7) {
    lp.balls_count  = 8;
  }
  lp.colors_count = ( level % 5 == 2 ? 2 : 3 );
  if (level % 7 == 3) {
    lp.colors_count = 1;
  }
  lp.columns_count = 10;
  lp.only_full_rows = ( level % 3 == 1 );
  lp.allow_double_color = 
    GameProgress::getBoxStarsCount(0) > stars_to_open_double_color_bubbles;
  lp.allow_black_balls = 
    GameProgress::getBoxStarsCount(2) > stars_to_open_black_bubbles;
  lp.allow_white_balls = 
    GameProgress::getBoxStarsCount(2) > stars_to_open_white_bubbles;
  lp.allow_explode_balls = 
    GameProgress::getBoxStarsCount(1) > stars_to_open_explode_bubbles;
  lp.allow_rubber_balls = 
    GameProgress::getBoxStarsCount(0) > stars_to_open_rubber_bubbles;
  lp.allow_higher_orders = 
    GameProgress::getBoxStarsCount(1) > stars_to_open_higher_order_bubbles;
  lp.allow_magnetic_balls = 
    GameProgress::getBoxStarsCount(3) > stars_to_open_magnetic_bubbles;
  lp.allow_magnetic_balls = false;
  return lp;
}

const int TOTAL_STARS_IN_BOX = LEVELS_IN_GROUP * 3;
const int LevelManager::stars_to_open_double_color_bubbles = 5;
const int LevelManager::stars_to_open_black_bubbles = 3;
const int LevelManager::stars_to_open_white_bubbles = TOTAL_STARS_IN_BOX / 2;
const int LevelManager::stars_to_open_explode_bubbles = 3;
const int LevelManager::stars_to_open_rubber_bubbles = TOTAL_STARS_IN_BOX / 2;
const int 
  LevelManager::stars_to_open_higher_order_bubbles = TOTAL_STARS_IN_BOX / 2;
const int 
  LevelManager::stars_to_open_magnetic_bubbles = TOTAL_STARS_IN_BOX / 2;

BubbleParameters::Color randomColor(int colors_count) {
  colors_count = b2Max(colors_count,1);
  colors_count = b2Min(colors_count,3);
  return static_cast<BubbleParameters::Color>(rand() % colors_count);
}

Level LevelManager::generateLevel(
    const LevelManager::LevelParameters &parameters) {
  Level level;
  level.balls_number = parameters.balls_count;
  for (int column=0; column < parameters.columns_count; ++column) {
    std::vector<std::string> balls_portion;
    int balls_in_row = parameters.balls_count;
    if (!parameters.only_full_rows) {
      balls_in_row = 1 + rand() % parameters.balls_count;
    }
    for (int i=0; i < balls_in_row; ++i) {
      BubbleParameters bi(1,0.2f,3.0f / parameters.balls_count,
                          randomColor(parameters.colors_count));
      if (parameters.allow_black_balls && rand()%15 == 0) {
        bi.color = bi.double_color = BubbleParameters::BlackColor;
        bi.density = 30;
        bi.friction = 0.01f;
      } else if (parameters.allow_white_balls && rand()%15 == 0) {
        bi.color = bi.double_color = BubbleParameters::WhiteColor;
        bi.density = 0.1f;
      }
      bi.is_exploding = parameters.allow_explode_balls && rand()%7 == 0;
      if (parameters.allow_magnetic_balls) {
        bi.charge_density = (1 - (rand()%2)*2)*3;
      }
      bi.is_rubber = parameters.allow_rubber_balls && rand()%7 == 0;
      if (parameters.allow_double_color && rand()%7 == 0) {
        if (bi.color == BubbleParameters::BlackColor) {
          bi.density = 15;
        } else if (bi.color == BubbleParameters::WhiteColor) {
          bi.density = 0.5;
        }
        bi.double_color = randomColor(parameters.colors_count);
      }
      if (parameters.allow_higher_orders && rand()%7 == 0) {
        do {
          bi.order++;
        } while (rand()%10 == 0);
      }
      std::map<std::string,BubbleParameters>::const_iterator it =
               level.available_balls.begin();
      for (; it!=level.available_balls.end(); ++it) {
        if (bi.compare(it->second)) break;
      }
      std::string color_name;
      if (it != level.available_balls.end()) {
        color_name = it->first;
      } else {
        char a = '1' + level.available_balls.size();
        color_name += a;
        level.available_balls[color_name] = bi;
      }
      balls_portion.push_back(color_name);
    }
    level.balls.push_back(balls_portion);
  }
  return level;
}

void LevelManager::saveLevel(const Level &level, const std::string &path) {
  pugi::xml_document doc;
  pugi::xml_node level_node = doc.append_child("level");

  pugi::xml_node balls_number_node = level_node.append_child("balls_number");
  char bn[10];
  sprintf(bn,"%i",level.balls_number);
  balls_number_node.append_child(pugi::node_pcdata).set_value(bn);

  pugi::xml_node ball_types_node =
      level_node.append_child("ball_types");
  for (std::map<std::string,BubbleParameters>::const_iterator it =
       level.available_balls.begin(); it != level.available_balls.end(); ++it) {
    pugi::xml_node ball_node = ball_types_node.append_child("ball");
    ball_node.append_child(pugi::node_pcdata).set_value(it->first.c_str());

    ball_node.append_attribute("density") = it->second.density;
    ball_node.append_attribute("friction") = it->second.friction;
    ball_node.append_attribute("order") = it->second.order;
    ball_node.append_attribute("color") = static_cast<int>(it->second.color);
    if (it->second.isDouble()) {
      ball_node.append_attribute("double_color") = 
        static_cast<int>(it->second.double_color);
    }
    if (it->second.is_exploding) {
      ball_node.append_attribute("is_exploding") = 1;
    }
    if (it->second.is_rubber) {
      ball_node.append_attribute("is_rubber") = 1;
    }
    if (it->second.charge_density) {
      ball_node.append_attribute("charge_density") = it->second.charge_density;
    }
  }

  for (size_t i=0; i<level.balls.size(); ++i) {
    pugi::xml_node balls_portion_node =
        level_node.append_child("balls_portion");
    for (size_t j=0; j<level.balls[i].size(); ++j) {
      balls_portion_node.append_child("ball")
                        .append_attribute("type") = level.balls[i][j].c_str();
    }
  }

  doc.save_file(path.c_str());
}
