#include "ground.h"

b2Body *Ground::createGround(b2World *world) {
  b2BodyDef body_def;
  body_def.position.Set(0.0f, -10.0f);
  b2Body *body = world->CreateBody(&body_def);

  b2Vec2 vertices[4];
  vertices[3] = b2Vec2(-50,5);
  vertices[2] = b2Vec2(-50,60);
  vertices[1] = b2Vec2(0,10);
  vertices[0] = b2Vec2(0,5);
  b2PolygonShape groundBox;
  groundBox.Set(vertices,4);
  b2Fixture *fixture = body->CreateFixture(&groundBox, 0.0f);
  b2Filter filter = fixture->GetFilterData();
  filter.maskBits = 0xFFFF; // Collide with all
  fixture->SetFilterData(filter);

  vertices[3] = b2Vec2(0,5);
  vertices[2] = b2Vec2(0,10);
  vertices[1] = b2Vec2(50,60);
  vertices[0] = b2Vec2(50,5);
  groundBox.Set(vertices,4);
  body->CreateFixture(&groundBox, 0.0f)->SetFilterData(filter);

  vertices[3] = b2Vec2(-50,60);
  vertices[2] = b2Vec2(-50,70);
  vertices[1] = b2Vec2(50,70);
  vertices[0] = b2Vec2(50,60);
  groundBox.Set(vertices,4);
  body->CreateFixture(&groundBox, 0.0f)->SetFilterData(filter);

  return body;
}
