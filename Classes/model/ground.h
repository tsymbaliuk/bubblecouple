#ifndef GROUND_H
#define GROUND_H

#include "Box2D/Box2D.h"

class Ground
{
public:
  static b2Body *createGround(b2World *world);
};

#endif // GROUND_H
