#include "bubble_parameters.h"

BubbleParameters::BubbleParameters() :
  radius(0.5f),
  density(1),
  friction(0.2f),
  order(1),
  is_exploding(false),
  is_rubber(false),
  charge_density(0),
  is_availible(false) {
}


BubbleParameters::BubbleParameters(float density, float friction, float radius, 
                                   BubbleParameters::Color color) :
  radius(radius),
  density(density),
  friction(friction),
  color(color),
  double_color(color),
  order(1),
  is_exploding(false),
  is_rubber(false),
  charge_density(0),
  is_availible(false) {
}

BubbleParameters::BubbleParameters(float density, float friction, float radius,
                                   BubbleParameters::Color color, 
                                   BubbleParameters::Color double_color,
                                   int order, float charge_density) :
  radius(radius),
  density(density),
  friction(friction),
  color(color),
  double_color(double_color),
  order(order),
  is_exploding(false),
  is_rubber(false),
  charge_density(charge_density),
  is_availible(false) {
}

BubbleParameters::~BubbleParameters() {
}

bool BubbleParameters::compare(const BubbleParameters &other) {
  return radius == other.radius &&
      density == other.density &&
      friction == other.friction &&
      color == other.color &&
      double_color == other.double_color &&
      order == other.order &&
      is_exploding == other.is_exploding &&
      is_rubber == other.is_rubber &&
      charge_density == other.charge_density;
}
