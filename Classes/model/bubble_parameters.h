#ifndef BUBBLE_PARAMETERS_H
#define BUBBLE_PARAMETERS_H

#include "Box2D/Box2D.h"

#ifdef COCOS
#include "view/bubble_animations.h"
class Bubble;
#endif

struct BubbleParameters {

  // Note that RColor might be yellow, brown or even white, that's just
  // a name for color. Actual color differs from location to location and caused
  // only by colors that should be pretty.
  enum Color {
    RColor,
    GColor,
    BColor,
    BlackColor,
    WhiteColor,

    TotalColors
  };

  BubbleParameters();
  BubbleParameters(float density, float friction, float radius, Color color);
  BubbleParameters(float density, float friction, float radius,
                   Color color, Color double_color, int order = 1, 
                   float charge_density = 0);
  ~BubbleParameters();
  bool isDouble() const {return !(color == double_color);}

  // Returns true if parameters that must be constant (radius, density,
  // friction, color, double_color, order) are the same
  bool compare(const BubbleParameters &other);

  float radius;
  float density;
  float friction;

  Color color;
  Color double_color; // same to color in solid-color balls
  int order;
  bool is_exploding;
  bool is_rubber;
  float charge_density;
  // if you want to add other parameter here, you should add it to
  // bool compare(...) function too

  b2Vec2 position;
  float angle;
  bool is_availible;
#ifdef COCOS
  Bubble *sprite;
  AnimationTarget animation_target;
#endif
};

#endif // BUBBLE_PARAMETERS_H
