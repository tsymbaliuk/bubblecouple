#ifndef GAME_PROGRESS_H
#define GAME_PROGRESS_H

#include <vector>
#include <string>
#include <cstddef> // for size_t
#include "view/view_interface.h"

struct LevelProgress {
  static LevelProgress fromChar(char lp_char);
  static char toChar(LevelProgress lp);
  char toChar() {return toChar(*this);}

  LevelProgress(int star_count = 0, bool blocked = true) :
    star_count(star_count), blocked(blocked) {}
  int star_count;
  bool blocked;
};

typedef std::vector<LevelProgress> GroupProgress;

class GameProgress {
public:
  static LevelProgress getLevelProgress(size_t group, size_t level);
  static void setLevelProgress(size_t group, size_t level,
                               LevelProgress progress);
  static int getTotalStarsCount();
  static int getBoxStarsCount(size_t box_number);
  static int getStarsForBoxUnlock(size_t box_number);
  static int getScore();
  static void setScore(int new_score);
  static int getInsaneLevelRecord();
  static void setInsaneLevelRecord(int level);
#ifdef ANDROID
  static int getOfferwallStarsCount();
  static void setOfferwallStarsCount(int new_stars_count);
#endif
  static int getExternalStarsCount();
  static void setExternalStarsCount(int new_stars_count);
private:
  static std::vector<std::string> loadProgress();
  static bool is_progress_loaded;
  static std::vector<std::string> progress;
  static void starsNumberChanged(int old_stars_count);
  static void starsInGroupNumberChanged(int old_stars_in_group, size_t group);
};

#endif // GAME_PROGRESS_H
