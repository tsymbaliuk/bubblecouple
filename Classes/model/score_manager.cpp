#include "score_manager.h"

#include "model/game_progress.h"
#include <algorithm>

ScoreManager::ScoreManager() : score(GameProgress::getScore()) {
}

ScoreManager::ScoreManager(int score_) : score(score_) {
}

void ScoreManager::increaseScore(int delta) {
  score += delta;
  for (size_t i=0; i<observers.size(); ++i) {
    observers[i]->onScoreChanged(score, delta);
  }
}

void ScoreManager::flush() {
  GameProgress::setScore(score);
}

void ScoreManager::addObserver(ScoreManager::Observer *observer) {
  observers.push_back(observer);
  observer->onScoreChanged(score,0);
}

void ScoreManager::removeObserver(ScoreManager::Observer *observer) {
  observers.erase(std::remove(observers.begin(),observers.end(),observer),
                  observers.end());
}
