#ifndef MAIN_MODEL_H
#define MAIN_MODEL_H

#include "Box2D/Box2D.h"
#include "level_manager.h"
#include "score_manager.h"
#include <set>

class Bubbles;
class BonusManager;
class ViewInterface;
class MainModel
{
public:
  MainModel();
  ~MainModel();
  
  void addScoreObserver(ScoreManager::Observer *observer);
  static Level::ResolveResult resolveLevel(const Level& level);
  void removeScoreObserver(ScoreManager::Observer *observer);
  void setViewInterface(ViewInterface *view_interface_);
  void setSoundsInterface(SoundsInterface *sounds_interface_);

  void setWinSequence(std::vector<b2Vec2> sequence) {}

  void mouseClicked(b2Vec2 pos);
  void bonusPressed(Bonus type);
  void bonusPicked(Bonus type);

  void startClassic(); // Classic infinite game
  void startInsane(); // Infinite game with different bonuses
  void startCampaign(int level_group, int level);
  void skipAndStartNext();
  void pauseGame();
  void resumeGame();
  void stopGame();
  void restartGame();

  void doStep(float dt);
private:
  static b2World *constructWorld();
  void startLevel(const Level& level);
  void startInsaneImplicitly();
  void newIteration();
  void proceedGameEnd(int balls_left);
  void levelChanged();
  void decreaseInsaneBubblesNumber(int delta);
  ScoreManager *score_manager;
  bool paused;
  bool stopped;
  ViewInterface *current_view_interface;
  SoundsInterface *current_sounds_interface;
  Bubbles *bubbles;
  BonusManager *bonus_manager;
  b2World *world;
  bool is_active;

  // level_id is equal to -1 in infinite games, level_group_id is equal to 
  // current location
  int level_group_id, level_id;
  bool is_insane_mode;
  int insane_left_bubbles;
  int insane_current_level;
  float insane_time_left;
};

#endif // MAIN_MODEL_H
