#ifndef BONUS_MANAGER_H
#define BONUS_MANAGER_H

#include "model/score_manager.h"
#include "view/view_interface.h"

class Bubbles;
class BonusManager : public ScoreManager::Observer {
public:
  BonusManager(Bubbles *bubbles);

  void handleBonus( Bonus type );
  void bonusPicked( Bonus type );
  bool hasBonuses() const;
  void step();
  void updateBubbles(Bubbles *bubbles_);
private:
  void handleShuffler();
  void handleSplitter();
  void handleEarthquake();
  void handleExploser();
  void handleBubbleRain();
  void handleCreator();

  void applyShuffler();
  void applySplitter();
  void applyEarthquake();
  void applyExploser();
  void applyBubbleRain();
  void applyCreator();

  virtual void onScoreChanged(int new_score, int delta);

  Bubbles *bubbles;

  int time_to_end[BonusesNumber];
  bool available_bonuses[BonusesNumber];
};

#endif // BONUS_MANAGER_H
