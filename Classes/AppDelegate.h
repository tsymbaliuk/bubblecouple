#ifndef __APP_DELEGATE_H__
#define __APP_DELEGATE_H__

#include "cocos2d.h"

#include "view/main_menu.h"
#include "view/levels_menu.h"
#include "view/game_view.h"
#include "view/info_menu.h"
#include "view/settings_menu.h"

class GameView;
class MainModel;

/**
@brief    The cocos2d Application.

The reason for implement as private inheritance is to hide some interface call by CCDirector.
*/

class AppDelegate : private cocos2d::CCApplication, public MainMenu::Observer,
  public LevelsMenu::Observer, public GameView::Observer,
  public InfoMenu::Observer, public SettingsMenu::Observer {
public:
  AppDelegate();
  virtual ~AppDelegate();

  /**
  @brief    Implement CCDirector and CCScene init code here.
  @return true    Initialize success, app continue.
  @return false   Initialize failed, app terminate.
  */
  virtual bool applicationDidFinishLaunching();

  /**
  @brief  The function be called when the application enter background
  @param  the pointer of the application
  */
  virtual void applicationDidEnterBackground();

  /**
  @brief  The function be called when the application enter foreground
  @param  the pointer of the application
  */
  virtual void applicationWillEnterForeground();
private:
  // MainMenu::Observer implementation
  virtual void startCampaign();
  virtual void startInsane();
  virtual void startClassic();
  virtual void showInfo();
  virtual void showSettings();
  virtual void quit();
  // GameView::Observer implementation
  virtual void tick(float dt);
  virtual void touched(b2Vec2 pos);
  virtual void pauseButtonPressed();
  virtual void resumeButtonPressed();
  virtual void nextLevelButtonPressed();
  virtual void toMenuButtonPressed();
  virtual void toLevelsButtonPressed();
  virtual void restartButtonPressed();
  virtual void bonusButtonPressed(Bonus type);
  virtual void bonusPicked(Bonus type);
  // LevelsMenu::Observer implementation
  virtual void startLevel(int group, int level);
  virtual void quitLevelSelection();
  // InfoMenu::Observer implementation
  virtual void quitInfoMenu();
  // SettingsMenu::Observer implementation
  virtual void quitSettingsMenu();
  virtual void languageChanged();


  MainMenu *main_menu;
  CCScene *main_scene;
  GameView *game_view;
  InfoMenu *info_menu;
  SettingsMenu *settings_menu;
  LevelsMenu *levels_menu;

  bool application_did_finish_launching;

  MainModel *main_model;
};

#endif  // __APP_DELEGATE_H__

