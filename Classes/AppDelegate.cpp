#include "cocos2d.h"
#include "CCEGLView.h"
#include "AppDelegate.h"
#include "view/comics_scene.h"
#include "view/game_view.h"
#include "view/global_info.h"
#include "view/main_menu.h"
#include "view/resource_loader.h"
#include "view/sounds_manager.h"
#include "view/view_settings.h"
#include "model/main_model.h"
#include "SimpleAudioEngine.h"

using namespace CocosDenshion;

USING_NS_CC;

enum Direction { DirectionUp, DirectionDown, DirectionLeft, DirectionRight };
void showHide(CCLayer *sprite_to_show, CCLayer *sprite_to_hide, Direction dir) {
  sprite_to_show->setKeypadEnabled(true);
  sprite_to_hide->setKeypadEnabled(false);

  sprite_to_show->setVisible(true);

  sprite_to_show->stopAllActions();
  sprite_to_hide->stopAllActions();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  CCPoint pos;
  switch(dir) {
  case DirectionUp: pos = ccp(0,-win_size.height); break;
  case DirectionDown: pos = ccp(0,win_size.height); break;
  case DirectionLeft: pos = ccp(win_size.width,0); break;
  case DirectionRight: pos = ccp(-win_size.width,0); break;
  }

  sprite_to_show->runAction(
    CCEaseSineOut::create(CCMoveTo::create(0.5f,ccp(0,0))));
  sprite_to_hide->runAction(
    CCSequence::createWithTwoActions(
      CCEaseSineOut::create(CCMoveTo::create(0.5f,pos)),CCHide::create()));
}

AppDelegate::AppDelegate() : application_did_finish_launching(false) {
}

AppDelegate::~AppDelegate()
{
  delete main_model;
  SimpleAudioEngine::end();
}

bool AppDelegate::applicationDidFinishLaunching()
{
  // initialize director
  CCDirector *pDirector = CCDirector::sharedDirector();
  application_did_finish_launching = true;
  pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());

  // turn on display FPS
  //pDirector->setDisplayStats(true);

  // set FPS. the default value is 1.0/60 if you don't call this
  pDirector->setAnimationInterval(1.0 / 60);

  // Creating common background
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  char path[100];
  sprintf(path,"images/menu/stars_%i.jpg",getResourceWidth());
  CCSprite *background = CCSprite::create(path);
  background->setPosition(ccp(win_size.width/2,win_size.height/2));
  float scale = 1;
  float win_ratio = win_size.width / win_size.height;
  float bg_ratio = background->getContentSize().width
                   / background->getContentSize().height;
  if (bg_ratio > win_ratio) {
    // scale to height
    scale = win_size.height / background->getContentSize().height;
  } else {
    // scale to width
    scale = win_size.width / background->getContentSize().width;
  }
  background->setScale(scale);

  // Creating common foreground
  const float w1 = background->getContentSize().width;
  const float h1 = background->getContentSize().height;
  const float s1 = background->getScale();

  sprintf(path,"images/menu/level_select_bg_%i.jpg",getResourceWidth());
  CCSprite *level_select_background = CCSprite::create(path);
  const float w2 = level_select_background->getContentSize().width;
  sprintf(path,"images/menu/level_select_fg_%i.png",getResourceWidth());
  CCSprite *foreground = CCSprite::create(path);
  float s2;
  bg_ratio = foreground->getContentSize().width
             / foreground->getContentSize().height;
  if (bg_ratio < win_ratio) {
    s2 = win_size.height / foreground->getContentSize().height;
  } else {
    s2 = win_size.width / foreground->getContentSize().width;
  }

  sprintf(path,"images/menu/stars_%i.jpg",getResourceWidth());
  CCRect rect_l(0,0,(w1*s1-w2*s2)/2/s1, h1);
  CCSprite* foreground_l = CCSprite::create(path, rect_l);
  foreground_l->setScale(s1);
  foreground_l->setPosition
    (ccp(win_size.width/2-(w1*s1/2-(w1*s1-w2*s2)/4),win_size.height/2));

  CCRect rect_r(w1-rect_l.size.width,0,rect_l.size.width, h1);
  CCSprite* foreground_r = CCSprite::create(path, rect_r);
  foreground_r->setScale(s1);
  foreground_r->setPosition(
    ccp(win_size.width/2+(w1*s1/2-(w1*s1-w2*s2)/4),win_size.height/2));

  main_scene = CCScene::create();
  main_scene->addChild(background,-1);
  main_scene->addChild(foreground_l,100);
  main_scene->addChild(foreground_r,100);
  pDirector->pushScene(main_scene);

  // run
  if (!getViewSettings().hasComicsBeenSeen()) {
    getViewSettings().setComicsBeenSeen(true);
    CCScene *comics_scene = CCScene::create();
    comics_scene->addChild(new ComicsScene());
    pDirector->runWithScene(comics_scene);
  } else {
    SoundsManager::get().playMainMenuMusic();
  }

  main_menu = new MainMenu(scale);
  main_menu->setObserver(this);
  main_scene->addChild(main_menu, 300);

  info_menu = new InfoMenu();
  info_menu->setPositionY(win_size.height);
  info_menu->setObserver(this);
  info_menu->setVisible(false);
  main_scene->addChild(info_menu,101);

  settings_menu = new SettingsMenu();
  settings_menu->setPositionX(win_size.width);
  settings_menu->setObserver(this);
  settings_menu->setVisible(false);
  main_scene->addChild(settings_menu,101);

  levels_menu = new LevelsMenu(main_scene);
  levels_menu->setObserver(this);
  levels_menu->setPositionY(-win_size.height);
  levels_menu->foreground->setPosition(
    ccp(win_size.width/2,-win_size.height/2));
  levels_menu->setVisible(false);
  levels_menu->foreground->setVisible(false);
  main_scene->addChild(levels_menu);

  main_model = new MainModel();
  game_view = new GameView();
  main_model->setViewInterface(game_view);
  main_model->setSoundsInterface(game_view);
  main_model->addScoreObserver(game_view);
  game_view->setObserver(this);
  game_view->setVisible(false);
  game_view->setPositionY(-win_size.height);
  main_scene->addChild(game_view,101);

  main_menu->setKeypadEnabled(true);

  main_scene->addChild(GlobalInfo::get(), 500); // GlobalInfo is always on top

  return true;
}

// This function will be called when the app is inactive. 
// When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
  SoundsManager::get().pauseMusic();
  CCDirector::sharedDirector()->stopAnimation();
  if (game_view->isVisible()) {
    game_view->pauseGame();
  }
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
  if (application_did_finish_launching) {
    SoundsManager::get().restoreMenuOrComicsMusic();
    CCDirector::sharedDirector()->startAnimation();
  }
}

void AppDelegate::startCampaign() {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  levels_menu->foreground->stopAllActions();
  levels_menu->foreground->setVisible(true);
  levels_menu->foreground->runAction(CCEaseSineOut::create(
    CCMoveTo::create(0.5f,ccp(win_size.width/2,win_size.height/2))));
  showHide(levels_menu,main_menu,DirectionDown);
}

void AppDelegate::startInsane() {
  main_model->startInsane();
  showHide(game_view,main_menu,DirectionDown);
}

void AppDelegate::startClassic() {
  main_model->startClassic();
  showHide(game_view,main_menu,DirectionDown);
}

void AppDelegate::showInfo() {
  showHide(info_menu,main_menu,DirectionUp);
}

void AppDelegate::showSettings() {
  showHide(settings_menu,main_menu,DirectionRight);
}

void AppDelegate::quit() {
  CCDirector::sharedDirector()->end();
}

void AppDelegate::tick(float dt) {
  main_model->doStep(dt);
}

void AppDelegate::touched( b2Vec2 pos ) {
  main_model->mouseClicked(pos);
}

void AppDelegate::pauseButtonPressed() {
  main_model->pauseGame();
}

void AppDelegate::resumeButtonPressed() {
  main_model->resumeGame();
}

void AppDelegate::nextLevelButtonPressed() {
  main_model->skipAndStartNext();
}

void AppDelegate::toMenuButtonPressed() {
  game_view->clear();
  main_model->pauseGame();
  showHide(main_menu,game_view,DirectionUp);
  SoundsManager::get().playMainMenuMusic();
}

void AppDelegate::toLevelsButtonPressed() {
  game_view->clear();
  main_model->pauseGame();
  levels_menu->updateStars();
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  levels_menu->foreground->setVisible(true);
  levels_menu->foreground->stopAllActions();
  levels_menu->foreground->runAction(CCEaseSineOut::create(
    CCMoveTo::create(0.5f,ccp(win_size.width/2,win_size.height/2))));
  showHide(levels_menu,game_view,DirectionUp);
  SoundsManager::get().playMainMenuMusic();
}

void AppDelegate::restartButtonPressed() {
  main_model->restartGame();
}

void AppDelegate::bonusButtonPressed( Bonus type ) {
  main_model->bonusPressed(type);
}

void AppDelegate::bonusPicked( Bonus type ) {
  main_model->bonusPicked(type);
}

void AppDelegate::startLevel( int group, int level ) {
  main_model->startCampaign(group,level);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  levels_menu->foreground->stopAllActions();
  levels_menu->foreground->runAction(
    CCSequence::createWithTwoActions(
      CCEaseSineOut::create(
        CCMoveTo::create(0.5f,ccp(win_size.width/2,3*win_size.height/2))),
      CCHide::create()));
  showHide(game_view,levels_menu,DirectionDown);
}

void AppDelegate::quitLevelSelection() {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  levels_menu->foreground->stopAllActions();
  levels_menu->foreground->runAction(
    CCSequence::createWithTwoActions(
      CCEaseSineOut::create(
        CCMoveTo::create(0.5f,ccp(win_size.width/2,-win_size.height/2))),
      CCHide::create()));
  showHide(main_menu,levels_menu,DirectionUp);
}

void AppDelegate::quitInfoMenu() {
  showHide(main_menu,info_menu,DirectionDown);
}

void AppDelegate::quitSettingsMenu() {
  // It is possible to change language or quality settings in settings menu,
  // so we should write appropriate functions.
  showHide(main_menu,settings_menu,DirectionLeft);
}

void AppDelegate::languageChanged() {
  settings_menu->onLanguageChanged();
  main_menu->onLanguageChanged();
  info_menu->onLanguageChanged();
  levels_menu->onLanguageChanged();
  game_view->onLanguageChanged();
}


