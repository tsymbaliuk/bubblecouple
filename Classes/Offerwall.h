#ifndef __CCADVIEW_H__
#define __CCADVIEW_H__

class OfferwallInterface {
public:
  virtual ~OfferwallInterface() {}
  virtual void starsNumberUpdateStarted() = 0;
  virtual void starsNumberUpdated(int new_stars_count) = 0;
  virtual void starsNumberUpdateFailed() = 0;
};

void showOfferwall(OfferwallInterface *interface_);

#endif /* __CCADVIEW_H__ */