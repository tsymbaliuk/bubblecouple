#ifndef BUBBLE_ANIMATIONS_H
#define BUBBLE_ANIMATIONS_H

#include "cocos2d.h"

using namespace cocos2d;

enum AnimationType {
  None,
  EyeAnimation1,
  AngryMaskAction,
  AngryEyesAction,
  GlassAnimation
};

class Bubble;
struct AnimationTarget {
  AnimationTarget();

  CCSprite *eye;
  CCSprite *left_pupil, *right_pupil;
  CCSprite *glasses;
  CCSprite *mask;

  Bubble *bubble;
};

class BubbleAnimations
{
public:
  static void startRandomAnimation(AnimationTarget target);
  static void startAngryAnimation(AnimationTarget target);
private:
  // Absolute action, will return to start position from any other start 
  // position
  static CCActionInterval * createGlassesAnimation( CCSprite *glasses );
  // Relative action, should bu run only from start position
  static CCActionInterval * createPupilAnimation1( CCSprite *pupil );
  // Relative action, should bu run only from start position
  static CCActionInterval * createEyeAnimation( CCSprite *eye );
};

#endif  // BUBBLE_ANIMATIONS_H