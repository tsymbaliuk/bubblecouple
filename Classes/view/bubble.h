#ifndef BUBBLE_H
#define BUBBLE_H

#include "cocos2d.h"

#include "view/colors.h"
#include "view/view_interface.h"

using namespace cocos2d;

class Bubble : public CCSprite
{
public:
  Bubble(BubbleParameters *ball, float scale, Location location);
  Bubble(
    ViewInterface::InfoMessage info, float real_diameter, Location location);
  void setOpacity(GLubyte value, bool immediately);
  void updateColor(Location location, BubbleParameters::Color color, 
                   BubbleParameters::Color double_color);
  void setPukingMouth();
private:
  static void setOpacity(
    CCSprite* sprite_object, GLubyte value, bool immediately);
  void init( BubbleParameters *ball, float real_diameter, Location location );


  CCSprite *mask_sprite;
  CCSprite *mask_second_sprite;
  CCSprite *mouth_sprite;
  bool is_mouse_puking;
  int size;
};

#endif  // BUBBLE_H