#ifndef GAME_VIEW_H
#define GAME_VIEW_H

#include "cocos2d.h"

#include "Box2D/Box2D.h"

#include "model/level_manager.h"
#include "model/score_manager.h"
#include "view/view_interface.h"
#include "view/game_menu.h"

using namespace cocos2d;

class GameView : 
  public CCLayer, public GameMenu::Observer, public ViewInterface,
  public SoundsInterface, public ScoreManager::Observer
{
public:
  GameView();
  ~GameView();

  void setGroupAndLevel(int group_, int level_);

  void gameLogic(float dt);
  void pauseGame();
  void onLanguageChanged();

  class Observer {
  public:
    virtual void tick(float dt) = 0;
    virtual void touched(b2Vec2 pos) = 0;
    virtual void pauseButtonPressed() = 0;
    virtual void resumeButtonPressed() = 0;
    virtual void nextLevelButtonPressed() = 0;
    virtual void toMenuButtonPressed() = 0;
    virtual void toLevelsButtonPressed() = 0;
    virtual void restartButtonPressed() = 0;
    virtual void bonusButtonPressed(Bonus type) = 0;
    virtual void bonusPicked(Bonus type) = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
  void clear();
private:
  // ViewInterface implementation
  virtual void ballCreated(BubbleParameters *ball);
  virtual void ballDestroyed(BubbleParameters *ball);
  virtual void ballUpdated(BubbleParameters *ball, BallUpdatedParameter param);
  virtual void sparkCreated(Spark spark);
  virtual void sparkDestroyed(Spark spark);
  virtual void newInfoMessage(InfoMessage type);
  virtual void bonusAppeared(Bonus type);
  void hideBonuses();
  virtual void bonusNumberUpdated(bool *bonuses_number);
  virtual void leftTimeChanged(float time_left);
  virtual void leftBubblesChanged(int left_bubbles);
  void updateBubble(BubbleParameters *ball, bool remove_before, bool add_after);
  virtual void worldChanged();
  virtual void gameFinished(int balls_left, int previous_stars_count);
  virtual void levelChanged(int new_group, int new_level);
  virtual void gameResumed();

  // SoundsInterface implementation
  virtual void bubbleTouchedBubble(float power, bool rubber);
  virtual void bubbleTouchedGround(float power, bool rubber);
  virtual void bubbleExploded();
  virtual void bubbleDisappeared();
  virtual void bubbleBecomeSmaller();
  virtual void aloneBubbleClicked(BubbleParameters *ball);

  // GameMenu::Observer implementation
  virtual void resumeButtonPressed();
  virtual void restartButtonPressed();
  virtual void nextLevelButtonPressed();
  virtual void menuButtonPressed();

  // ScoreManager::Observer implementation
  virtual void onScoreChanged(int new_score, int delta);
  int level_score;
  int left_bubbles;
  int score;
  CCLabelTTF *score_label;
  CCLabelTTF *time_label;
  CCPoint last_touch;

  virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

  virtual void keyBackClicked();

  std::list<InfoMessage> info_queue;
  CCMenuItemImage *info_menu_item;

  int scale;
  CCPoint translateToView(b2Vec2 point);
  b2Vec2 translateToWorld(CCPoint point);

  std::vector<BubbleParameters *> balls;
  std::list<Spark> sparks;

  CCSprite* backgrounds[LEVEL_GROUPS_COUNT];
  CCSprite* foregrounds[LEVEL_GROUPS_COUNT];
  CCMenuItemImage *restart_button;
  CCSprite *button_r_background;

  void bonusCallback(CCObject* sender);

  void menuPauseCallback(CCObject*);
  void menuRestartCallback(CCObject*);
  void menuNewInfoCallback(CCObject* sender);

  int group, level;

  // Animate creation from the point
  void handleGameMenuCreation(CCPoint point);
  void handleGameMenuCreation();
  GameMenu *game_menu;
  Observer *observer;
  CCMenu* menu;

  void createBonusesMenu(float scale);
  CCMenuItemImage* bonuses_buttons[BonusesNumber];
  int insane_time_left;
 
  CCSprite *shadow; // Shadow for game menu that covers all the game view
  void showShadow();
  void hideShadow();
  void removeGameMenuWithAnimation();
};

#endif  // GAME_VIEW_H