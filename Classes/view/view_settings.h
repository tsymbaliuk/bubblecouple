#ifndef VIEW_SETTINGS_H
#define VIEW_SETTINGS_H

#include <string>
#include "view/translator.h"
#include "view/view_interface.h"

class ViewSettings {
public:
  inline int getSoundsLevel() const {return sound_level;};
  void setSoundsLevel(int level);

  inline int getMusicLevel() const {return music_level;};
  void setMusicLevel(int level);

  inline bool hasComicsBeenSeen() const {return comics_was_seen;};
  void setComicsBeenSeen(bool state);

  enum Graphics {
    Low, Middle, High
  };
  inline Graphics getGraphics() const {return graphics;};
  void setGraphics(Graphics state);

  inline Language getLanguage() const {return language;};
  void setLanguage(Language state);

  inline int getCurrentLevelGroup() const {return current_level_group;};
  void setCurrentLevelGroup(int current_level_group_);

  bool isUserSawInfoMessage( ViewInterface::InfoMessage info );
  void setUserSawInfoMessage( ViewInterface::InfoMessage info );
private:
  ViewSettings();

  // handled settings:
  bool comics_was_seen;
  int sound_level, music_level;
  int current_level_group;
  Language language;
  Graphics graphics;

  friend ViewSettings& getViewSettings();
};

ViewSettings& getViewSettings();

#endif // VIEW_SETTINGS_H
