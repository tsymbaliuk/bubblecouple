#ifndef INFO_MENU_H
#define INFO_MENU_H

#include "view/menu_interface.h"

using namespace cocos2d;

class InfoMenu : public MenuInterface, public MenuInterface::InternalObserver
{
public:
  InfoMenu();
  class Observer {
  public:
    virtual void quitInfoMenu() = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
  void onLanguageChanged();
private:
  void callback( CCObject * sender );
  virtual void quitMenu();
  Observer *observer;
  CCMenuItemFont* titles[4];
};

#endif // INFO_MENU_H