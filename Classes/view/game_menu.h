#ifndef GAME_MENU_H
#define GAME_MENU_H

#include "cocos2d.h"

#include "view/view_interface.h"
#include "view/colors.h"

using namespace cocos2d;

class GameMenu : public CCSprite
{
public:
  static GameMenu *createPauseMenu(int stars_count, bool show_skip_button);
  static GameMenu *createPauseMenu(int score);
  static GameMenu *createPauseMenu(int level, int score);
  static GameMenu *createEndGameMenu(
    int stars_count, int previous_stars_count, bool show_skip_button);
  static GameMenu *createEndGameMenu(int score);
  static GameMenu *createEndGameMenu(
    int level, int score, int time_bonus_score, bool new_record);
  static GameMenu *createNewInfoMenu( 
    ViewInterface::InfoMessage info, Location location );

  class Observer {
  public:
    virtual void resumeButtonPressed() = 0;
    virtual void restartButtonPressed() = 0;
    virtual void nextLevelButtonPressed() = 0;
    virtual void menuButtonPressed() = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
private:
  GameMenu(bool show_back_button, bool show_skip_button,
           bool show_restart_button, bool show_menu_button);
  void showStars(int count, int previous_count, bool animate = true);
  void showScore(int score);
  void showLevelAndScore(int level, int score);
  void showLevelAndBestLevel(int level);
  void showScoreAndBonus(int score, int bonus_score, bool new_record);
  enum BorderType {
    PauseBorder,
    WinBorder,
    WinRaysBorder,
    LooseBorder
  };
  CCSprite *showBorder( BorderType border_type );
  void showWinEffects();
  void showPausedText();
  void showBubble( ViewInterface::InfoMessage info, Location location );
  void showNewInfoText( const char *header, const char *description );
  void showText(const char *text);

  CCMenuItemImage *skip_button;
  void replaceSkipToBackButton();

  void menuResumeCallback(CCObject* );
  void menuRestartCallback(CCObject* );
  void menuNextLevelCallback(CCObject* );
  void menuMenuCallback(CCObject* );

  std::list<std::string> next_subinfo;
  Observer *observer;
  CCLabelTTF *label;
  CCLabelTTF *sub_label;
  float scale_factor;
  CCSize bg_size;
};

#endif  // GAME_MENU_H