#include "view/game_view.h"

#include "view/bubble.h"
#include "view/bubble_animations.h"
#include "view/bubble_sparks.h"
#include "view/colors.h"
#include "view/global_info.h"
#include "view/main_menu.h"
#include "view/resource_loader.h"
#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/view_settings.h"
#include "model/game_progress.h"

using namespace cocos2d;

GameView::GameView() : 
  level_score(0), score(0), info_menu_item(NULL), scale(20), 
  observer(NULL), group(-1), level(-1), game_menu(NULL), insane_time_left(0) {
  CCLayer::init();
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char background_path[100], mask_path[100];
  for (int i = 0; i < LEVEL_GROUPS_COUNT; ++i) {
    sprintf(background_path,
      "images/levels/%i_%i.jpg",i+1,getResourceWidth());
    sprintf(mask_path,
      "images/levels/%i_mask_%i.png",i+1,getResourceWidth());
    // More resource usage by double loading of background image
    // but avoiding screen blinking on Galaxy S4
    foregrounds[i] = spriteFromMask(background_path,mask_path);

    foregrounds[i]->setVisible(false);
  }

  float scale_factor = 1;

  for (int i = 0; i < LEVEL_GROUPS_COUNT; ++i) {
    float bg_ratio = foregrounds[i]->getContentSize().width
                     / foregrounds[i]->getContentSize().height;
    float win_ratio = win_size.width / win_size.height;

    sprintf(background_path,
      "images/levels/%i_%i.jpg",i+1,getResourceWidth());

    if (bg_ratio > win_ratio) {
      // scale to height
      scale_factor = win_size.height / foregrounds[i]->getContentSize().height;
      backgrounds[i] = CCSprite::create(background_path);
      foregrounds[i]->setPosition(ccp(win_size.width/2, win_size.height/2));
    } else {
      // scale to width
      scale_factor = win_size.width / foregrounds[i]->getContentSize().width;

      CCRect rect(0,(foregrounds[i]->getContentSize().height
                     - win_size.height/scale_factor)/2,
                  getResourceWidth(), win_size.height/scale_factor);

      backgrounds[i] = CCSprite::create(background_path, rect);
      foregrounds[i]->setPosition(ccp(
        win_size.width/2, 
        foregrounds[i]->getContentSize().height*scale_factor/2));
    }
    backgrounds[i]->setVisible(false);
    backgrounds[i]->setPosition(ccp(win_size.width/2, win_size.height/2));

    backgrounds[i]->setScale(scale_factor);
    addChild(backgrounds[i]);

    foregrounds[i]->setScale(scale_factor);
    addChild(foregrounds[i],3);
  }

  char path[100], path_p[100];
  sprintf(path,"images/buttons/background_%i.png",getResourceWidth());
  CCSprite *button_l_background = CCSprite::create(path);
  button_r_background = CCSprite::create(path);
  button_l_background->setScale(scale_factor);
  button_r_background->setScaleX(-scale_factor);
  button_r_background->setScaleY(-scale_factor);
  addChild(button_l_background,4);
  addChild(button_r_background,4);

  score_label = CCLabelTTF::create("", tr(FontName), win_size.height * 0.1);
  score_label->setColor(ccBLACK);
  addChild(score_label,4);

  time_label = CCLabelTTF::create("", tr(FontName), win_size.height * 0.1);
  time_label->setColor(ccBLACK);
  addChild(time_label,4);

  sprintf(path,"images/buttons/pause_%i.png",getResourceWidth());
  sprintf(path_p,"images/buttons/pause_pressed_%i.png",getResourceWidth());
  CCMenuItemImage *pause_button = CCMenuItemImage::create(
    path,path_p,this,menu_selector(GameView::menuPauseCallback));

  pause_button->setPosition(
    ccp(pause_button->getContentSize().width*0.61*scale_factor, 
        pause_button->getContentSize().height*0.6*scale_factor));
  pause_button->setScale(scale_factor);

  sprintf(path,"images/buttons/restart_%i.png",getResourceWidth());
  sprintf(path_p,"images/buttons/restart_pressed_%i.png",getResourceWidth());
  restart_button = CCMenuItemImage::create(
    path,path_p,this,menu_selector(GameView::menuRestartCallback));

  button_l_background->setPosition(
    ccp(pause_button->getContentSize().width*scale_factor*0.6, 
        pause_button->getContentSize().height*0.6*scale_factor));

  button_r_background->setPosition(
    ccp(pause_button->getContentSize().width*scale_factor*1.67, 
        pause_button->getContentSize().height*0.6*scale_factor));

  restart_button->setPosition(
    ccp(pause_button->getContentSize().width*scale_factor*1.66, 
        pause_button->getContentSize().height*0.6*scale_factor));
  restart_button->setScale(scale_factor);

  menu = CCMenu::create(pause_button,restart_button,NULL);
  menu->setPosition(CCPointZero);

  addChild(menu, 5);

  createBonusesMenu(scale_factor);

  schedule( schedule_selector(GameView::gameLogic), 1.0f / 60.0f );
  setTouchEnabled(true);
  setTouchMode(kCCTouchesOneByOne);
  setKeypadEnabled(true);

  scale = b2Min(win_size.width,win_size.height) / 8;

  shadow = CCSprite::create("images/menu/pause_shadow.png");
  shadow->setPosition(ccp(win_size.width/2,win_size.height/2));
  shadow->setScaleX(win_size.width);
  shadow->setScaleY(win_size.height);
  shadow->setOpacity(0);
  addChild(shadow, 5);
}

GameView::~GameView() {
}

void GameView::setGroupAndLevel( int group_, int level_ ) {
  if (group != -1) {
    backgrounds[group]->setVisible(false);
    foregrounds[group]->setVisible(false);
  }

  group = group_;
  level = level_;

  backgrounds[group]->setVisible(true);
  foregrounds[group]->setVisible(true);

  score_label->setVisible(level == -1 || level >= 1000);
  time_label->setVisible(level >= 1000);
  restart_button->setVisible(level < 1000);
  button_r_background->setVisible(level < 1000);

  if (level == -1) {
    if(!getViewSettings().isUserSawInfoMessage(ViewInterface::ClassicGameMod)) {
      newInfoMessage(ViewInterface::ClassicGameMod);
    }
  } else if (level >= 1000) {
    if (!getViewSettings().isUserSawInfoMessage(ViewInterface::InsaneGameMod)) {
      newInfoMessage(ViewInterface::InsaneGameMod);
    }
    LevelManager::LevelParameters lp = 
      LevelManager::LevelParameters::insaneLevelParameters(1);
    if (lp.allow_double_color && !getViewSettings().isUserSawInfoMessage(
      ViewInterface::GradientBubbleAppear)) {
        newInfoMessage(ViewInterface::GradientBubbleAppear);
        getViewSettings().setUserSawInfoMessage(
          ViewInterface::GradientBubbleAppear);
    }
    if (lp.allow_rubber_balls && !getViewSettings().isUserSawInfoMessage(
      ViewInterface::RubberBubbleAppear)) {
        newInfoMessage(ViewInterface::RubberBubbleAppear);
        getViewSettings().setUserSawInfoMessage(
          ViewInterface::RubberBubbleAppear);
    }
  }

  SoundsManager::get().playMusic(group,level >= 1000);

  for (size_t i=0; i<balls.size(); ++i) {
    ballUpdated(balls[i],ViewInterface::Color);
  }
}

void GameView::gameLogic(float dt) {
  if (observer) {
    observer->tick(b2Min(dt,1.0f/30));
  }
  for (size_t i=0; i<balls.size(); ++i) {
    BubbleAnimations::startRandomAnimation(balls[i]->animation_target);
  }
}

void GameView::pauseGame() {
  menuPauseCallback(NULL);
}

void GameView::onLanguageChanged() {
  if (level >= 1000) {
    leftBubblesChanged(left_bubbles);
  }
}

void GameView::clear() {
  removeGameMenuWithAnimation();
  hideShadow();
  SoundsManager::get().pauseMusic();
}

void GameView::ballCreated( BubbleParameters *ball ) {
  balls.push_back(ball);
  updateBubble(ball, false, true);
}

void GameView::ballDestroyed( BubbleParameters *ball ) {
  if (ball->is_exploding) {
    int sizes[] = {40,60,88,125,177,250,350,500};
    int i;
    for (i=0;i<sizeof(sizes)/sizeof(int)-1; ++i) {
      if (2*ball->radius*scale <= sizes[i]) {
        break;
      }
    }
    char path[100];
    sprintf(path,"images/circles/explosion_%i.png",sizes[i]);
    CCSprite *explosion = CCSprite::create(path);
    explosion->setPosition(ball->sprite->getPosition());
    explosion->setColor(ball->sprite->getColor());
    explosion->runAction(CCSequence::create(
      CCSpawn::create(CCScaleTo::create(1.0f,10),
                      CCEaseSineOut::create(CCFadeOut::create(1.0f)),NULL),
      CCRemoveSelf::create(),
      NULL));
    addChild(explosion);
  }
  balls.erase(std::remove(balls.begin(),balls.end(),ball),balls.end());
  removeChild(ball->sprite, true);

  BubbleSparks* m_emitter = new BubbleSparks(
    getColor4F(static_cast<Location>(group),ball->color),
    3*ball->radius*scale,ball->is_exploding);
  addChild(m_emitter);
  m_emitter->setPosition(translateToView(ball->position));
}

void GameView::ballUpdated(BubbleParameters *ball, BallUpdatedParameter param) {
  switch(param) {
  case Color: {
      ball->sprite->updateColor(
        static_cast<Location>(group),ball->color,ball->double_color);
    } break;
  case Availability: {
      ball->sprite->setZOrder(2);
      ball->sprite->setOpacity(0xFF,false);
    } break;
  }
  
}

void GameView::sparkCreated( Spark spark ) {
  float real_diameter = (spark.ball1->radius+spark.ball2->radius)*scale;
  int sizes[] = {40,60,88,125,177,250,350,500};
  int i;
  for (i=0;i<sizeof(sizes)/sizeof(int)-1; ++i) {
    if (real_diameter <= sizes[i]) {
      break;
    }
  }
  char path[100];
  sprintf(path,"images/circles/spark_%i.png",sizes[i]);

  spark.bubble_sparks = CCSprite::create(path);
  spark.bubble_sparks->setPosition(translateToView(spark.getPosition()));
  CCSprite *colored = CCSprite::create(path);
  ccColor3B color = 
      getColor3B(static_cast<Location>(group),
                 spark.ball1->color == spark.ball2->color ? spark.ball1->color : 
                  (spark.ball1->double_color == spark.ball2->color ? 
                    spark.ball1->double_color : spark.ball2->double_color));
  colored->setColor(color);
  colored->setPosition(ccp(spark.bubble_sparks->getContentSize().width/2,
                           spark.bubble_sparks->getContentSize().height/2));
  colored->setOpacity(50);
  spark.bubble_sparks->addChild(colored);
  float scale = real_diameter / spark.bubble_sparks->getContentSize().width;
  spark.bubble_sparks->setScale(0);
  spark.bubble_sparks->setRotation(rand()%45);
  spark.bubble_sparks->runAction(CCScaleTo::create(0.3f,scale));
  spark.bubble_sparks->runAction(
    CCRepeatForever::create(CCRotateBy::create(1,20,20)));
  addChild(spark.bubble_sparks,3);
  sparks.push_back(spark);
}

void GameView::sparkDestroyed( Spark spark ) {
  for(std::list<Spark>::iterator it = sparks.begin(); it != sparks.end(); ++it){
    if (*it == spark) {
      it->bubble_sparks->runAction(
        CCSequence::create(CCFadeOut::create(0.2f),
                           CCRemoveSelf::create(),NULL));
      sparks.erase(it);
      return;
    }
  }
}

void GameView::newInfoMessage( InfoMessage info ) {
  if (info_menu_item) {
    info_queue.push_back(info);
    return;
  }
  char path[100];
  sprintf(path,"images/menu/new_%i.png",getResourceWidth());
  CCSprite *new_info_sprite = CCSprite::create(path);
  CCSprite *shining_sprite = NULL, *bubble_sprite = NULL;

  if (info < BubbleAppearFinish) {
    sprintf(path,"images/menu/new_bubble_shining_%i.png",getResourceWidth());
    shining_sprite = CCSprite::create(path);
    shining_sprite->setScale(backgrounds[0]->getScale());

    float real_diameter = 0.66*(translateToWorld(
      ccp(shining_sprite->getContentSize().width*backgrounds[0]->getScale(),0))
      - translateToWorld(ccp(0,0))).x*scale;
    bubble_sprite = new Bubble(info,real_diameter,static_cast<Location>(group));
  }

  float new_info_scale = backgrounds[0]->getScale();
  new_info_sprite->setScale(backgrounds[0]->getScale());
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  new_info_sprite->setPosition(ccp(
    new_info_sprite->getContentSize().width*new_info_scale/2,
    (shining_sprite?
      shining_sprite->getContentSize().height*new_info_scale*0.7:0)
    + new_info_sprite->getContentSize().height*new_info_scale/2));
  if (shining_sprite) {
    shining_sprite->setPosition(ccp(
     new_info_sprite->getContentSize().width*new_info_scale/2,
     new_info_sprite->getContentSize().height*new_info_scale/2));
    bubble_sprite->setPosition(shining_sprite->getPosition());
  }
  info_menu_item = CCMenuItemImage::create(
    NULL,
    NULL,
    this,
    menu_selector(GameView::menuNewInfoCallback));
  info_menu_item->setTag(info);
  info_menu_item->setContentSize(CCSize(
    new_info_sprite->getContentSize().width*new_info_scale,
    new_info_sprite->getContentSize().height*new_info_scale 
    + (shining_sprite?shining_sprite->getContentSize().height*new_info_scale:0)
    ));
  info_menu_item->addChild(new_info_sprite);
  if (shining_sprite) {
    info_menu_item->addChild(shining_sprite);
    info_menu_item->addChild(bubble_sprite);
  }
  info_menu_item->setPosition(ccp(
    win_size.width - info_menu_item->getContentSize().width*0.7,
    info_menu_item->getContentSize().height*0.7));
  CCFiniteTimeAction *action1 = CCScaleTo::create(1,1.15*new_info_scale);
  CCFiniteTimeAction *action2 = CCScaleTo::create(3,new_info_scale);
  new_info_sprite->runAction(
    CCRepeatForever::create(CCSequence::create(action1,action2,NULL)));
  if (shining_sprite) {
    shining_sprite->runAction(
      CCRepeatForever::create(CCRotateBy::create(1,10,10)));
  }
  
  menu->addChild(info_menu_item);

  // If user don't know some important info, we should immediately tell it
  if (info > BubbleAppearFinish 
      && !getViewSettings().isUserSawInfoMessage(info)) {
    menuNewInfoCallback(info_menu_item);
    getViewSettings().setUserSawInfoMessage(info);
  }
}

void GameView::bonusAppeared( Bonus type ) {
  if (!bonuses_buttons[type]->isVisible()) {
    SoundsManager::get().playSound(BonusAppearedSound);
  }
  if (observer) {
    observer->bonusPicked(type);
  }
}

void GameView::hideBonuses() {
  for (Bonus i=static_cast<Bonus>(0); 
    i<BonusesNumber; i=static_cast<Bonus>(i+1) ) {
    bonuses_buttons[i]->setVisible(false);
  }
}

void GameView::bonusNumberUpdated( bool *bonuses_number ) {
  int visible_bonuses = 0;
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  for (Bonus i=static_cast<Bonus>(0); 
       i<BonusesNumber; i=static_cast<Bonus>(i+1) ) {
    if (bonuses_number[i]) {
      bool was_visible = bonuses_buttons[i]->isVisible();
      float scale = bonuses_buttons[i]->getScale();
      CCPoint pos = ccp(
        win_size.width - bonuses_buttons[i]->getContentSize().width*
                          scale*1.2*(visible_bonuses+0.6),
        win_size.height - bonuses_buttons[i]->getContentSize().height*
                          scale*0.7);
      visible_bonuses++;
      if (!was_visible) {
        // become available
        bonuses_buttons[i]->setPosition(pos);
        bonuses_buttons[i]->setScale(0);
        bonuses_buttons[i]->runAction(CCScaleTo::create(0.2f,scale));
      } else {
        bonuses_buttons[i]->runAction(CCMoveTo::create(0.2f,pos));
      }
    }
    bonuses_buttons[i]->setVisible(bonuses_number[i]);
  }
}

void GameView::leftTimeChanged( float time_left ) {
  char time_string[50];
  sprintf(time_string,"%i.%i",
          static_cast<int>(time_left),static_cast<int>(time_left*10)%10);
  if (time_left < 10) {
    time_label->setColor(ccRED);
    if (static_cast<int>(time_left) != insane_time_left) {
      SoundsManager::get().playSound(static_cast<SoundEffect>(
        Countdown1+static_cast<int>(time_left)));
    }
  } else {
    time_label->setColor(ccBLACK);
  }
  time_label->setString(time_string);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  time_label->setPosition(
    ccp(time_label->getContentSize().width/2+win_size.width*0.01,
        win_size.height - score_label->getContentSize().height 
                        - time_label->getContentSize().height/2));
  insane_time_left = time_left;
}

void GameView::leftBubblesChanged( int left_bubbles_ ) {
  left_bubbles = left_bubbles_;
  char left_bubbles_string[50];
  sprintf(left_bubbles_string,tr(BubblesLeft),left_bubbles);
  score_label->setString(left_bubbles_string);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  score_label->setPosition(
    ccp(score_label->getContentSize().width/2+win_size.width*0.01,
        win_size.height - score_label->getContentSize().height/2));
}

void GameView::updateBubble(
  BubbleParameters *ball, bool remove_before, bool add_after) {
  if (remove_before) {
    removeChild(ball->sprite, true);
  }

  ball->sprite = new Bubble(ball,scale,static_cast<Location>(group));

  ball->sprite->setPosition( translateToView(ball->position) );
  if (add_after) {
    if (ball->is_availible) {
      addChild(ball->sprite, 2);
    } else {
      addChild(ball->sprite, 1);
    }
  }
}

void GameView::worldChanged() {
  for(size_t i=0; i<balls.size(); ++i) {
    balls[i]->sprite->setPosition( translateToView(balls[i]->position) );
  }
  for(std::list<Spark>::iterator it = sparks.begin(); it != sparks.end(); ++it){
    it->bubble_sparks->setPosition(translateToView(it->getPosition()));
  }
}

void GameView::gameFinished( int balls_left, int previous_stars_count ) {
  if (!game_menu) {
    showShadow();
    if (level < 0) {
      game_menu = GameMenu::createEndGameMenu(level_score);
    } else if (level >= 1000) {
      int record = GameProgress::getInsaneLevelRecord();
      bool new_record = false;
      if (record < level%1000) {
        new_record = true;
        GameProgress::setInsaneLevelRecord(level%1000);
      }
      game_menu = GameMenu::createEndGameMenu(
        level%1000,level_score,balls_left*10,new_record);
    } else {
      game_menu = GameMenu::createEndGameMenu(
        3-balls_left, previous_stars_count, level < LEVELS_IN_GROUP-1);
    }
    handleGameMenuCreation();
  }
}

void GameView::levelChanged( int new_group, int new_level ) {
  level_score = 0;
  if (new_level == -1) {
    level = new_level; // hack
    onScoreChanged(score/10,0); // update score label
  } else if (new_group == group && level == new_level) {
    if (info_menu_item) {
      info_queue.clear();
      menu->removeChild(info_menu_item,true);
      info_menu_item = NULL;
    }
  } else {
    if (new_group >= 0) {
      char level_number_string[100];
      if (new_level >= 1000) {
        char format_string[100];
        sprintf(format_string,"%s\n%s",tr(LevelNumber),tr(BestLevelNumber));
        sprintf(level_number_string,format_string,
                new_level%1000+1,GameProgress::getInsaneLevelRecord()+1);
      } else {
        sprintf(level_number_string,tr(LevelNumber),new_level+1);
      }
      GlobalInfo::get()->queueText(level_number_string);
    }
  }
  setGroupAndLevel(new_group,new_level);
  if (new_level < 1000) {
    hideBonuses();
  }
}

void GameView::gameResumed() {
  SoundsManager::get().playMusic(group,level >= 1000);
}

void GameView::bubbleTouchedBubble( float power, bool rubber ) {
  SoundsManager::get().collision(power,rubber,false);
}

void GameView::bubbleTouchedGround( float power, bool rubber ) {
  SoundsManager::get().collision(power,rubber,true);
}

void GameView::bubbleExploded() {
  SoundsManager::get().playSound(BubbleExploded);
}

void GameView::bubbleDisappeared() {
  SoundsManager::get().playSound(BubbleDisappeared);
}

void GameView::bubbleBecomeSmaller() {
  SoundsManager::get().playSound(BubbleBecomeSmaller);
}

void GameView::aloneBubbleClicked(BubbleParameters *ball) {
  BubbleAnimations::startAngryAnimation(ball->animation_target);
  SoundsManager::get().playSound(AloneBubbleClicked);
}

void GameView::resumeButtonPressed() {
  if (game_menu) {
    hideShadow();
    removeGameMenuWithAnimation();
    if (observer) {
      observer->resumeButtonPressed();
    }
    SoundsManager::get().playMusic(group,level >= 1000);
  }
}

void GameView::restartButtonPressed() {
  if (observer) {
    observer->restartButtonPressed();
  }
  if (game_menu) {
    hideShadow();
    removeGameMenuWithAnimation();
  }
}

void GameView::nextLevelButtonPressed() {
  if (game_menu) {
    hideShadow();
    removeGameMenuWithAnimation();
  }
  if (observer) {
    observer->nextLevelButtonPressed();
  }
}

void GameView::menuButtonPressed() {
  if (observer) {
    if (level < 0 || level >= 1000) {
      observer->toMenuButtonPressed();
    } else {
      observer->toLevelsButtonPressed();
    }
  }
}

void GameView::onScoreChanged( int new_score, int delta ) {
  if (level >= 0 && level < 1000) return;
  new_score *= 10; delta *= 10; // Makes user 10 times happier
  score = new_score;
  level_score += delta;

  char score_string[50];
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  if (level == -1) {
    sprintf(score_string,tr(ScoreString),new_score);
    score_label->setString(score_string);
    score_label->setPosition(
      ccp(score_label->getContentSize().width/2+win_size.width*0.01,
          win_size.height - score_label->getContentSize().height/2));
    score_label->runAction(CCSequence::create(CCScaleTo::create(0.1f,1.05f),
                                              CCScaleTo::create(0.1f,1),NULL));
  }

  if (last_touch.x == 0 && last_touch.y == 0) return; // First touch

  if (delta) {
    CCLabelTTF *score_change_label = 
      CCLabelTTF::create("", tr(FontName), win_size.height*0.15);
    score_change_label->setColor(ccORANGE);
    addChild(score_change_label,5);
    sprintf(score_string,tr(IncreaseScoreString),delta);
    score_change_label->setString(score_string);
    score_change_label->setPosition(last_touch);
    score_change_label->setOpacity(255);
    CCPoint new_position = last_touch;
    new_position.y += win_size.height * 0.3f;
    score_change_label->runAction(
      CCSequence::create(
        CCSpawn::create(
          CCSequence::create(
            CCDelayTime::create(0.5f),
            CCFadeTo::create(1.0f,0),
            NULL
          ),   
          CCMoveTo::create(1.5f,new_position),
          NULL
        ),
        CCRemoveSelf::create(),
        NULL
      )
    );
  }
}


bool GameView::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent ) {
  last_touch = pTouch->getLocation();
  if (observer) {
    observer->touched(translateToWorld(pTouch->getLocation()));
  }
  return true;
}

void GameView::keyBackClicked() {
  if (game_menu) {
    hideShadow();
    removeGameMenuWithAnimation();

    if (observer) {
      observer->resumeButtonPressed();
    }
  } else {
    menuPauseCallback(NULL);
  }
}

CCPoint GameView::translateToView( b2Vec2 point ) {
  point = scale * point;
  point.x += getContentSize().width/2;
  return CCPoint(point.x, point.y);
}

b2Vec2 GameView::translateToWorld(CCPoint point) {
  b2Vec2 pointf(point.x,point.y);
  pointf.x -= getContentSize().width/2;
  pointf = 1.0f / scale * pointf;
  return pointf;
}

void GameView::bonusCallback( CCObject* sender ) {
  if (observer) {
    for (Bonus i = static_cast<Bonus>(0);
      i < BonusesNumber; i = static_cast<Bonus>(i+1)) {
      if (sender == bonuses_buttons[i]) {
        observer->bonusButtonPressed(i);
        SoundEffect effect;
        switch(i) {
          case ShufflerBonus: effect = ShufflerSound; break;
          case SplitterBonus: effect = SplitterSound; break;
          case EarthquakeBonus: effect = EarthquakeSound; break;
          case ExploserBonus: effect = BubbleExploded; break;
          case BubbleRainBonus: effect = BubbleRainSound; break;
          case CreatorBonus: effect = CreatorSound; break;
        }
        if (effect != CreatorSound) {
          // CreatorSound is managed from BonusManager
          SoundsManager::get().playSound(effect);
        }
        return;
      }
    }
  }
}

void GameView::menuPauseCallback(CCObject* ) {
  if (!game_menu) {
    if (observer) {
      observer->pauseButtonPressed();
    }
    showShadow();
    if (level == -1) {
      game_menu = GameMenu::createPauseMenu(level_score);
    } else if (level >= 1000) {
      game_menu = GameMenu::createPauseMenu(level%1000,level_score);
    } else {
      int star_count = GameProgress::getLevelProgress(group,level).star_count;
      game_menu = 
        GameMenu::createPauseMenu(star_count,level < LEVELS_IN_GROUP-1);
    }
    handleGameMenuCreation(ccp(0,0));
    SoundsManager::get().pauseMusic();
  }
}

void GameView::menuRestartCallback(CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  restartButtonPressed();
}

void GameView::menuNewInfoCallback( CCObject* sender ) {
  SoundsManager::get().playSound(ButtonSound);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  if (info_menu_item) {
    info_menu_item->stopAllActions();
    info_menu_item->runAction(CCSequence::createWithTwoActions(
      CCMoveTo::create(0.2f,ccp(win_size.width/2,win_size.height/2)),
      CCRemoveSelf::create(true)));
  }
  if (game_menu) {
    removeChild(game_menu);
  } else {
    showShadow();
  }
  game_menu = GameMenu::createNewInfoMenu(
    static_cast<InfoMessage>(static_cast<CCMenuItem*>(sender)->getTag()),
    static_cast<Location>(group));
  if (info_menu_item) {
    handleGameMenuCreation(info_menu_item->getPosition());
    info_menu_item = NULL;
  } else {
    handleGameMenuCreation(ccp(win_size.height,0));
  }

  if (observer) {
    observer->pauseButtonPressed();
  }
  if (!info_queue.empty()) {
    InfoMessage im = info_queue.front();
    info_queue.pop_front();
    newInfoMessage(im);
  }
}

void GameView::handleGameMenuCreation(CCPoint point) {
  game_menu->setScale(0);
  game_menu->setObserver(this);
  game_menu->setPosition(point);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  game_menu->runAction(CCSpawn::createWithTwoActions(
    CCMoveTo::create(0.2f,ccp(win_size.width/2,win_size.height/2)),
    CCScaleTo::create(0.2f,1)));
  addChild(game_menu,100);
}

void GameView::handleGameMenuCreation() {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  handleGameMenuCreation(ccp(win_size.width/2,win_size.height));
}

void GameView::createBonusesMenu(float scale) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  CCArray buttons;
  char border_path[100],border_path_p[100],
       bg_path[100],bg_path_p[100],path[100];
  sprintf(border_path,"images/bonuses/border_%i.png",getResourceWidth());
  sprintf(border_path_p,
          "images/bonuses/border_pressed_%i.png",getResourceWidth());
  sprintf(bg_path,"images/bonuses/background_%i.png",getResourceWidth());
  sprintf(bg_path_p,
          "images/bonuses/background_pressed_%i.png",getResourceWidth());
  for (unsigned i=0; i<BonusesNumber; ++i) {
    CCMenuItemImage *item = bonuses_buttons[i] = CCMenuItemImage::create(
      NULL,NULL,this,menu_selector(GameView::bonusCallback));

    item->setVisible(false);
    CCSprite *bg = CCSprite::create(bg_path);
    CCSprite *bg_pressed = CCSprite::create(bg_path_p);
    CCPoint pos(bg_pressed->getContentSize().width/2,
                bg_pressed->getContentSize().height/2);

    sprintf(path,"images/bonuses/%i_%i.png",i,getResourceWidth());
    CCSprite *image = CCSprite::create(path);
    image->setPosition(pos);
    CCSprite *image_p = CCSprite::create(path);
    image_p->setPosition(pos);
    bg->addChild(image);
    bg_pressed->addChild(image_p);

    CCSprite *border = CCSprite::create(border_path);
    border->setPosition(pos);
    CCSprite *border_pressed = CCSprite::create(border_path_p);
    border_pressed->setPosition(pos);
    bg->addChild(border);
    bg_pressed->addChild(border_pressed);

    item->setNormalImage(bg);
    item->setSelectedImage(bg_pressed);

    item->setScale(scale);
    buttons.addObject(item);
  }

  CCMenu* bonuses_menu = CCMenu::createWithArray(&buttons);
  bonuses_menu->setPosition(CCPointZero);
  addChild(bonuses_menu, 4);
}

void GameView::showShadow() {
  shadow->runAction(CCFadeIn::create(0.2f));
}

void GameView::hideShadow() {
  shadow->runAction(CCFadeOut::create(0.2f));
}

void GameView::removeGameMenuWithAnimation() {
  SoundsManager::get().restoreMusicVolume(); // hack
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  game_menu->runAction(CCSequence::create(
    CCSpawn::create(CCScaleTo::create(0.2f,0),
                    CCMoveTo::create(0.2f,ccp(win_size.width/2,0)),
                    NULL),
    CCRemoveSelf::create(),
    NULL
  ));
  game_menu = NULL;
}
