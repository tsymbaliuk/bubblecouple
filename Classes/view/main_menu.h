#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include "cocos2d.h"

using namespace cocos2d;

class MainMenu : public CCLayerColor
{
public:
  MainMenu(float scale);
  ~MainMenu();
  class Observer {
  public:
    virtual void startCampaign() = 0;
    virtual void startInsane() = 0;
    virtual void startClassic() = 0;
    virtual void showInfo() = 0;
    virtual void showSettings() = 0;
    virtual void quit() = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
  void onLanguageChanged();
private:
  void startCampaignCallback(cocos2d::CCObject* pSender);
  void startInsaneCallback(cocos2d::CCObject* pSender);
  void startClassicCallback(cocos2d::CCObject* pSender);
  void infoCallback(cocos2d::CCObject* pSender);
  void settingsCallback(cocos2d::CCObject* pSender);
  Observer *observer;

  CCLabelTTF *campaign_label, *insane_label, *classic_label;

  virtual void keyBackClicked();
};

#endif // MAIN_MENU_H