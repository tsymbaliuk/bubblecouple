#include "view/colors.h"

#include "Box2D/Box2D.h"

static const 
  b2Color location_colors[TotalLocations][BubbleParameters::TotalColors] = {
  { // DesertLocation
    b2Color(0.188f,0.416f,0.682f), // 8
    b2Color(1.0f,0.863f,0.286f), // 2
    b2Color(0.725f,0.180f,0.180f), // 6
    b2Color(0.09f,0.08f,0.11f), // 10
    b2Color(0.81f,0.81f,0.81f) // 5
  },
  { // TownLocation
    b2Color(0.906f,0.486f,0.918f), // 4
    b2Color(0.725f,0.180f,0.180f), // 6
    b2Color(0.522f,0.745f,0.169f), // 7
    b2Color(0.09f,0.08f,0.11f), // 10
    b2Color(0.81f,0.81f,0.81f) // 5
  },
  { // MountainsLocation
    b2Color(0.882f,0.471f,0.055f), // 1
    b2Color(0.725f,0.180f,0.180f), // 6
    b2Color(0.365f,0.188f,0.784f), // 9
    b2Color(0.09f,0.08f,0.11f), // 10
    b2Color(0.81f,0.81f,0.81f) // 5
  },
  { // TunguskaLocation
    b2Color(0.188f,0.416f,0.682f), // 8
    b2Color(1.0f,0.863f,0.286f), // 2
    b2Color(0.725f,0.180f,0.180f), // 6
    b2Color(0.09f,0.08f,0.11f), // 10
    b2Color(0.81f,0.81f,0.81f) // 5
  }
};

/*
b2Color(0.882f,0.471f,0.055f), // 1
b2Color(1.0f,0.863f,0.286f), // 2
b2Color(0.651f,0.874f,0.910f), // 3
b2Color(0.906f,0.486f,0.918f), // 4
b2Color(0.81f,0.81f,0.81f), // 5
b2Color(0.725f,0.180f,0.180f), // 6
b2Color(0.522f,0.745f,0.169f), // 7
b2Color(0.188f,0.416f,0.682f), // 8
b2Color(0.365f,0.188f,0.784f), // 9
b2Color(0.09f,0.08f,0.11f) // 10 
*/

static const 
  b2Color complement_colors[TotalLocations][BubbleParameters::TotalColors] = {
  { // DesertLocation
    b2Color(0.651f,0.874f,0.910f), // 3
    b2Color(0.522f,0.745f,0.169f), // 7
    b2Color(0.882f,0.471f,0.055f), // 1
    b2Color(0.81f,0.81f,0.81f), // 5
    b2Color(0.09f,0.08f,0.11f) // 10 
  },
  { // TownLocation
    b2Color(0.725f,0.180f,0.180f), // 6
    b2Color(0.882f,0.471f,0.055f), // 1
    b2Color(1.0f,0.863f,0.286f), // 2
    b2Color(0.81f,0.81f,0.81f), // 5
    b2Color(0.09f,0.08f,0.11f) // 10
  },
  { // MountainsLocation
    b2Color(0.81f,0.81f,0.81f), // 5
    b2Color(0.882f,0.471f,0.055f), // 1
    b2Color(0.906f,0.486f,0.918f), // 4
    b2Color(0.81f,0.81f,0.81f), // 5
    b2Color(0.09f,0.08f,0.11f) // 10
  },
  { // TunguskaLocation
    b2Color(0.651f,0.874f,0.910f), // 3
    b2Color(0.522f,0.745f,0.169f), // 7
    b2Color(0.882f,0.471f,0.055f), // 1
    b2Color(0.81f,0.81f,0.81f), // 5
    b2Color(0.09f,0.08f,0.11f) // 10 
  }
};

cocos2d::ccColor4F getColor4F(
  Location location, BubbleParameters::Color number ) {
  cocos2d::ccColor4F color =
  {
    location_colors[location][number].r,
    location_colors[location][number].g,
    location_colors[location][number].b,
    1.0f
  };
  return color;
}

cocos2d::ccColor3B getColor3B(
  Location location, BubbleParameters::Color number ) {
  cocos2d::ccColor3B color =
  {
    location_colors[location][number].r*255,
    location_colors[location][number].g*255,
    location_colors[location][number].b*255
  };
  return color;
}

cocos2d::ccColor3B getComplement3B( 
  Location location, BubbleParameters::Color number ) {
  cocos2d::ccColor3B color =
  {
    complement_colors[location][number].r*255,
    complement_colors[location][number].g*255,
    complement_colors[location][number].b*255
  };
  return color;
}
