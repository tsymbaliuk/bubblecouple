#include "view/levels_menu.h"

#include "model/game_progress.h"
#include "view/global_info.h"
#include "view/resource_loader.h"
#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/view_settings.h"

LevelsMenu::LevelsMenu(CCNode *parent) : MenuInterface(parent),
  current_touch(NULL), observer(NULL), 
  current_level_group(getViewSettings().getCurrentLevelGroup()) {
  setInternalObserver(this);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  char path[100], path_p[100];
  sprintf(path,"images/menu/star_%i.png",getResourceWidth());
  star = CCSprite::create(path);
  CCSize star_size = star->getContentSize();
  float star_scale = foreground_height*0.06 / star_size.height;
  star->setScale(star_scale);
  star->setPosition(ccp(win_size.width*0.5 + foreground_width*0.32,
                        win_size.height*0.5 - foreground_height*0.36));
  addChild(star);

  stars_number_label = 
    CCLabelTTF::create("",tr(FontName),foreground_height*0.075);
  updateStarsNumberLabel();
  addChild(stars_number_label);

  CCMenu* buttons_menu = CCMenu::create();
  sprintf(path,"images/menu/left_button_%i.png",getResourceWidth());
  sprintf(path_p,"images/menu/left_button_pressed_%i.png",getResourceWidth());
  CCMenuItemImage *back = CCMenuItemImage::create(
    path,path_p,this,menu_selector(LevelsMenu::switchToPrevBox));
  back->setScale(win_size.width / getResourceWidth());
  back->setPosition(ccp(
    win_size.width/2 - foreground_width/2.65f,win_size.height/2));
  buttons_menu->addChild(back);
  CCMenuItemImage *forward = CCMenuItemImage::create(
    path,path_p,this,menu_selector(LevelsMenu::switchToNextBox));
  forward->setScale(win_size.width / getResourceWidth());
  forward->setScaleX(-forward->getScaleX());
  forward->setPosition(ccp(
    win_size.width/2 + foreground_width/2.65f,win_size.height/2));
  buttons_menu->addChild(forward);
  buttons_menu->setPosition(ccp(0,0));
  addChild(buttons_menu);

  CCMenu* selectors_menu = CCMenu::create();
  for (int i=0; i < LEVEL_GROUPS_COUNT; ++i) {
    sprintf(path,"images/menu/selector_%i.png",i);
    level_icons[i] = CCMenuItemImage::create(
      path,path,this,menu_selector(LevelsMenu::switchToBox));
    level_icons[i]->setScale(
      foreground_height * 0.05 / level_icons[i]->getContentSize().height);
    level_icons[i]->setPosition(ccp(
      win_size.width/2 - foreground_width/8 -
        (0.5*(LEVEL_GROUPS_COUNT-1)-i)*90*level_icons[i]->getScale(),
      win_size.height/2 - foreground_height*0.36));
    selectors_menu->addChild(level_icons[i],0,i);
  }
  selectors_menu->setPosition(ccp(0,0));
  addChild(selectors_menu);

  content->setPosition(-current_level_group*win_size.width, 0);

  for (int i=0; i<LEVEL_GROUPS_COUNT; ++i) {
    levels_submenus[i] = 
      new LevelsSubmenu(this,i,foreground_width,foreground_height);
    levels_submenus[i]->setPosition(i*foreground_width,0);
    content->addChild(levels_submenus[i]);
  }

  provideSwitch();
}

void LevelsMenu::updateStars() {
  for (int i=0; i<LEVEL_GROUPS_COUNT; ++i) {
    levels_submenus[i]->update(true);
  }
  updateStarsNumberLabel();
}

void LevelsMenu::onLanguageChanged() {
  setHeaderText(tr(static_cast<TrId>(Desert+current_level_group)));
  updateStarsNumberLabel();
  for (int i=0; i < LEVEL_GROUPS_COUNT; ++i) {
    levels_submenus[i]->onLanguageChanged();
  }
}

void LevelsMenu::updateStarsNumberLabel() {
  char stars_number_text[50];
  sprintf(stars_number_text,tr(Total),GameProgress::getTotalStarsCount());
  stars_number_label->setString(stars_number_text);
  stars_number_label->setPosition(
    ccp(star->getPosition().x-star->getContentSize().width*star->getScale()/1.7
          - stars_number_label->getContentSize().width/2,
        star->getPosition().y));
}

void LevelsMenu::startLevelCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->startLevel(static_cast<CCMenuItem*>(sender)->getTag() / 100,
                         static_cast<CCMenuItem*>(sender)->getTag() % 100);
  }
}

void LevelsMenu::quitMenu() {
  if (observer) {
    observer->quitLevelSelection();
  }
}

void LevelsMenu::provideSwitch() {
  getViewSettings().setCurrentLevelGroup(current_level_group);
  CCFiniteTimeAction* actionMove = 
    CCMoveTo::create( 0.5, ccp(-current_level_group*foreground_width, 0) );
  content->stopAllActions();
  content->runAction( actionMove );

  for (int i=0; i < LEVEL_GROUPS_COUNT; ++i) {
    float scale = foreground_height * 0.05 
                  / level_icons[i]->getContentSize().height;
    if (i != current_level_group) {
      scale /= 1.3f;
    }
    level_icons[i]->stopAllActions();
    level_icons[i]->runAction(CCSpawn::createWithTwoActions(
      CCScaleTo::create(0.3f,scale),
      CCFadeTo::create(0.3f,i==current_level_group?255:150)));
  }

  setHeaderText(tr(static_cast<TrId>(Desert+current_level_group)));
}

void LevelsMenu::switchToBox( cocos2d::CCObject* sender ) {
  SoundsManager::get().playSound(ButtonSound);
  current_level_group = static_cast<CCMenuItem*>(sender)->getTag();
  provideSwitch();
}

void LevelsMenu::switchToNextBox(CCObject*) {
  SoundsManager::get().playSound(ButtonSound);
  current_level_group = (current_level_group+1)%LEVEL_GROUPS_COUNT;
  provideSwitch();
}

void LevelsMenu::switchToPrevBox(CCObject*) {
  SoundsManager::get().playSound(ButtonSound);
  current_level_group = 
    (current_level_group+LEVEL_GROUPS_COUNT-1)%LEVEL_GROUPS_COUNT;
  provideSwitch();
}

bool LevelsMenu::ccTouchBegan( CCTouch *touch, CCEvent *event ) {
  if (current_touch != NULL) {
    return false;
  } else {
    current_touch = touch;
    return true;
  }
}

void LevelsMenu::ccTouchMoved( CCTouch *touch, CCEvent *event ) {
  if (touch != current_touch) return;
  content->stopAllActions();
  content->setPosition(
    -current_level_group*foreground_width
    -touch->getStartLocation().x+touch->getLocation().x, 0);
}

void LevelsMenu::ccTouchEnded( CCTouch *touch, CCEvent *event ) {
  if (touch != current_touch) return;
  current_touch = NULL;

  float delta_x = -touch->getStartLocation().x+touch->getLocation().x;
  if (fabs(delta_x) > foreground_width/8) {
    if (delta_x < 0) {
      if (current_level_group != LEVEL_GROUPS_COUNT-1) {
        current_level_group++;
      }
    } else {
      if (current_level_group != 0) {
        current_level_group--;
      }
    }
  }
  provideSwitch();
}

void LevelsMenu::ccTouchCancelled( CCTouch *touch, CCEvent *event ) {
  ccTouchEnded(touch,event);
}

#ifdef ANDROID
void LevelsMenu::starsNumberUpdateStarted() {
}

void LevelsMenu::starsNumberUpdated(int new_stars_count) {
  GameProgress::setOfferwallStarsCount(new_stars_count);
  updateStars();
}

void LevelsMenu::starsNumberUpdateFailed() {
  GlobalInfo::get()->queueText("Network error occurred");
}
#endif

LevelsSubmenuMenu::LevelsSubmenuMenu(LevelsMenu *levels_menu) : 
  levels_menu(levels_menu), tracked_touch(NULL), translate_to_ccmenu(false) {
  if (initWithArray(NULL)) {
    autorelease();
  }
}
bool LevelsSubmenuMenu::ccTouchBegan(CCTouch* touch, CCEvent* event) {
  if (tracked_touch == NULL) {
    translate_to_ccmenu = CCMenu::ccTouchBegan(touch,event);
    // In this case LevelsMenu will handle touch
    if (!translate_to_ccmenu) return false; 
    tracked_touch = touch;
    levels_menu->ccTouchBegan(touch,event);
    return true;
  } else {
    return false;
  }
};
void LevelsSubmenuMenu::ccTouchEnded(CCTouch* touch, CCEvent* event) {
  if (tracked_touch == touch) {
    tracked_touch = NULL;
    levels_menu->ccTouchEnded(touch,event);
    if (translate_to_ccmenu) {
      CCMenu::ccTouchEnded(touch,event);
    }
  }
};
void LevelsSubmenuMenu::ccTouchCancelled(CCTouch *touch, CCEvent* event) {
  if (tracked_touch == touch) {
    if (translate_to_ccmenu) {
      CCMenu::ccTouchCancelled(touch,event);
    }
    levels_menu->ccTouchCancelled(touch,event);
  }
};
void LevelsSubmenuMenu::ccTouchMoved(CCTouch* touch, CCEvent* event) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  float delta_x = -touch->getStartLocation().x+touch->getLocation().x;
  if (fabs(delta_x) > win_size.width/20) {
    if (translate_to_ccmenu) {
      CCMenu::ccTouchCancelled(touch,event);
      translate_to_ccmenu = false;
    }
  } else {
    if (translate_to_ccmenu) {
      CCMenu::ccTouchMoved(touch,event);
    }
  }
  levels_menu->ccTouchMoved(touch,event);
};

LevelsSubmenu::LevelsSubmenu(
  LevelsMenu *levels_menu, int level_group, float width, float height) :
  levels_menu(levels_menu), level_group(level_group), locked_background(NULL), 
  star_int_menu(NULL), stars_locked_desc_label(NULL), stars_number_label(NULL) {
  CCLayer::init();
  autorelease();

  int stars_for_box = GameProgress::getStarsForBoxUnlock(level_group);

  CCMenu* menu = new LevelsSubmenuMenu(levels_menu);

  char path[100];
  for (int i=0; i<LEVELS_IN_GROUP; ++i) {
    LevelProgress progress = GameProgress::getLevelProgress(level_group,i);
    CCMenuItemImage *level_item;
    sprintf(path,
            "images/menu/selector_%i_%i.png",level_group,getResourceWidth());
    if (!progress.blocked || (i == 0 && stars_for_box <= 0)) {
      level_item = CCMenuItemImage::create(
        path,
        path,
        levels_menu,
        menu_selector(LevelsMenu::startLevelCallback));
    } else {
      level_item = CCMenuItemImage::create(path,path);
    }
    level_item->setCascadeOpacityEnabled(true);
    CCSize level_item_size = level_item->getContentSize();
    int star_count = progress.star_count;
    for (int j=0; j<3; ++j) {
      sprintf(path,"images/menu/star_%i.png",getResourceWidth());
      CCSprite *star = CCSprite::create(path);
      CCSize star_size = star->getContentSize();
      star->setPosition(
        ccp((level_item_size.width
             +(-star_count+1+2*j)*star_size.width)/2,star_size.height/2));
      level_item->addChild(star);
      star->setVisible(j<star_count);
      stars[i][j] = star;
    }
    char item_label_text[10];
    sprintf(item_label_text,"%i",i+1);
    CCLabelTTF* item_label = 
      CCLabelTTF::create(item_label_text, tr(FontName), getResourceWidth()/20);
    item_label->setColor(ccBLACK);
    item_label->setPosition(ccp(level_item_size.width/2,
                                level_item_size.height/2));
    level_item->addChild(item_label);

    float scale = 0.65 * height / (4*level_item_size.height);
    level_item->setScale(scale);

    locks[i] = NULL;
    if (progress.blocked && !(i == 0 && stars_for_box <= 0)) {
      sprintf(path,"images/menu/lock_%i.png",getResourceWidth());
      locks[i] = CCSprite::create(path);
      CCSize lock_size = locks[i]->getContentSize();
      locks[i]->setPosition(ccp(
        level_item_size.width-lock_size.width*1.2,
        lock_size.height/2));
      level_item->addChild(locks[i]);
    }

    int row = i / 5, column = i % 5;

    level_item->setPosition(ccp((-2+column)*(width*0.7/5),
                                -(-1.6+row)*(height*0.67/4)));

    if (stars_for_box > 0) {
      level_item->setOpacity(50);
    }

    menu->addChild(level_item,0,100*level_group+i);

    level_items[i] = level_item;
  }

  addChild(menu);

  if (stars_for_box > 0) {
    sprintf(path,"images/menu/locked_bg_mask_%i.png",getResourceWidth());
    CCSprite *mask_sprite = CCSprite::create(path);
    CCSprite *color_sprite = CCSprite::create(
      "images/menu/locked_bg_color.png",
      CCRect(0,0,
             mask_sprite->getContentSize().width,
             mask_sprite->getContentSize().height));
    locked_background = spriteFromMask(color_sprite,mask_sprite);
    locked_background->setScale(
      width * 0.7 / locked_background->getContentSize().width);
    locked_background->setPosition(
      ccp(getContentSize().width/2,getContentSize().height/2));
    addChild(locked_background);

    stars_locked_desc_label = 
      CCLabelTTF::create(
      tr(YouShouldEarnMoreStarsToUnlock), tr(FontName), 
      height*0.05, CCSize(width * 0.6 ,0), kCCTextAlignmentCenter);
    stars_locked_desc_label->setPosition(
      ccp(getContentSize().width/2,
          getContentSize().height/2+locked_background->getContentSize().height
                                    * locked_background->getScale()*0.28));
    addChild(stars_locked_desc_label);

    stars_number_label = CCLabelTTF::create("1", tr(FontName), height*0.08);
    addChild(stars_number_label);

    sprintf(path,"images/menu/star_%i.png",getResourceWidth());
    star_int_menu = CCSprite::create(path);
    float star_scale = stars_number_label->getContentSize().height*0.6 
                       / star_int_menu->getContentSize().height;
    star_int_menu->setScale(star_scale);
    addChild(star_int_menu);
#ifdef ANDROID
    char path_p[100];
    sprintf(path,"images/menu/button_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/button_pressed_%i.png",getResourceWidth());
    CCMenuItemImage *offerwall_item = CCMenuItemImage::create(
      path, path_p, this, menu_selector(LevelsSubmenu::goToOfferwall) );
    offerwall_item->setScale(locked_background->getScale()*0.8);
    offerwall_item->setPosition(ccp(0,-locked_background->getContentSize().height
                                      * locked_background->getScale()*0.2));

    free_stars_label = CCLabelTTF::create(
      tr(FreeStars), tr(FontName), 
      locked_background->getContentSize().height
      * locked_background->getScale()*0.12);
    const ccColor3B fontColor={0,23,39};
    free_stars_label->setColor(fontColor);
    free_stars_label->setPosition(ccp(
      getContentSize().width/2,
      getContentSize().height/2 - locked_background->getContentSize().height
                                  * locked_background->getScale()*0.2));
    addChild(free_stars_label,2);

    offerwall_menu = CCMenu::create( offerwall_item, NULL);
    addChild(offerwall_menu);
#else
    sprintf(path,
            "images/menu/selector_%i_%i.png",level_group,getResourceWidth());
    CCSprite *selector = CCSprite::create(path);
    sprintf(path,"images/menu/lock_big_%i.png",getResourceWidth());
    CCSprite *lock = CCSprite::create(path);
    lock->setPosition(ccp(selector->getContentSize().width/2,
                          selector->getContentSize().height/2.5));
    selector->addChild(lock);
    selector->setPosition(ccp(locked_background->getContentSize().width/2,
                              locked_background->getContentSize().height/3.5));
    locked_background->addChild(selector);
#endif
  }

  update(false);
}

void removeSelfWithFade(CCNode *sprite) {
  sprite->runAction(
    CCSequence::create(
      CCFadeTo::create(0.5f,0),
      CCRemoveSelf::create(),
      NULL
    )
  );
}

void LevelsSubmenu::update(bool with_animation) {
  int stars_for_box = GameProgress::getStarsForBoxUnlock(level_group);
  if (locked_background) {
    if (stars_for_box < 0) {
      removeSelfWithFade(locked_background);
      removeSelfWithFade(stars_locked_desc_label);
      removeSelfWithFade(stars_number_label);
      removeSelfWithFade(star_int_menu);
#ifdef ANDROID
      removeSelfWithFade(offerwall_menu);
      removeSelfWithFade(free_stars_label);
#endif

      locked_background = NULL;
      stars_locked_desc_label = NULL;
      stars_number_label = NULL;
      star_int_menu = NULL;
    } else {
      char stars_label_text[20];
      sprintf(
        stars_label_text,tr(StarsCountString),
        GameProgress::getTotalStarsCount(),
        stars_for_box+GameProgress::getTotalStarsCount());
      stars_number_label->setString(stars_label_text);

      CCSize star_size = star_int_menu->getContentSize();
      stars_number_label->setPosition(
        ccp(getContentSize().width/2
             - star_size.width*star_int_menu->getScale()/1.8,
            getContentSize().height/2
             + locked_background->getContentSize().height
               * locked_background->getScale()*0.04));
      star_int_menu->setPosition(ccp(
        getContentSize().width/2+stars_number_label->getContentSize().width/1.8,
        getContentSize().height/2+locked_background->getContentSize().height
                                  * locked_background->getScale()*0.04));
    }
  }
  
  
  for (int i=0; i<LEVELS_IN_GROUP; ++i) {
    if (stars_for_box <= 0 && level_items[i]->getOpacity() != 255) {
      level_items[i]->setOpacity(255);
    }
    LevelProgress progress = GameProgress::getLevelProgress(level_group,i);
    int star_count = progress.star_count;
    for (int j=0; j<star_count; ++j) {
      CCSize star_size = stars[i][j]->getContentSize();
      stars[i][j]->setPosition(
        ccp((level_items[i]->getContentSize().width
             +(-star_count+1+2*j)*star_size.width)/2,star_size.height/2));
      if (with_animation) {
        if (j<star_count && !stars[i][j]->isVisible()) {
          stars[i][j]->setVisible(true);
          stars[i][j]->setOpacity(0);
          stars[i][j]->runAction(CCSequence::createWithTwoActions(
            CCDelayTime::create(0.5f),
            CCSpawn::create(CCFadeIn::create(0.5f),
                            CCScaleTo::create(0.5f,stars[i][j]->getScale()),
                            NULL)));
          stars[i][j]->setScale(stars[i][j]->getScale()*10);
        }
      }
    }
    if (locks[i] && !(progress.blocked && !(i == 0 && stars_for_box <= 0))) {
      if (with_animation) {
        locks[i]->runAction(CCSequence::create(
          CCDelayTime::create(0.5f),
          CCSpawn::createWithTwoActions(
          CCFadeOut::create(0.5f),
          CCScaleTo::create(0.5f, locks[i]->getScale()*3)),
          CCRemoveSelf::create(),
          NULL));
      }
      locks[i] = NULL;
      level_items[i]->setTarget(
        levels_menu,menu_selector(LevelsMenu::startLevelCallback));
    }
  }
}

void LevelsSubmenu::onLanguageChanged() {
  update(false);
  if (stars_locked_desc_label) {
    stars_locked_desc_label->setString(tr(YouShouldEarnMoreStarsToUnlock));
#ifdef ANDROID
    free_stars_label->setString(tr(FreeStars));
#endif
  }
}

#ifdef ANDROID
void LevelsSubmenu::goToOfferwall( cocos2d::CCObject* sender ) {
  showOfferwall(levels_menu);
}
#endif
