#ifndef SOUNDS_MANAGER_H
#define SOUNDS_MANAGER_H

#include "cocos2d.h"

using namespace cocos2d;

enum SoundEffect {
  BubbleExploded,
  BubbleDisappeared,
  BubbleBecomeSmaller,
  AloneBubbleClicked,

  ButtonSound,

  BonusAppearedSound,
  BubbleRainSound,
  CreatorSound,
  EarthquakeSound,
  ShufflerSound,
  SplitterSound,
  
  BackgroundSound0,
  BackgroundSound1,
  BackgroundSound2,
  BackgroundSound3,

  Countdown1,
  Countdown2,
  Countdown3,
  Countdown4,
  Countdown5,
  Countdown6,
  Countdown7,
  Countdown8,
  Countdown9,
  Countdown10,

  FirstStarSound,
  SecondStarSound,
  ThirdStarSound,
  GameOverSound,
  GreatSound,
  GamePausedSound,
  VictorySound
};

class SoundsManager {
public:
  static SoundsManager &get();
  void setSoundsVolume(int value);
  void setMusicVolume(int value);
  // Set volume much lower then sounds volume. Doesn't store volume level.
  void lowMusicVolume();
  void restoreMusicVolume();
  int getSoundsVolume();
  int getMusicVolume();

  void playSound(SoundEffect sound_id);
  void collision(float power, bool rubber, bool with_ground);
  void playComicsMusic();
  void playMainMenuMusic();
  void playMusic(int group, bool insane);
  void pauseMusic();
  void restoreMenuOrComicsMusic();
private:
  SoundsManager();
  int current_group;
  bool current_insane;
  bool is_music_paused;
  enum NowPlaying {
    None,
    ComicsMusic,
    MainMenuMusic,
    LevelMusic
  } now_playing;
};

#endif // SOUNDS_MANAGER_H