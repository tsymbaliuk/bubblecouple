#include "view/main_menu.h"

#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/resource_loader.h"

MainMenu::MainMenu(float scale) : observer(NULL) {
  CCLayerColor::initWithColor( ccc4(255,255,255,0) );
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  char path[100], path_p[100];

  sprintf(path,"images/menu/spaceship_%i.png",getResourceWidth());
  CCSprite *spaceship = CCSprite::create(path);
  spaceship->setPosition(ccp(win_size.width/2,win_size.height*0.55));
  spaceship->setScale(0.8*win_size.height / spaceship->getContentSize().height);
  addChild(spaceship);

  sprintf(path,"images/menu/logo_%i.png",getResourceWidth());
  CCSprite *logo = CCSprite::create(path);
  logo->setPosition(ccp(win_size.width/2,win_size.height*0.65));
  logo->setScale(0.45 * win_size.height / logo->getContentSize().height);
  addChild(logo);

  sprintf(path,"images/menu/button_%i.png",getResourceWidth());
  sprintf(path_p,"images/menu/button_pressed_%i.png",getResourceWidth());
  CCMenuItemImage *start_campaign = CCMenuItemImage::create(
    path,path_p,this, menu_selector(MainMenu::startCampaignCallback) );
  scale = win_size.height * 0.117 / start_campaign->getContentSize().height;
  start_campaign->setScale(scale);
  start_campaign->setPosition(ccp(0,-win_size.height * 0.25));

  campaign_label = 
    CCLabelTTF::create(tr(Campaign), tr(FontName), win_size.height * 0.08);
  const ccColor3B fontColor={0,23,39};
  campaign_label->setColor(fontColor);
  campaign_label->setPosition(ccpAdd(
    start_campaign->getPosition(),ccp(win_size.width/2,win_size.height/2)));
  addChild(campaign_label,2);

  CCMenuItemImage *start_insane = CCMenuItemImage::create(
    path, path_p, this, menu_selector(MainMenu::startInsaneCallback) );
  scale = win_size.height * 0.117 / start_insane->getContentSize().height;
  start_insane->setScale(scale);
  start_insane->setPosition(ccp(
    start_insane->getContentSize().width*scale/1.85,-win_size.height * 0.4));

  insane_label = 
    CCLabelTTF::create(tr(Insane), tr(FontName), win_size.height * 0.08);
  insane_label->setColor(fontColor);
  insane_label->setPosition(ccpAdd(
    start_insane->getPosition(),ccp(win_size.width/2,win_size.height/2)));
  addChild(insane_label,2);

  CCMenuItemImage *start_classic = CCMenuItemImage::create(
    path, path_p, this, menu_selector(MainMenu::startClassicCallback) );
  scale = win_size.height * 0.117 / start_classic->getContentSize().height;
  start_classic->setScale(scale);
  start_classic->setPosition(ccp(
    -start_classic->getContentSize().width*scale/1.85,-win_size.height * 0.4));

  classic_label = 
    CCLabelTTF::create(tr(Classic), tr(FontName), win_size.height * 0.08);
  classic_label->setColor(fontColor);
  classic_label->setPosition(ccpAdd(
    start_classic->getPosition(),ccp(win_size.width/2,win_size.height/2)));
  addChild(classic_label,2);

  CCMenuItemImage *info = CCMenuItemImage::create(
    "images/menu/info_button.png","images/menu/info_button_pressed.png",
    this, menu_selector(MainMenu::infoCallback) );
  scale = win_size.height * 0.093 / info->getContentSize().height;
  if (win_size.height * 0.093 / CCDevice::getDPI() < 0.5) {
    scale *= 1.5f;
  }
  info->setScale(scale);
  info->setPosition(ccp(
    win_size.width*0.5-info->getContentSize().width*1.353*scale,
    win_size.height * 0.5 - info->getContentSize().height*scale/1.8));

  CCMenuItemImage *settings = CCMenuItemImage::create(
    "images/menu/settings_button.png","images/menu/settings_button_pressed.png",
    this, menu_selector(MainMenu::settingsCallback) );
  scale = win_size.height * 0.09 / settings->getContentSize().height;
  if (win_size.height * 0.093 / CCDevice::getDPI() < 0.5) {
    scale *= 1.5f;
  }
  settings->setScale(scale);
  settings->setPosition(ccp(
    win_size.width*0.5-settings->getContentSize().width*scale/1.8,
    win_size.height * 0.5 - settings->getContentSize().height*scale*1.353));

  CCMenu* menu = CCMenu::create(
    start_campaign, start_insane, start_classic, info, settings, NULL);

  addChild(menu);
}

MainMenu::~MainMenu() {
}

void MainMenu::onLanguageChanged() {
  campaign_label->setString(tr(Campaign));
  insane_label->setString(tr(Insane));
  classic_label->setString(tr(Classic));
}

void MainMenu::startCampaignCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->startCampaign();
  }
}

void MainMenu::startInsaneCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->startInsane();
  }
}

void MainMenu::startClassicCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->startClassic();
  }
}

void MainMenu::infoCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->showInfo();
  }
}

void MainMenu::settingsCallback(CCObject* sender) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->showSettings();
  }
}

void MainMenu::keyBackClicked() {
  if (observer) {
    observer->quit();
  }
}
