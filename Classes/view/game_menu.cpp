#include "view/game_menu.h"

#include <sstream>
#include "view/bubble.h"
#include "view/main_menu.h"
#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/resource_loader.h"
#include "model/game_progress.h"

using namespace cocos2d;

std::list<std::string> split(const std::string &s, char delim) {
  std::stringstream ss(s);
  std::list<std::string> elems;
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }
  return elems;
}

GameMenu * GameMenu::createPauseMenu( int stars_count, bool show_skip_button ) {
  SoundsManager::get().playSound(GamePausedSound);
  GameMenu *game_menu = new GameMenu(true, show_skip_button, true, true);
  game_menu->showPausedText();
  game_menu->showStars(stars_count,stars_count,false);
  game_menu->showBorder(PauseBorder);
  return game_menu;
}

GameMenu * GameMenu::createPauseMenu( int score ) {
  SoundsManager::get().playSound(GamePausedSound);
  GameMenu *game_menu = new GameMenu(true, false, true, true);
  game_menu->showPausedText();
  game_menu->showScore(score);
  game_menu->showBorder(PauseBorder);
  return game_menu;
}

GameMenu * GameMenu::createPauseMenu( int level, int score ) {
  SoundsManager::get().playSound(GamePausedSound);
  GameMenu *game_menu = new GameMenu(true, false, false, true);
  game_menu->showPausedText();
  game_menu->showLevelAndScore(level+1,score);
  game_menu->showBorder(PauseBorder);
  return game_menu;
}


GameMenu * GameMenu::createEndGameMenu( 
  int stars_count, int previous_stars_count, bool show_skip_button ) {
  GameMenu *game_menu = new GameMenu(false,show_skip_button,true,true);
  switch(stars_count) {
  case 3: game_menu->showText(tr(Excellent)); break;
  case 2: game_menu->showText(tr(Great)); break;
  case 1: game_menu->showText(tr(Good)); break;
  case 0: game_menu->showText(tr(Completed)); break;
  default: game_menu->showText(tr(GameOver)); break;
  }
  ccColor3B yellow_color = {255, 242, 133};
  game_menu->label->setColor(yellow_color);
  for (int i=-2; i<=1; i+=3) {
    CCSprite *shadow = CCSprite::createWithTexture(game_menu->label->getTexture());
    shadow->setPosition(game_menu->label->getPosition());
    shadow->setPositionX(shadow->getPositionX()+i*shadow->getContentSize().height/30);
    shadow->setPositionY(shadow->getPositionY()-i*shadow->getContentSize().height/30);
    shadow->setColor(yellow_color);
    shadow->setOpacity(30);
    game_menu->label->getParent()->addChild(shadow, 2);
  }
  if (stars_count > 0) {
    CCSprite *border = game_menu->showBorder(WinRaysBorder);
    switch(stars_count) {
    case 1: border->setOpacity(0x80); break;
    case 3: game_menu->showWinEffects(); break;
    }
    SoundsManager::get().playSound(VictorySound);
  } else if (stars_count == 0) {
    game_menu->showBorder(WinBorder);
    SoundsManager::get().playSound(VictorySound);
  } else {
    game_menu->showBorder(LooseBorder);
    SoundsManager::get().playSound(GameOverSound);
  }
  game_menu->showStars(stars_count,previous_stars_count,true);
  return game_menu;
}

GameMenu * GameMenu::createEndGameMenu( int score ) {
  GameMenu *game_menu = new GameMenu(false,false,true,true);
  game_menu->showText(tr(WellDone));
  game_menu->showScore(score);
  game_menu->showBorder(WinBorder);
  SoundsManager::get().playSound(GameOverSound);
  return game_menu;
}

GameMenu * GameMenu::createEndGameMenu( 
  int level, int score, int time_bonus_score, bool new_record ) {
  GameMenu *game_menu = 
    new GameMenu(false,time_bonus_score != -10,time_bonus_score == -10,true);
  if (time_bonus_score == -10) {
    // Game finished
    game_menu->showBorder(WinBorder);
    game_menu->showText(tr(GameOver));
    game_menu->showLevelAndBestLevel(level+1);
    SoundsManager::get().playSound(GameOverSound);
  } else {
    game_menu->showScoreAndBonus(score,time_bonus_score,new_record);
    game_menu->showBorder(WinRaysBorder);
    char level_string[100];
    if (new_record) {
      game_menu->showWinEffects();
    } 
    sprintf(level_string,tr(LevelCleared),level+1);
    game_menu->showText(level_string);
    SoundsManager::get().playSound(VictorySound);
  }
  return game_menu;
}

GameMenu * GameMenu::createNewInfoMenu( 
  ViewInterface::InfoMessage info, Location location ) {
  TrId header_id, desc_id;
  switch(info) {
  case ViewInterface::BlackBubbleAppear:
    header_id = BlackBubble;
    desc_id = BlackBubbleDescription;
    break;
  case ViewInterface::WhiteBubbleAppear:
    header_id = WhiteBubble;
    desc_id = WhiteBubbleDescription;
    break;
  case ViewInterface::RubberBubbleAppear:
    header_id = RubberBubble;
    desc_id = RubberBubbleDescription;
    break;
  case ViewInterface::ExplodingBubbleAppear:
    header_id = ExplodingBubble;
    desc_id = ExplodingBubbleDescription;
    break;
  case ViewInterface::GradientBubbleAppear:
    header_id = DoubleBubble;
    desc_id = DoubleBubbleDescription;
    break;
  case ViewInterface::RedEyedBubbleAppear: 
    header_id = RedEyeBubble;
    desc_id = RedEyeBubbleDescription;
    break;
  case ViewInterface::MagneticBubbleAppear: 
    header_id = MagneticBubble;
    desc_id = MagneticBubbleDescription;
    break;
  case ViewInterface::FirstInfo: 
    header_id = Introduction;
    desc_id = IntroductionDescription;
    break;
  case ViewInterface::BlackAndWhiteBubbles: 
    header_id = Annihilation;
    desc_id = AnnihilationDescription;
    break;
  case ViewInterface::ClassicGameMod: 
    header_id = ClassicGame;
    desc_id = ClassicGameDescription;
    break;
  case ViewInterface::InsaneGameMod: 
    header_id = InsaneGame;
    desc_id = InsaneGameDescription;
    break;
  default: assert(!"Never rich here"); break;
  }
  std::list<std::string> splited = split(tr(desc_id),'|');
  GameMenu *game_menu = 
    new GameMenu(splited.size()==1,splited.size()!=1,false,false);
  if (info < ViewInterface::BubbleAppearFinish) {
    game_menu->showBubble(info, location);
  }
  game_menu->next_subinfo = splited;
  game_menu->showNewInfoText(
    tr(header_id), game_menu->next_subinfo.front().c_str());
  game_menu->next_subinfo.pop_front();
  game_menu->showBorder(PauseBorder);
  return game_menu;
}


GameMenu::GameMenu( bool show_back_button, bool show_skip_button,
                    bool show_restart_button, bool show_menu_button ) : 
  observer(NULL), skip_button(NULL) {
  init();
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char path[100],path_p[100];
  sprintf(path,"images/menu/pause_background_%i.png",getResourceWidth());
  CCSprite *background = CCSprite::create(path);
  bg_size = background->getContentSize();

  // (1096/2048)% percent of screen width
  scale_factor = 1096.0f / 2048 * win_size.width / bg_size.width;
  
  background->setScale(scale_factor);

  CCArray buttons;
  if (show_back_button) {
    sprintf(path,"images/menu/ok_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/ok_pressed_%i.png",getResourceWidth());
    CCMenuItemImage *resume_item = CCMenuItemImage::create(
      path,
      path_p,
      this,
      menu_selector(GameMenu::menuResumeCallback));
    buttons.addObject(resume_item);
  }

  if (show_restart_button) {
    sprintf(path,"images/menu/restart_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/restart_pressed_%i.png",getResourceWidth());
    CCMenuItemImage *restart_item = CCMenuItemImage::create(
      path,
      path_p,
      this,
      menu_selector(GameMenu::menuRestartCallback));
    buttons.addObject(restart_item);
  }

  if (show_menu_button) {
    sprintf(path,"images/menu/menu_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/menu_pressed_%i.png",getResourceWidth());
    CCMenuItemImage *menu_item = CCMenuItemImage::create(
      path,
      path_p,
      this,
      menu_selector(GameMenu::menuMenuCallback));
    buttons.addObject(menu_item);
  }

  if (show_skip_button) {
    sprintf(path,"images/menu/skip_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/skip_pressed_%i.png",getResourceWidth());
    skip_button = CCMenuItemImage::create(
      path,
      path_p,
      this,
      menu_selector(GameMenu::menuNextLevelCallback));
    buttons.addObject(skip_button);
  }
  
  for (unsigned i=0; i<buttons.count(); ++i) {
    CCMenuItemImage *item = 
      static_cast<CCMenuItemImage *>(buttons.objectAtIndex(i));
    item->setScale(scale_factor);
    item->setPosition(
      bg_size.width*scale_factor*(-0.09*(buttons.count()-1)+0.18*i),
                                  -bg_size.height*scale_factor*0.272);
  }

  background->setPosition(ccp(0, bg_size.height*scale_factor*0.04));
  addChild(background, 2);

  CCMenu* menu = CCMenu::createWithArray(&buttons);
  menu->setPosition(CCPointZero);

  addChild(menu, 2);

  // 13% of height
  label = CCLabelTTF::create(tr(GamePaused), tr(FontName), 
                             bg_size.height * scale_factor * 0.13);
  ccColor3B gray = {0.83f*255, 0.83f*255, 0.83f*255};
  label->setColor(gray);
  label->setPosition(ccp(0, bg_size.height*scale_factor*0.36));
  addChild(label, 2);

  // 10% of height
  sub_label = CCLabelTTF::create("", tr(FontName), 
                                 bg_size.height * scale_factor * 0.10, 
                                 CCSize(bg_size.width*scale_factor*0.9,0), 
                                 kCCTextAlignmentCenter);
  sub_label->setColor(gray);
  sub_label->setPosition(ccp(0, bg_size.height*scale_factor*0.07));
  addChild(sub_label, 2);

  SoundsManager::get().lowMusicVolume(); // hack
}

class PlaySoundAction : public CCFiniteTimeAction {
public:
  PlaySoundAction(SoundEffect sound) : sound(sound) {autorelease();}
  virtual void startWithTarget(CCNode *) {
    SoundsManager::get().playSound(sound);
  }
private:
  SoundEffect sound;
};

void GameMenu::showStars( int count, int previous_count, bool animate ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  char star_path[100],shadow_path[100];
  sprintf(star_path,"images/menu/star_medium_%i.png",getResourceWidth());
  sprintf(shadow_path,
          "images/menu/star_medium_shadow_%i.png",getResourceWidth());
  CCTargetedAction* actions[3];
  for (int i=0; i<3; ++i) {
    CCSprite *shadow = CCSprite::create(shadow_path);

    shadow->setScale(scale_factor);
    shadow->setPosition(ccp((-0.11+i*0.11)*scale_factor*getResourceWidth(),
                             0.025*scale_factor*getResourceWidth()));
    addChild(shadow, 2);

    actions[i] = NULL;
    if (i<count) {
      CCSprite *star = CCSprite::create(star_path);
      if (animate) {
        star->setOpacity(0);
        CCFiniteTimeAction* action = NULL;
        if (i<count) {
          action = CCSpawn::createWithTwoActions(
            CCFadeIn::create(0.3f),
            new PlaySoundAction(static_cast<SoundEffect>(FirstStarSound+i)));
          actions[i] = CCTargetedAction::create(star,action);
        }
      } else {
        // If we don't animate, we show stars with fade
        star->setOpacity(100);
      }
      star->setScale(scale_factor);
      star->setPosition(shadow->getPosition());
      addChild(star, 2);
    }
  }
  if (animate && actions[0]) {
    runAction(CCSequence::create(
      CCDelayTime::create(0.3f),actions[0],actions[1],actions[2],NULL));
  }
}

void GameMenu::showScore( int score ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char score_string[100];
  sprintf(score_string,tr(ScoreString),score);
  sub_label->setString(score_string);
}

void GameMenu::showLevelAndScore( int level, int score ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char score_string[100];
  sprintf(score_string,tr(LevelAndScoreString),level,score);
  sub_label->setString(score_string);
}

void GameMenu::showLevelAndBestLevel( int level ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char score_string[100], format_string[100];
  sprintf(format_string,"%s\n%s",tr(LevelNumber),tr(BestLevelNumber));
  sprintf(score_string,format_string,
          level,GameProgress::getInsaneLevelRecord()+1);
  sub_label->setString(score_string);
}

void GameMenu::showScoreAndBonus( 
  int score, int bonus_score, bool new_record ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  char score_string[100];
  sprintf(score_string,tr(ScoreAndTimeBonusString),score,bonus_score);
  if (new_record) {
    strcat(score_string,"\n");
    strcat(score_string,tr(NewRecord));
  }
  sub_label->setString(score_string);
}

CCSprite *GameMenu::showBorder( BorderType border_type ) {
  char path[100];
  switch(border_type) {
  case PauseBorder: 
    sprintf(path,
      "images/menu/pause_background_shadow_%i.png",getResourceWidth());
    break;
  case WinBorder: 
    sprintf(path,
      "images/menu/pause_background_win_shadow_%i.png",getResourceWidth());
    break;
  case WinRaysBorder: 
    sprintf(path,
      "images/menu/pause_background_win_rays_%i.png",getResourceWidth());
    break;
  case LooseBorder: 
    sprintf(path,
      "images/menu/pause_background_loose_shadow_%i.png",getResourceWidth());
    break;
  }

  CCSprite *border = CCSprite::create(path);
  if (border_type == WinRaysBorder || border_type == LooseBorder) {
    CCSize win_size = CCDirector::sharedDirector()->getWinSize();
    float scale = 1;
    float bg_ratio = border->getContentSize().width
                     / border->getContentSize().height;
    float win_ratio = win_size.width / win_size.height;

    if (bg_ratio > win_ratio) {
      // scale to height
      scale = win_size.height / border->getContentSize().height;
    } else {
      // scale to width
      scale = win_size.width / border->getContentSize().width;
    }
    border->setScale(scale);
  } else {
    border->setScale(scale_factor);
  }
  addChild(border, 1);

  if (border_type == LooseBorder) {
    showBorder(PauseBorder); // Double border for loose
  }
  return border;
}

void GameMenu::showWinEffects() {
  for (int i=0; i<5; ++i) {
    CCParticleSystemQuad *emitter = new CCParticleSystemQuad();
    emitter->initWithTotalParticles(30);
    emitter->autorelease();

    addChild(emitter, 1);

    char path[100];
    sprintf(path,"images/menu/win_effect%i_%i.png",i,getResourceWidth());

    emitter->setTexture( CCTextureCache::sharedTextureCache()->addImage(path) );

    // duration
    emitter->setDuration(-1);

    // gravity
    emitter->setGravity(CCPointZero);

    // angle
    emitter->setAngle(90);
    emitter->setAngleVar(360);

    // speed of particles
    emitter->setSpeed(getResourceWidth()/3);
    emitter->setSpeedVar(getResourceWidth()/5);

    // radial
    emitter->setRadialAccel(-120);
    emitter->setRadialAccelVar(0);

    // emitter position
    emitter->setPosition( ccp(160,240) );
    emitter->setPosVar(CCPointZero);

    // life of particles
    emitter->setLife(3);
    emitter->setLifeVar(1);

    // spin of particles
    emitter->setStartSpin(0);
    emitter->setStartSpinVar(500);
    emitter->setEndSpin(0);
    emitter->setEndSpinVar(1000);

    // color of particles
    ccColor4F startColor = {1, 1, 1, 1.0f};
    emitter->setStartColor(startColor);

    ccColor4F startColorVar = {0, 0, 0, 0};
    emitter->setStartColorVar(startColorVar);

    ccColor4F endColor = {1, 1, 1, 0};
    emitter->setEndColor(endColor);

    ccColor4F endColorVar = {0, 0, 0, 0};
    emitter->setEndColorVar(endColorVar);

    // size, in pixels
    emitter->setEndSize(120*getResourceWidth()/2048*scale_factor);
    emitter->setStartSize(emitter->getStartSize()/2);
    emitter->setStartSizeVar(emitter->getStartSize()/5);

    // emits per second
    emitter->setEmissionRate(
      emitter->getTotalParticles()/emitter->getLife());

    // additive
    emitter->setBlendAdditive(false);

    emitter->setPosition(0,0);
  }
}

void GameMenu::showPausedText() {
  label->setString(tr(GamePaused));
}

void GameMenu::showBubble(ViewInterface::InfoMessage info, Location location) {
  char path[100];
  sprintf(path,"images/menu/new_bubble_shadow_%i.png",getResourceWidth());
  CCSprite *shadow = CCSprite::create(path);
  shadow->setScale(scale_factor);
  addChild(shadow,2);

  Bubble *bubble = 
    new Bubble(info,1.35*shadow->getContentSize().width*scale_factor,location);
  bubble->setPosition(ccp(bg_size.width*scale_factor*0.27,
                          bg_size.height*scale_factor*0.27));
  addChild(bubble,2);
  shadow->setPosition(ccpAdd(bubble->getPosition(),
                             ccp(0,-bubble->getContentSize().height
                                   *bubble->getScale()/2)));

  label->setFontSize(bg_size.height * scale_factor * 0.10);
  label->setPosition(ccp(-bg_size.width*scale_factor*0.15,
                         bubble->getPosition().y));

  sub_label->setFontSize(bg_size.height * scale_factor * 0.08);
  sub_label->setPosition(ccpSub(sub_label->getPosition(),
                                ccp(0,bg_size.height*scale_factor*0.1)));
}

void GameMenu::showNewInfoText( const char *header, const char *description ) {
  label->setString(header);
  sub_label->setString(description);
}

void GameMenu::showText(const char *text) {
  label->setString(text);
}

void GameMenu::replaceSkipToBackButton() {
  if (skip_button) {
    char path[100], path_p[100];
    sprintf(path,"images/menu/ok_%i.png",getResourceWidth());
    sprintf(path_p,"images/menu/ok_pressed_%i.png",getResourceWidth());
    skip_button->setNormalImage(CCSprite::create(path));
    skip_button->setSelectedImage(CCSprite::create(path_p));
    skip_button->setTarget(this,menu_selector(GameMenu::menuResumeCallback));
    skip_button = NULL; // possible only once
  }
}

void GameMenu::menuResumeCallback( CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->resumeButtonPressed();
  }
}

void GameMenu::menuRestartCallback( CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->restartButtonPressed();
  }
}

void GameMenu::menuNextLevelCallback( CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  if (next_subinfo.empty()) {
    if (observer) {
      observer->nextLevelButtonPressed();
    }
  } else {
    sub_label->setString(next_subinfo.front().c_str());
    next_subinfo.pop_front();
    if (next_subinfo.empty()) {
      replaceSkipToBackButton();
    }
  }
}

void GameMenu::menuMenuCallback( CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  if (observer) {
    observer->menuButtonPressed();
  }
}