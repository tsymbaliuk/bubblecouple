#ifndef COLORS_H
#define COLORS_H

#include "cocos2d.h"
#include "model/bubble_parameters.h"

enum Location {
  DesertLocation,
  TownLocation,
  MountainsLocation,
  TunguskaLocation,

  TotalLocations
};

cocos2d::ccColor4F getColor4F(
  Location location, BubbleParameters::Color number );
cocos2d::ccColor3B getColor3B( 
  Location location, BubbleParameters::Color number );
cocos2d::ccColor3B getComplement3B( 
  Location location, BubbleParameters::Color number );

#endif // COLORS_H
