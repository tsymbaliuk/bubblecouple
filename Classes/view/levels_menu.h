#ifndef LEVELS_MENU_H
#define LEVELS_MENU_H

#include "view/menu_interface.h"
#include "model/level_manager.h"
#ifdef ANDROID
#include "Offerwall.h"
#endif

using namespace cocos2d;

class LevelsSubmenu;
class LevelsMenu : public MenuInterface, public MenuInterface::InternalObserver
#ifdef ANDROID
  , public OfferwallInterface
#endif
{
public:
  LevelsMenu(CCNode *parent);
  class Observer {
  public:
    virtual void startLevel(int group, int level) = 0;
    virtual void quitLevelSelection() = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
  void updateStars();
  void onLanguageChanged();
private:
  void updateStarsNumberLabel();
  void startLevelCallback(cocos2d::CCObject* pSender);
  virtual void quitMenu();

  void provideSwitch();
  void switchToBox(cocos2d::CCObject* sender);
  void switchToNextBox(cocos2d::CCObject*);
  void switchToPrevBox(cocos2d::CCObject*);

  virtual bool ccTouchBegan(CCTouch *touch, CCEvent *event);
  virtual void ccTouchMoved(CCTouch *touch, CCEvent *event);
  virtual void ccTouchEnded(CCTouch *touch, CCEvent *event);
  virtual void ccTouchCancelled(CCTouch *touch, CCEvent *event);
  CCTouch *current_touch;

#ifdef ANDROID
  // OfferwallInterface implementation
  virtual void starsNumberUpdateStarted();
  virtual void starsNumberUpdated(int new_stars_count);
  virtual void starsNumberUpdateFailed();
#endif

  Observer *observer;

  int current_level_group;
  CCMenuItemImage *level_icons[LEVEL_GROUPS_COUNT];
  LevelsSubmenu *levels_submenus[LEVEL_GROUPS_COUNT];

  CCSprite *star;
  CCLabelTTF* stars_number_label;

  // Necessary to connect to startLevelCallback();
  friend class LevelsSubmenu;
  // Necessary to proceed menu touches
  friend class LevelsSubmenuMenu;
};

class LevelsSubmenuMenu : public CCMenu {
public:
  LevelsSubmenuMenu(LevelsMenu *levels_menu);
  virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
  virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
  virtual void ccTouchCancelled(CCTouch *touch, CCEvent* event);
  virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
private:
  LevelsMenu *levels_menu;
  CCTouch *tracked_touch;
  bool translate_to_ccmenu;
};

class LevelsSubmenu : public CCLayer
{
public:
  LevelsSubmenu(
    LevelsMenu *levels_menu, int level_group, float width, float height);
  void update(bool with_animation);
  void onLanguageChanged();
private:
  LevelsMenu *levels_menu;
  int level_group;
  CCSprite *stars[LEVELS_IN_GROUP][3];
  CCMenuItemImage *level_items[LEVELS_IN_GROUP];
  CCSprite *locks[LEVELS_IN_GROUP];

  CCSprite *locked_background, *star_int_menu;
  CCLabelTTF *stars_locked_desc_label, *stars_number_label;
#ifdef ANDROID
  CCMenu* offerwall_menu;
  CCLabelTTF *free_stars_label;
  void goToOfferwall(cocos2d::CCObject* sender);
#endif
};

#endif // LEVELS_MENU_H