#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <map>

enum TrId {
  GamePaused,
  ScoreString,
  LevelAndScoreString,
  ScoreAndTimeBonusString,
  NewRecord,
  BubblesLeft,
  IncreaseScoreString,
  StarsCountString,
  FreeStars,

  Authors,
  Developer,
  DesignAndGraphics,
  SoundDesigner,
  CreatedBy,
  Settings,
  Sounds,
  Music,
  GraphicsLow,
  GraphicsMid,
  GraphicsHigh,
  RecommendedGraphics,

  BlackBubble,
  BlackBubbleDescription,
  WhiteBubble,
  WhiteBubbleDescription,
  ExplodingBubble,
  ExplodingBubbleDescription,
  DoubleBubble,
  DoubleBubbleDescription,
  RedEyeBubble,
  RedEyeBubbleDescription,
  RubberBubble,
  RubberBubbleDescription,
  MagneticBubble,
  MagneticBubbleDescription,
  BubbleInInsaneModUnlocked,

  Introduction,
  IntroductionDescription,
  Annihilation,
  AnnihilationDescription,

  LevelCleared,
  WellDone,
  Excellent,
  Great,
  Good,
  Completed,
  GameOver,

  LevelNumber,
  BestLevelNumber,

  Desert,
  Town,
  Mountains,
  Tunguska,
  LevelsUnlocked,

  Total,
  YouShouldEarnMoreStarsToUnlock,

  Campaign,
  Insane,
  Classic,

  Karr,

  ClassicGame,
  ClassicGameDescription,
  InsaneGame,
  InsaneGameDescription,

  LanguageName,
  FontName,

  TotalVariables
};

enum Language {
  English,
  Ukrainian,
  Russian,
  German,
  Lithuanian,
  Spanish,

  TotalLanguages
};

const char *tr(TrId input);

const char *languageString(int id);

#endif // TRANSLATOR_H
