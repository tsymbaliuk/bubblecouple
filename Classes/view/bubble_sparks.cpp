#include "view/bubble_sparks.h"

BubbleSparks::BubbleSparks(ccColor4F color, float start_size, bool exploding) {
  CCParticleSystemQuad::initWithTotalParticles(250);

  // duration
  m_fDuration = 0.1f;

  // Gravity Mode
  m_nEmitterMode = kCCParticleModeGravity;

  m_ePositionType = kCCPositionTypeGrouped;

  int height = CCDirector::sharedDirector()->getWinSize().height;
  // Gravity Mode: speed of particles
  modeA.speed = height / 3.0f;
  modeA.speedVar = height / 4.0f;    

  // starting angle
  m_fAngle = 90;
  m_fAngleVar = exploding ? 360 : 20;

  // life of particles
  m_fLife = 1.3f;
  m_fLifeVar = 1;

  // size, in pixels
  m_fStartSize = start_size;
  m_fStartSizeVar = start_size/4;
  m_fEndSize = 0.0f;

  // emits per frame
  m_fEmissionRate = m_uTotalParticles/m_fLife;

  // color of particles
  m_tStartColor = color;
  m_tStartColorVar.r = 0.0f;
  m_tStartColorVar.g = 0.0f;
  m_tStartColorVar.b = 0.0f;
  m_tStartColorVar.a = 0.0f;
  m_tEndColor = color;
  m_tEndColor.a = 0.5f;
  m_tEndColorVar.r = 0.0f;
  m_tEndColorVar.g = 0.0f;
  m_tEndColorVar.b = 0.0f;
  m_tEndColorVar.a = 0.5f;

  CCTexture2D* pTexture;
  {
    pTexture = NULL;
    CCImage* pImage = NULL;
    do 
    {
      bool bRet = false;
      const char* key = "__firePngData";
      pTexture = CCTextureCache::sharedTextureCache()->textureForKey(key);
      CC_BREAK_IF(pTexture != NULL);

      pImage = new CCImage();
      CC_BREAK_IF(NULL == pImage);
      bRet = pImage->initWithImageFile("images/circles/spark_88.png");
      CC_BREAK_IF(!bRet);

      pTexture = CCTextureCache::sharedTextureCache()->addUIImage(pImage, key);
    } while (0);

    CC_SAFE_RELEASE(pImage);
  }
  setTexture(pTexture);

  setStartSpin(0);
  setStartSpinVar(500);
  setEndSpin(0);
  setEndSpinVar(1000);

  autorelease();
  setAutoRemoveOnFinish(true);
}
