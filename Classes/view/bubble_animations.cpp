#include "view/bubble_animations.h"

#include "view/bubble.h"

const float mult = 1.0f; // For debug purposes

AnimationTarget::AnimationTarget() : 
  eye(NULL), left_pupil(NULL), right_pupil(NULL), glasses(NULL) {
}

void BubbleAnimations::startRandomAnimation( AnimationTarget target ) {
  switch(rand()%1000) {
  case 1: if (target.glasses) {
      target.glasses->stopActionByTag(GlassAnimation);
      target.glasses->runAction(createGlassesAnimation(target.glasses));
    } break;
  case 2: {
    if (!target.eye->numberOfRunningActions()) {
      target.left_pupil->runAction(createPupilAnimation1(target.left_pupil));
      target.right_pupil->runAction(createPupilAnimation1(target.right_pupil));
      target.eye->runAction(createEyeAnimation(target.eye));
    }
    } break;
  }
}

void BubbleAnimations::startAngryAnimation( AnimationTarget target ) {
  int running_actions = target.mask->numberOfRunningActions();
  if (running_actions < 3) {
    // First 3 clicks aren't valid
    target.mask->runAction(CCDelayTime::create(1.0*mult));
  } else {
    if (running_actions < 8) {
      CCActionInterval *rotate_mask = CCEaseSineOut::create(
        CCRotateBy::create(2.5f*mult,360*(running_actions-3)));
      rotate_mask->setTag(AngryMaskAction);
      target.mask->runAction(rotate_mask);
    } else {
      target.bubble->setPukingMouth();
    }
  }
}

CCActionInterval *BubbleAnimations::createGlassesAnimation(CCSprite *glasses) {
  CCActionInterval *action = CCSequence::create(
    CCRotateTo::create(0.15*mult,10,10),
    CCRotateTo::create(0.15*mult,-10,-10),
    CCRotateTo::create(0.15*mult,0,0),NULL);
  action->setTag(GlassAnimation);
  return action;
}

CCActionInterval *BubbleAnimations::createPupilAnimation1(CCSprite *pupil) {
  CCSize p_s = pupil->getContentSize();
  CCSize e_s(p_s.width*2,p_s.height*2); 
  CCPoint e_p = pupil->getPosition();
  CCActionInterval *action = CCSequence::create(
    CCMoveTo::create(0.2*mult,
    ccp(e_p.x-e_s.width/1.4+p_s.width*0.65,e_p.y)),
    CCDelayTime::create(0.2*mult),
    CCMoveTo::create(0.2*mult,
    ccp(e_p.x+e_s.width/1.8-p_s.width*0.65,e_p.y)),
    CCDelayTime::create(0.4*mult),
    CCMoveTo::create(0.15*mult,e_p),
    NULL);
  action->setTag(EyeAnimation1);
  return action;
}

CCActionInterval *BubbleAnimations::createEyeAnimation( CCSprite *eye ) {
  CCActionInterval *action = CCSequence::create(
    CCScaleTo::create(0.2*mult,eye->getScaleX(),eye->getScaleY()*0.7),
    CCDelayTime::create(0.7*mult),
    CCScaleTo::create(0.2*mult,eye->getScaleX(),eye->getScaleY()),
    CCDelayTime::create(1.2*mult), // Time that no other action will start
    NULL);
  action->setTag(EyeAnimation1);
  return action;
}