#include "view/bubble.h"

#include "view/resource_loader.h"

using namespace cocos2d;

Bubble::Bubble( BubbleParameters *ball, float scale, Location location ) {
  init(ball,ball->radius*2*scale,location);
}

Bubble::Bubble( 
  ViewInterface::InfoMessage info, float real_diameter, Location location ) {
  char path[100];
  sprintf(path,"images/menu/new_bubble_shining_%i.png",getResourceWidth());

  BubbleParameters bp;
  bp.is_availible = true;
  switch(info) {
  case ViewInterface::BlackBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::BlackColor; break;
  case ViewInterface::WhiteBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::WhiteColor; break;
  case ViewInterface::RubberBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::BColor; 
    bp.is_rubber = true; break;
  case ViewInterface::ExplodingBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::RColor; 
    bp.is_exploding = true; break;
  case ViewInterface::GradientBubbleAppear: 
    bp.color = BubbleParameters::RColor; 
    bp.double_color = BubbleParameters::BColor; break;
  case ViewInterface::RedEyedBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::GColor; bp.order = 2; 
    break;
  case ViewInterface::MagneticBubbleAppear: 
    bp.color = bp.double_color = BubbleParameters::GColor; 
    bp.charge_density = 1; 
    break;
  default: assert(!"Never rich here"); break;
  }
  init(&bp,real_diameter,location);
}

void Bubble::setOpacity( GLubyte value, bool immediately ) {
  setOpacity(this,value,immediately);
}

void Bubble::updateColor(Location location, BubbleParameters::Color color, 
                         BubbleParameters::Color double_color) {
  ccColor3B color3b = getColor3B(location,color);
  ccColor3B comp3b = getComplement3B(location,color);
  runAction(CCTintTo::create(0.2f,color3b.r,color3b.g,color3b.b));
  mask_sprite->runAction(CCTintTo::create(0.2f,comp3b.r,comp3b.g,comp3b.b));
  if (mask_second_sprite) {
    ccColor3B double_color3b = getColor3B(location,double_color);
    mask_second_sprite->runAction(CCTintTo::create(
      0.2f,double_color3b.r,double_color3b.g,double_color3b.b));
  }
}

void Bubble::setOpacity(
  CCSprite* sprite_object, GLubyte value, bool immediately) {
  if (immediately) {
    sprite_object->setOpacity(value);
  } else {
    sprite_object->runAction(CCFadeTo::create(0.2f,value));
  }
  CCArray *children = sprite_object->getChildren();
  if (children) {
    CCObject* child = NULL;
    CCARRAY_FOREACH(children, child) {
      CCSprite *sprite = static_cast<CCSprite *>(child);
      sprite->runAction(CCFadeTo::create(0.2f,value));
      if (sprite->getChildrenCount()>0) {
        setOpacity(sprite,value,immediately);
      } else {
        if (immediately) {
          sprite->setOpacity(value);
        } else {
          sprite->runAction(CCFadeTo::create(0.2f,value));
        }
      }
    }
  }
}

void Bubble::init( 
  BubbleParameters *ball, float real_diameter, Location location ) {
  int sizes[] = {40,60,88,125,177,250,350,500};
  int i;
  for (i=0;i<sizeof(sizes)/sizeof(int)-1; ++i) {
    if (real_diameter <= sizes[i]) {
      break;
    }
  }
  char filename[100];
  sprintf(filename,"images/circles/circle%s%i.png",
          ball->isDouble()?"_grad":"",sizes[i]);
  initWithFile(filename);
  if (ball->isDouble()) {
    setColor(getColor3B(location,ball->color));
    mask_second_sprite = CCSprite::create(filename);
    mask_second_sprite->setColor(getColor3B(location,ball->double_color));
    mask_second_sprite->setRotation(180);
    addChild(mask_second_sprite);
    mask_second_sprite->setPosition(ccp(getContentSize().width/2,
                                        getContentSize().height/2));
  } else {
    mask_second_sprite = NULL;
    setColor(getColor3B(location,ball->color));
  }
  autorelease();

  getTexture()->setAntiAliasTexParameters();

  sprintf(filename,"images/circles/masks/%i_%i.png",rand()%5,sizes[i]);
  mask_sprite = CCSprite::create(filename);
  mask_sprite->setColor(getComplement3B(location,ball->color));
  mask_sprite->setPosition(ccp(getContentSize().width/2,
                               getContentSize().height/2));
  ball->animation_target.mask = mask_sprite;
  addChild(mask_sprite);

  sprintf(filename,"images/circles/shining%i.png",sizes[i]);
  CCSprite *shining_sprite = CCSprite::create(filename);
  shining_sprite->setPosition(ccp(getContentSize().width/2,
                                  getContentSize().height/2));
  addChild(shining_sprite);

  static const CCPoint eyebrows_offsets[5] = {
    ccp(0,130.5f),ccp(0,153),ccp(-0.5f,137),ccp(0,135),ccp(-7,142)
  };
  int eyebrows_type = rand()%5;
  sprintf(filename,
    "images/circles/eyebrows/eyebrows%i_%i.png",eyebrows_type,sizes[i]);
  CCSprite *eyebrows_sprite = CCSprite::create(filename);
  eyebrows_sprite->setPosition(ccp(
    getContentSize().width/2 + eyebrows_offsets[eyebrows_type].x*sizes[i]/490,
    getContentSize().height/2+ eyebrows_offsets[eyebrows_type].y*sizes[i]/490));
  addChild(eyebrows_sprite);

  sprintf(filename,"images/circles/eyes_%i.png",sizes[i]);
  CCSprite *eyes_sprite = CCSprite::create(filename);
  if (ball->order > 1) {
    eyes_sprite->setColor(ccRED);
  }
  eyes_sprite->setPosition(ccp(getContentSize().width/2,
    getContentSize().height/2 + 54.5*sizes[i]/490));
  addChild(eyes_sprite);
  ball->animation_target.eye = eyes_sprite;

  sprintf(filename,"images/circles/pupils/black_l_%i.png",sizes[i]);
  CCSprite *left_pupil_sprite = CCSprite::create(filename);
  left_pupil_sprite->setPosition(ccp(
    getContentSize().width/2-44.5*sizes[i]/500,
    getContentSize().height/2+55*sizes[i]/490));
  addChild(left_pupil_sprite);
  ball->animation_target.left_pupil = left_pupil_sprite;

  sprintf(filename,"images/circles/pupils/black_r_%i.png",sizes[i]);
  CCSprite *right_pupil_sprite = CCSprite::create(filename);
  right_pupil_sprite->setPosition(ccp(
    getContentSize().width/2+59.5*sizes[i]/500,
    getContentSize().height/2+55*sizes[i]/490));
  addChild(right_pupil_sprite);
  ball->animation_target.right_pupil = right_pupil_sprite;

  if (ball->is_rubber) {
    sprintf(filename,"images/circles/accessories/moustache_%i.png",sizes[i]);
    CCSprite *moustache_sprite = CCSprite::create(filename);
    moustache_sprite->setPosition(ccp(getContentSize().width/2,
      getContentSize().height/2-19.5*sizes[i]/490));
    addChild(moustache_sprite);
    mouth_sprite = NULL;
  } else {
    static const CCPoint mouths_offsets[9] = {
      ccp(6,56.5f),ccp(0.5f,54),ccp(6.5f,44.5f),ccp(6,98),ccp(8,54.5f),
      ccp(4.5f,46),ccp(4,65),ccp(7.5f,52),ccp(10,49)
    };
    int mouse_type = rand()%9;
    sprintf(filename,"images/circles/mouths/%i_%i.png",mouse_type,sizes[i]);
    mouth_sprite = CCSprite::create(filename);
    mouth_sprite->setPosition(ccp(
      getContentSize().width/2 - mouths_offsets[mouse_type].x*sizes[i]/490,
      getContentSize().height/2 - mouths_offsets[mouse_type].y*sizes[i]/490));
    addChild(mouth_sprite);
    is_mouse_puking = false;
  }

  if (ball->is_exploding) {
    sprintf(filename,"images/circles/accessories/glasses_dark_%i.png",sizes[i]);
    CCSprite *glasses_sprite = CCSprite::create(filename);
    glasses_sprite->setPosition(ccp(
      getContentSize().width/2,
      getContentSize().height/2+50.5*sizes[i]/490));
    addChild(glasses_sprite);
    ball->animation_target.glasses = glasses_sprite;
  }

  if (ball->charge_density != 0) {
    if (ball->charge_density > 0) {
      sprintf(filename,"images/circles/accessories/magnetic_p_%i.png",sizes[i]);
    } else {
      sprintf(filename,"images/circles/accessories/magnetic_n_%i.png",sizes[i]);
    }
      CCSprite *armor_sprite = CCSprite::create(filename);
      armor_sprite->setPosition(ccp(getContentSize().width/2,
                                    getContentSize().height/2));
      addChild(armor_sprite);
  }

  float scale = real_diameter/getContentSize().width;
  if (ball->is_availible) {
    setScale(scale);
    setOpacity(0xFF,false);
  } else {
    setScale(0);
    runAction(CCScaleTo::create(0.2f,scale));
    setOpacity(0x33,false);
  }
  size = sizes[i];
  ball->animation_target.bubble = this;
}

void Bubble::setPukingMouth() {
  if (mouth_sprite && !is_mouse_puking) {
    CCPoint offset(7,43.5f);
    char filename[100];
    sprintf(filename,"images/circles/mouths/puking_%i.png",size);
    removeChild(mouth_sprite,true);
    mouth_sprite = CCSprite::create(filename);
    mouth_sprite->setPosition(ccp(
      getContentSize().width/2 - offset.x*size/490,
      getContentSize().height/2 - offset.y*size/490));
    addChild(mouth_sprite);
    is_mouse_puking = true;
  }
}
