#include "view/info_menu.h"

#include "view/resource_loader.h"
#include "view/translator.h"
#include "model/game_progress.h"
#include "model/level_manager.h"

InfoMenu::InfoMenu() : observer(NULL) {
  setHeaderText(tr(Authors));
  setInternalObserver(this);

  CCMenuItemFont::setFontName(tr(FontName));
  CCMenuItemFont::setFontSize(foreground_height / 16);

  titles[0] = CCMenuItemFont::create(
    tr(Developer), this, menu_selector(InfoMenu::callback) );
  titles[1] = CCMenuItemFont::create( 
    tr(DesignAndGraphics), this, menu_selector(InfoMenu::callback) );
  titles[2] = CCMenuItemFont::create( 
    tr(SoundDesigner), this, menu_selector(InfoMenu::callback) );
  titles[3] = CCMenuItemFont::create( 
    tr(CreatedBy), this, menu_selector(InfoMenu::callback) );

  CCMenu *menu = CCMenu::create( titles[0],titles[1],titles[2],titles[3],NULL );

  menu->alignItemsVerticallyWithPadding(foreground_height / 30);

  content->addChild( menu );
}

void InfoMenu::onLanguageChanged() {
  setHeaderText(tr(Authors));

  titles[0]->setString(tr(Developer));
  titles[1]->setString(tr(DesignAndGraphics));
  titles[2]->setString(tr(SoundDesigner));
  titles[3]->setString(tr(CreatedBy));
}

void InfoMenu::callback( CCObject * sender ) {
  // For openURL function see http://cocos2d-x.org/forums/6/topics/11290
  if (sender == titles[0]) {
    // easter egg
    static int count = 0;
    count++;
    if (count >= 5 && count-5 < LEVEL_GROUPS_COUNT) {
      for (int i=0; i<LEVELS_IN_GROUP; ++i) {
        GameProgress::setLevelProgress(count-5,i,LevelProgress(3,false));
      }
    }
    CCApplication::sharedApplication()->openURL(
      "https://www.facebook.com/ivan.tsymbaliuk");
  } else if (sender == titles[1]) {
    CCApplication::sharedApplication()->openURL("http://animogo.com");
  } else if (sender == titles[2]) {
    CCApplication::sharedApplication()->openURL(
      "https://www.facebook.com/igor.chebah");
  } else if (sender == titles[3]) {
    CCApplication::sharedApplication()->openURL("http://yourdevicegames.com");
  }
}

void InfoMenu::quitMenu() {
  if (observer) {
    observer->quitInfoMenu();
  }
}
