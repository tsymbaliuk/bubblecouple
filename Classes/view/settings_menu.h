#ifndef SETTINGS_MENU_H
#define SETTINGS_MENU_H

#include "view/menu_interface.h"

using namespace cocos2d;

class Slider;
class SettingsMenu : 
  public MenuInterface, public MenuInterface::InternalObserver
{
public:
  SettingsMenu();
  class Observer {
  public:
    virtual void quitSettingsMenu() = 0;
    virtual void languageChanged() = 0;
  };
  void setObserver(Observer *observer_) {observer = observer_;}
  void onLanguageChanged();
private:
  virtual void quitMenu();
  Observer *observer;
#ifdef WIN32
  CCMenuItemToggle *createGraphicsMenu();
  void updateGraphicsMenu();

  CCMenuItemFont *graphics_low_item, *graphics_mid_item, *graphics_high_item;
#endif

  static void setSoundVolume( int value );
  static void setMusicVolume( int value );
  static void setSoundVolumeImmediately( int value );
  static void setMusicVolumeImmediately( int value );
  void toggleGraphics( CCObject * sender );
  void toggleLanguage( CCObject * sender );

  Slider *music_slider, *sounds_slider;
};

class Slider : public CCSprite, public CCTargetedTouchDelegate {
public:
  Slider(const char *label, void (*valueChangedCallback)(int), 
         void (*valueChangedImmediatelyCallback)(int), 
         int value, int foreground_height);
  void onLabelTextChanged(const char *label);
  virtual void onEnter();
  virtual void onExit();
  virtual bool ccTouchBegan(CCTouch* touch, CCEvent* );
  virtual void ccTouchMoved(CCTouch* touch, CCEvent* );
  virtual void ccTouchEnded(CCTouch* touch, CCEvent* );
private:
  void proceedTouch(CCTouch* touch);
  void updatePercentage(int new_percentage, float pos = -1);
  void (*valueChangedCallback)(int);
  void (*valueChangedImmediatelyCallback)(int);
  CCSprite *background, *slider;
  CCLabelTTF *percents_label, *text_label;
  int current_value;
};

#endif // SETTINGS_MENU_H