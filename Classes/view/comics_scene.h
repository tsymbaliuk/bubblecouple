#ifndef COMICS_SCENE_H
#define COMICS_SCENE_H

#include "cocos2d.h"

using namespace cocos2d;

class ComicsScene : public CCLayerColor
{
public:
  ComicsScene();
private:
  CCSprite *border;

  CCSprite *addSprite( 
    const char *path, CCPoint position, 
    CCActionInterval *actions = NULL, float actions_delay = 0, float z = -1);

  inline CCPoint t(short x, short y) {
    return ccp(x,border->getContentSize().height-y);
  }

  virtual bool ccTouchBegan(CCTouch *, CCEvent *);
  virtual void keyBackClicked();
  void animationFinished(float);

  bool clicked;
};

#endif // COMICS_SCENE_H