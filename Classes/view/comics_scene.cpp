#include "view/comics_scene.h"

#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/resource_loader.h"

#define moveTo(a,b) CCMoveTo::create(a,b)
#define moveBy(a,b) CCMoveBy::create(a,b)
#define rotateBy(a,b) CCRotateBy::create(a,b)
#define spawn2(a,b) CCSpawn::create(a,b,NULL)
#define spawn3(a,b,c) CCSpawn::create(a,b,c,NULL)
#define scaleTo(a,b) CCScaleTo::create(a,b)
#define fadeIn(a) CCFadeIn::create(a)
#define fadeOut(a) CCFadeOut::create(a)

ComicsScene::ComicsScene() : clicked(false) {
  CCLayerColor::initWithColor( ccc4(0,0,0,255) );
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  border = CCSprite::create("images/comics/border.png");
  border->setPosition(ccp(win_size.width/2,win_size.height/2));
  float scale = 1;
  float win_ratio = win_size.width / win_size.height;
  float border_ratio = border->getContentSize().width
                       / border->getContentSize().height;
  if (border_ratio < win_ratio) {
    // scale to height
    scale = win_size.height / border->getContentSize().height;
  } else {
    // scale to width
    scale = win_size.width / border->getContentSize().width;
  }
  border->setScale(scale);
  addChild(border);

  addSprite("images/comics/4_earth.jpg",t(1303,981),spawn2(moveBy(3,ccp(0,40)),scaleTo(3,1)),13)->setScale(0.95f);
  addSprite("images/comics/4_ball3.png",t(1538,772),spawn2(moveBy(3,ccp(-40,-40)),scaleTo(3,0.78f)),13);
  addSprite("images/comics/4_ball2.png",t(1142,896),scaleTo(3,0.85f),13);
  addSprite("images/comics/4_ball1.png",t(1422,1056),scaleTo(3,0.8f),13);
  addSprite("images/comics/4_feather2.png",t(975,936),spawn3(moveBy(3,ccp(-30,-45)),rotateBy(3,-45),scaleTo(3,0.97f)),13);
  addSprite("images/comics/4_feather1.png",t(1731,903),spawn3(moveBy(3,ccp(40,-10)),rotateBy(3,-30),scaleTo(3,0.95f)),13);

  CCSprite *spoiler4 = addSprite("images/comics/spoiler.png",t(1339,955),fadeOut(0.5f),13,0);
  spoiler4->setScaleX(1210);
  spoiler4->setScaleY(687);

  addSprite("images/comics/3_sky.jpg",t(425,938),NULL,8);
  addSprite("images/comics/3_crown2.png",t(521,771),moveBy(4,ccp(-140,0)),8);
  addSprite("images/comics/3_ufo.png",t(421,1037),rotateBy(4,-10),8);
  addSprite("images/comics/3_ball3.png",t(620,796),moveBy(4,ccp(10,40)),8);
  addSprite("images/comics/3_ball2.png",t(161,934),moveBy(4,ccp(-20,30)),8);
  addSprite("images/comics/3_ball1.png",t(363,811),moveBy(4,ccp(0,45)),8);
  addSprite("images/comics/3_crown1.png",t(683,1094),moveBy(4,ccp(-250,0)),8);
  CCSprite *text_bg = addSprite("images/comics/3_text_bg.png",t(465,1140),spawn2(moveBy(2,ccp(355,-35)),scaleTo(2,1)),8.2f,0);
  text_bg->setScale(0);
  CCLabelTTF *karr_label = CCLabelTTF::create(tr(Karr), tr(FontName), text_bg->getContentSize().height / 3);
  karr_label->setPosition(ccp(text_bg->getContentSize().width/2,text_bg->getContentSize().height/2));
  karr_label->setColor(ccBLACK);
  text_bg->addChild(karr_label);

  CCSprite *spoiler3 = addSprite("images/comics/spoiler.png",t(425,938),fadeOut(0.5f),8);
  spoiler3->setScaleX(624);
  spoiler3->setScaleY(688);

  addSprite("images/comics/2_sky.jpg",t(1503,344),moveBy(3,ccp(0,10)),4);
  addSprite("images/comics/2_earth.png",t(1504,474),moveBy(3,ccp(0,15)),4);
  addSprite("images/comics/2_cloud3.png",t(1284,139),moveBy(3,ccp(-40,60)),4);
  addSprite("images/comics/2_cloud2.png",t(1547,139),moveBy(3,ccp(0,50)),4);
  addSprite("images/comics/2_cloud1.png",t(1745,212),moveBy(3,ccp(40,60)),4);
  addSprite("images/comics/2_birds.png",t(1700,230),spawn2(moveBy(3,ccp(-80,-30)),scaleTo(3,1)),4)->setScale(0.7f);
  addSprite("images/comics/2_ufo.png",t(1451,370),NULL,4);
  addSprite("images/comics/2_ball3.png",t(1668,377),moveBy(3,ccp(10,-10)),4);
  addSprite("images/comics/2_ball2.png",t(1403,480),moveBy(3,ccp(0,-10)),4);
  addSprite("images/comics/2_ball1.png",t(1558,462),moveBy(3,ccp(10,-10)),4);

  CCSprite *spoiler2 = addSprite("images/comics/spoiler.png",t(1503,344),fadeOut(0.5f),4);
  spoiler2->setScaleX(850);
  spoiler2->setScaleY(590);

  addSprite("images/comics/1_background.jpg",t(599,321));
  addSprite("images/comics/1_background.png",t(1124,321));
  addSprite("images/comics/1_atmosphere.png",t(853,338),scaleTo(3,1))->setScale(0.95f);
  addSprite("images/comics/1_moon.png",t(987,214),spawn2(moveBy(3,ccp(-40,10)),scaleTo(3,1)))->setScale(0.8f);
  addSprite("images/comics/1_earth.png",t(852,338),scaleTo(3,1))->setScale(0.95f);
  addSprite("images/comics/1_ufo.png",t(200,267),spawn2(moveBy(3,ccp(300,-60)),scaleTo(3,0.7f)));

  setKeypadEnabled(true);
  setTouchEnabled(true);
  setTouchMode(kCCTouchesOneByOne);

  border->setScale(scale*2);
  border->setPosition(ccp(win_size.width/2+(1024-634)*2*scale,
                          win_size.height/2-(668-331)*2*scale));

  class PlayMusicAction : public CCFiniteTimeAction {
  public:
    PlayMusicAction() {autorelease();}
    virtual void startWithTarget(CCNode *) {
      SoundsManager::get().playComicsMusic();
    }
  };

  border->runAction(
    CCSequence::create(
    new PlayMusicAction(),
    CCDelayTime::create(3.5f),
    CCEaseSineOut::create(moveTo(0.5f,
      ccp(win_size.width/2+(1024-1503)*2*scale,
          win_size.height/2-(668-344)*2*scale))),
      NULL));
  border->runAction(
    CCSequence::createWithTwoActions(
    CCDelayTime::create(7.5f),
    CCEaseSineOut::create(moveTo(0.5f,
      ccp(win_size.width/2+(1024-425)*2*scale,
          win_size.height/2-(668-938)*2*scale)))));
  border->runAction(
    CCSequence::createWithTwoActions(
    CCDelayTime::create(12.5f),
    CCEaseSineOut::create(moveTo(0.5f,
      ccp(win_size.width/2+(1024-1303)*2*scale,
          win_size.height/2-(668-981)*2*scale)))));
  border->runAction(
    CCSequence::createWithTwoActions(
      CCDelayTime::create(16),
      CCEaseSineOut::create(
        spawn2(
          moveTo(0.5f,ccp(win_size.width/2,win_size.height/2)),
          scaleTo(0.5f,scale)
        )
      )
    )
  );
  scheduleOnce(schedule_selector(ComicsScene::animationFinished),19);
}

CCSprite *ComicsScene::addSprite( 
  const char *path, CCPoint position, 
  CCActionInterval *actions /*= NULL*/, float actions_delay /*= 0*/,
  float z /* = -1 */) {
  CCSprite *sprite = CCSprite::create(path);
  sprite->setPosition(position);
  if (actions) {
    sprite->runAction(
      CCSequence::createWithTwoActions(
        CCDelayTime::create(actions_delay),CCEaseSineOut::create(actions)));
  }
  border->addChild(sprite,z);
  return sprite;
}

bool ComicsScene::ccTouchBegan( CCTouch *, CCEvent * ) {
  keyBackClicked();
  return true;
}

void ComicsScene::keyBackClicked() {
  if (!clicked) {
    clicked = true; // Double click prevention
    CCDirector::sharedDirector()->popScene();
    SoundsManager::get().playMainMenuMusic();
  }
}

void ComicsScene::animationFinished(float) {
  keyBackClicked();
}
