#ifndef RESOURCE_LOADER_H
#define RESOURCE_LOADER_H

#include "cocos2d.h"

using namespace cocos2d;

void provideGroundResources();

int getResourceWidth();

CCSprite* spriteFromMask(CCSprite* source, CCSprite* mask);
CCSprite* spriteFromMask(const char* source_path, const char* mask_path);

#endif // RESOURCE_LOADER_H
