#ifndef GLOBAL_INFO_H
#define GLOBAL_INFO_H

#include "cocos2d.h"

using namespace cocos2d;

class GlobalInfo : public CCSprite
{
public:
  static GlobalInfo *get();
  void queueText(const std::string &text);
private:
  GlobalInfo();
  void show();
  void showingFinished(float);
  static GlobalInfo *gi;
  std::list<std::string> texts_to_show;
  bool is_showing;

  CCSprite *bg;
  CCLabelTTF *label;
};

#endif // GLOBAL_INFO_H