#include "view/resource_loader.h"

#include "model/level_manager.h"
#include "view/view_settings.h"
#include <fstream>

using namespace cocos2d;

void provideGroundResources() {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  CCRenderTexture *target = CCRenderTexture::create(
    win_size.width, win_size.height, kCCTexture2DPixelFormat_RGBA8888);
  target->retain();
  target->setPosition(ccp(win_size.width / 2, win_size.height / 2));
  target->beginWithClear(1,1,1,0);

  for (int group = 0; group < LEVEL_GROUPS_COUNT; ++group)  {
    char background_path[100], foreground_path[100];
    char background_scaled_path[100], foreground_scaled_path[100];
    sprintf(background_path,"images/levels/%i_bg.jpg",group+1);
    sprintf(foreground_path,"images/levels/%i_fg.png",group+1);
    sprintf(background_scaled_path,"%i_%i_%i_bg.png",
            int(win_size.width), int(win_size.height), group+1);
    sprintf(foreground_scaled_path,"%i_%i_%i_fg.png",
            int(win_size.width), int(win_size.height), group+1);
    CCSprite *background = CCSprite::create(background_path);
    CCSprite *foreground = CCSprite::create(foreground_path);

    std::string bg_full_path = CCFileUtils::sharedFileUtils()->getWritablePath()+background_scaled_path;
    std::string fg_full_path = CCFileUtils::sharedFileUtils()->getWritablePath()+foreground_scaled_path;
    std::ifstream 
      bg_ifstream(bg_full_path.c_str()), fg_ifstream(fg_full_path.c_str());

    if (bg_ifstream && fg_ifstream) {
      continue;
    }

    float scale_factor = 1;
    float bg_ratio = background->getContentSize().width
      / background->getContentSize().height;
    float win_ratio = win_size.width / win_size.height;

    if (bg_ratio > win_ratio) {
      // scale to height
      scale_factor = win_size.height / background->getContentSize().height;
      background->setPosition(ccp(win_size.width/2, win_size.height/2));
    } else {
      // scale to width
      scale_factor = win_size.width / background->getContentSize().width;
      background->setPosition(ccp(
        win_size.width/2, background->getContentSize().height*scale_factor/2));
    }
    foreground->setPosition(background->getPosition());

    background->setScale(scale_factor);
    foreground->setScale(scale_factor);

    target->clear(1,1,1,0.5);
    background->visit();
    target->saveToFile(background_scaled_path, kCCImageFormatPNG);
    background->release();

    target->clear(1,1,1,0.5);
    foreground->visit();
    target->saveToFile(foreground_scaled_path, kCCImageFormatPNG);
    foreground->release();
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
  }

  target->end();
  target->release();
  CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}

int getResourceWidth() {
  const int graphics[3] = {512, 1024, 2048};
  return graphics[getViewSettings().getGraphics()];
}

CCSprite* spriteFromMask( CCSprite* source, CCSprite* mask ) {
  CCPoint textureSpriteOrigPosition(
    source->getPosition().x, source->getPosition().y);

  source->setPosition(ccp(
    source->getContentSize().width/2, source->getContentSize().height/2));
  mask->setPosition(ccp(
    mask->getContentSize().width/2, mask->getContentSize().height/2));

  CCRenderTexture* rt = CCRenderTexture::create(
    static_cast<int>(mask->getContentSize().width), 
    static_cast<int>(mask->getContentSize().height));

  ccBlendFunc bfmask = ccBlendFunc();
  bfmask.src = GL_ONE;
  bfmask.dst = GL_ZERO;
  mask->setBlendFunc(bfmask);

  mask->getTexture()->setAliasTexParameters();

  ccBlendFunc bfTexture = ccBlendFunc();
  bfTexture.src = GL_DST_ALPHA;
  bfTexture.dst = GL_ZERO;
  source->setBlendFunc(bfTexture);

  rt->begin();
  mask->visit();
  source->visit();
  rt->end();

  CCSprite* ret = CCSprite::createWithTexture(rt->getSprite()->getTexture());
  ret->setFlipY(true);

  // restore the original sprite positions
  source->setPosition(textureSpriteOrigPosition);

  return ret;
}

CCSprite* spriteFromMask( const char* source_path, const char* mask_path ) {
  CCSprite *source_sprite = CCSprite::create(source_path);
  CCSprite *mask_sprite = CCSprite::create(mask_path);
  return spriteFromMask(source_sprite,mask_sprite);
}

