#include "view/view_settings.h"

#include "cocos2d.h"

using namespace cocos2d;

void ViewSettings::setSoundsLevel( int level ) {
  if (sound_level == level) return;
  sound_level = level;
  CCUserDefault::sharedUserDefault()->setIntegerForKey("sound",level);
}

void ViewSettings::setMusicLevel( int level ) {
  if (music_level == level) return;
  music_level = level;
  CCUserDefault::sharedUserDefault()->setIntegerForKey("music",level);
}

void ViewSettings::setComicsBeenSeen( bool state ) {
  comics_was_seen = state;
  CCUserDefault::sharedUserDefault()->setBoolForKey("comics_was_seen",state);
}

void ViewSettings::setGraphics( Graphics state ) {
  graphics = state;
  CCUserDefault::sharedUserDefault()->setIntegerForKey("graphics",state);
}

void ViewSettings::setLanguage( Language state ) {
  language = state;
  CCUserDefault::sharedUserDefault()->setIntegerForKey("language",state);
}

void ViewSettings::setCurrentLevelGroup( int current_level_group_ ) {
  current_level_group = current_level_group_;
  CCUserDefault::sharedUserDefault()->
    setIntegerForKey("current_level_group",current_level_group);
}

Language getCurrentLanguage() {
  switch(CCApplication::sharedApplication()->getCurrentLanguage()) {
  case kLanguageEnglish: return English;
  case kLanguageUkrainian: return Ukrainian;
  case kLanguageGerman: return German;
  case kLanguageSpanish: return Spanish;
  case kLanguageRussian: return Russian;
  case kLanguageLithuanian: return Lithuanian;
  default: return English;
  }
}

ViewSettings::ViewSettings() :
  sound_level(CCUserDefault::sharedUserDefault()->getIntegerForKey("sound",90)),
  music_level(CCUserDefault::sharedUserDefault()->getIntegerForKey("music",80)),
  comics_was_seen(
    CCUserDefault::sharedUserDefault()->getBoolForKey("comics_was_seen",false)),
  current_level_group(CCUserDefault::sharedUserDefault()->
    getIntegerForKey("current_level_group",0)),
  language(static_cast<Language>(CCUserDefault::sharedUserDefault()->
    getIntegerForKey("language",getCurrentLanguage()))) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  int sizes[] = {512,1024,2048};
  int i;
  for (i=0;i<sizeof(sizes)/sizeof(int)-1; ++i) {
    if (win_size.width <= sizes[i]) {
      break;
    }
  }
  graphics = static_cast<Graphics>(
    CCUserDefault::sharedUserDefault()->getIntegerForKey("graphics",i));
}

bool ViewSettings::isUserSawInfoMessage( ViewInterface::InfoMessage info ) {
  char path[40];
  sprintf(path,"info_%i",static_cast<int>(info));
  return CCUserDefault::sharedUserDefault()->getBoolForKey( path, false );
}

void ViewSettings::setUserSawInfoMessage( ViewInterface::InfoMessage info ) {
  char path[40];
  sprintf(path,"info_%i",static_cast<int>(info));
  CCUserDefault::sharedUserDefault()->setBoolForKey( path, true );
}

ViewSettings& getViewSettings() {
  static ViewSettings vs;
  return vs;
}
