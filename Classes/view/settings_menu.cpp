#include "view/settings_menu.h"

#include "view/global_info.h"
#include "view/resource_loader.h"
#include "view/sounds_manager.h"
#include "view/translator.h"
#include "view/view_settings.h"
#include "SimpleAudioEngine.h"

SettingsMenu::SettingsMenu() : observer(NULL) {
  setHeaderText(tr(Settings));
  setInternalObserver(this);

  CCMenuItemFont::setFontName(tr(FontName));
  CCMenuItemFont::setFontSize(foreground_height / 14);

  CCMenuItemToggle *language = CCMenuItemToggle::createWithTarget(this, 
    menu_selector(SettingsMenu::toggleLanguage),
    CCMenuItemFont::create( languageString(0) ),
    CCMenuItemFont::create( languageString(1) ),
    CCMenuItemFont::create( languageString(2) ),
    CCMenuItemFont::create( languageString(3) ),
    CCMenuItemFont::create( languageString(4) ),
    CCMenuItemFont::create( languageString(5) ),
    NULL );
  language->setSelectedIndex(getViewSettings().getLanguage());

  CCMenuItemToggle *graphics = 
#ifdef WIN32
    createGraphicsMenu();
#else
    NULL;
#endif
  CCMenu *menu = CCMenu::create( language, graphics, NULL );

  menu->alignItemsVerticallyWithPadding(foreground_height / 30);
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  menu->setPositionY(win_size.height/2-foreground_height*0.2);

  content->addChild( menu );

  music_slider = new Slider(
    tr(Music), &setMusicVolume, &setMusicVolumeImmediately,
    SoundsManager::get().getMusicVolume(), foreground_height);
  music_slider->setPosition(ccp(win_size.width/2,
                                win_size.height/2+foreground_height*0.15));
  content->addChild( music_slider );

  sounds_slider = new Slider(
    tr(Sounds), &setSoundVolume, &setSoundVolumeImmediately,
    SoundsManager::get().getSoundsVolume(), foreground_height);
  sounds_slider->setPosition(ccp(win_size.width/2,
                                 win_size.height/2+foreground_height*0.02));
  content->addChild( sounds_slider );
}

void SettingsMenu::onLanguageChanged() {
  setHeaderText(tr(Settings));
  music_slider->onLabelTextChanged(tr(Music));
  sounds_slider->onLabelTextChanged(tr(Sounds));
#ifdef WIN32
  updateGraphicsMenu();
#endif
}

void SettingsMenu::quitMenu() {
  if (observer) {
    observer->quitSettingsMenu();
  }
}

#ifdef WIN32
CCMenuItemToggle * SettingsMenu::createGraphicsMenu() {
  CCMenuItemToggle *graphics = CCMenuItemToggle::createWithTarget(this, 
    menu_selector(SettingsMenu::toggleGraphics),
    graphics_low_item = CCMenuItemFont::create( tr(GraphicsLow) ),
    graphics_mid_item = CCMenuItemFont::create( tr(GraphicsMid) ),
    graphics_high_item = CCMenuItemFont::create( tr(GraphicsHigh) ),
    NULL );
  graphics->setSelectedIndex(static_cast<int>(getViewSettings().getGraphics()));
  updateGraphicsMenu();
  return graphics;
}

void SettingsMenu::updateGraphicsMenu() {
  char graphics_low[100];
  char graphics_mid[100];
  char graphics_high[100];
  strcpy(graphics_low,tr(GraphicsLow));
  strcpy(graphics_mid,tr(GraphicsMid));
  strcpy(graphics_high,tr(GraphicsHigh));
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  int sizes[] = {512,1024,2048};
  int i;
  for (i=0;i<sizeof(sizes)/sizeof(int)-1; ++i) {
    if (win_size.width <= sizes[i]) {
      break;
    }
  }
  switch(i) {
  case 0: 
    sprintf(graphics_low,"%s (%s)",tr(GraphicsLow),tr(RecommendedGraphics)); 
    break;
  case 1: 
    sprintf(graphics_mid,"%s (%s)",tr(GraphicsMid),tr(RecommendedGraphics)); 
    break;
  case 2: 
    sprintf(graphics_high,"%s (%s)",tr(GraphicsHigh),tr(RecommendedGraphics)); 
    break;
  }

  graphics_low_item->setString(graphics_low);
  graphics_mid_item->setString(graphics_mid);
  graphics_high_item->setString(graphics_high);
}
#endif

void SettingsMenu::setSoundVolume( int value ) {
  SoundsManager::get().setSoundsVolume(value);
}

void SettingsMenu::setMusicVolume( int value ) {
  SoundsManager::get().setMusicVolume(value);
}

void SettingsMenu::setSoundVolumeImmediately( int value ) {
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setEffectsVolume(value*0.01);
}

void SettingsMenu::setMusicVolumeImmediately( int value ) {
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setBackgroundMusicVolume(value*0.01);
}
void SettingsMenu::toggleGraphics( CCObject * sender ) {
  getViewSettings().setGraphics(
    static_cast<ViewSettings::Graphics> (
      static_cast<CCMenuItemToggle *>(sender)->getSelectedIndex()));
}

void SettingsMenu::toggleLanguage( CCObject * sender ) {
  getViewSettings().setLanguage(
    static_cast<Language> (
      static_cast<CCMenuItemToggle *>(sender)->getSelectedIndex()));
  if (observer) {
    observer->languageChanged();
  }
}

Slider::Slider( const char *label, void (*valueChangedCallback)(int), 
                void (*valueChangedImmediatelyCallback)(int), 
                int value, int foreground_height ) : 
  valueChangedCallback(valueChangedCallback), 
  valueChangedImmediatelyCallback(valueChangedImmediatelyCallback), 
  current_value(value) {
  CCSprite::init();
  autorelease();

  char path[100];
  sprintf(path,"images/menu/slider_background_%i.png",getResourceWidth());
  background = CCSprite::create(path);
  float scale = foreground_height * 0.06 / background->getContentSize().height;
  background->setScale(scale);
  addChild(background);

  sprintf(path,"images/menu/slider_%i.png",getResourceWidth());
  slider = CCSprite::create(path);
  slider->setScale(scale);
  addChild(slider);

  percents_label = CCLabelTTF::create(
    "", tr(FontName), background->getContentSize().height*scale*1.2f);
  ccColor3B color = {214, 216, 213};
  percents_label->setColor(color);
  percents_label->setPositionY(background->getContentSize().height*scale*0.04);
  addChild(percents_label);
  updatePercentage(value);

  text_label = CCLabelTTF::create(
    "", tr(FontName), background->getContentSize().height*scale*1.2f);
  text_label->setColor(color);
  addChild(text_label);
  onLabelTextChanged(label);
}

void Slider::onLabelTextChanged(const char *label) {
  text_label->setString(label);
  text_label->setPositionX(
    - background->getContentSize().width*0.54*background->getScale() 
    - text_label->getContentSize().width/2);
  text_label->setPositionY(slider->getPositionY());
}

void Slider::onEnter() {
  CCDirector::sharedDirector()->getTouchDispatcher()->
    addTargetedDelegate(this, -1, true);
  CCSprite::onEnter();
}

void Slider::onExit() {
  CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
  CCSprite::onExit();
}

bool Slider::ccTouchBegan(CCTouch* touch, CCEvent* ) {
  CCSize s = background->getContentSize()*background->getScale();
  CCRect r(-s.width / 2, -s.height / 2, s.width, s.height);
  if (r.containsPoint(convertTouchToNodeSpaceAR(touch))) {
    proceedTouch(touch);
    return true;
  }
  return false;
}

void Slider::ccTouchMoved(CCTouch* touch, CCEvent* ) {
  proceedTouch(touch);
}

void Slider::ccTouchEnded( CCTouch* touch, CCEvent* ) {
  proceedTouch(touch);
  valueChangedCallback(current_value);
}

void Slider::proceedTouch( CCTouch* touch ) {
  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  float scale = background->getScale();
  float bg_width = background->getContentSize().width * scale;
  float slider_width = slider->getContentSize().width * scale;
  float pos_x = touch->getLocationInView().x - win_size.width/2;
  if (pos_x < -bg_width/2 + slider_width/2) {
    pos_x = -bg_width/2 + slider_width/2;
  } else if (pos_x > bg_width/2 - slider_width/2) {
    pos_x = bg_width/2 - slider_width/2;
  }
  int percent = 
    (pos_x + bg_width/2 - slider_width/2) * 100 / (bg_width - slider_width);
  updatePercentage(percent,pos_x);
}

void Slider::updatePercentage( int new_percentage, float pos ) {
  float scale = background->getScale();
  float bg_width = background->getContentSize().width * scale;
  if (pos == -1) {
    float slider_width = slider->getContentSize().width * scale;
    pos = - bg_width/2 + slider_width/2 
          + new_percentage * (bg_width - slider_width) / 100;
  }
  slider->setPositionX(pos);
  char percents_text[10];
  sprintf(percents_text,"%i%%",new_percentage);
  percents_label->setString(percents_text);
  percents_label->setPositionX(
    bg_width*0.54 + percents_label->getContentSize().width/2);
  percents_label->setPositionY(slider->getPositionY());
  current_value = new_percentage;
  valueChangedImmediatelyCallback(current_value);
}
