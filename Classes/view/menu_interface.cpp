#include "view/menu_interface.h"

#include "view/resource_loader.h"
#include "view/sounds_manager.h"
#include "view/translator.h"

MenuInterface::MenuInterface(CCNode *parent) : internal_observer(NULL) {
  CCLayer::init();
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();
  char path[100],path_p[100];
  sprintf(path,"images/menu/stars_%i.jpg",getResourceWidth());
  CCSprite *background = CCSprite::create(path);
  background->setPosition(ccp(win_size.width/2,win_size.height/2));
  float scale = 1;
  float win_ratio = win_size.width / win_size.height;
  float bg_ratio = background->getContentSize().width
                   / background->getContentSize().height;
  if (bg_ratio > win_ratio) {
    // scale to height
    scale = win_size.height / background->getContentSize().height;
  } else {
    // scale to width
    scale = win_size.width / background->getContentSize().width;
  }
  background->setScale(scale);

  sprintf(path,"images/menu/level_select_bg_%i.jpg",getResourceWidth());
  CCSprite *level_select_background = CCSprite::create(path);
  addChild(level_select_background,-1);

  content = CCLayer::create();
  addChild(content);

  sprintf(path,"images/menu/level_select_fg_%i.png",getResourceWidth());
  foreground = CCSprite::create(path);
  foreground->setPosition(ccp(win_size.width/2,win_size.height/2));
  bg_ratio = foreground->getContentSize().width
             / foreground->getContentSize().height;
  if (bg_ratio < win_ratio) {
    scale = win_size.height / foreground->getContentSize().height;
  } else {
    scale = win_size.width / foreground->getContentSize().width;
  }
  foreground->setScale(scale);

  foreground_width = foreground->getContentSize().width*foreground->getScale();
  foreground_height = 
    foreground->getContentSize().height*foreground->getScale();

  sprintf(
    path,"images/menu/select_level_close_button_%i.png",getResourceWidth());
  sprintf(path_p,
    "images/menu/select_level_close_button_pressed_%i.png",getResourceWidth());
  CCMenuItemImage *back = CCMenuItemImage::create(
    path, path_p, this, menu_selector(MenuInterface::quitCallback) );
  back->setPosition(ccp(-back->getContentSize().width*1.085,
    -back->getContentSize().height*1.11));
  CCMenu* menu = CCMenu::create(back, NULL);
  menu->setPosition(ccp(foreground->getContentSize().width,
    foreground->getContentSize().height));
  foreground->addChild(menu);

  level_select_background->setPosition(foreground->getPosition());
  level_select_background->setScale(foreground->getScale());

  header_label = CCLabelTTF::create("", tr(FontName), 
                                    foreground->getContentSize().height*0.07f);
  ccColor3B color = {16, 34, 44};
  header_label->setColor(color);
  header_label->setPosition(ccp(foreground->getContentSize().width*0.5f, 
                                foreground->getContentSize().height*0.93f));
  foreground->addChild(header_label);

  if (parent) {
    parent->addChild(foreground,101);
  } else {
    addChild(foreground);
  }
}

void MenuInterface::setHeaderText( const char * text ) {
  header_label->setString(text);
}

void MenuInterface::keyBackClicked() {
  if (internal_observer) {
    internal_observer->quitMenu();
  }
}

void MenuInterface::quitCallback( cocos2d::CCObject* ) {
  SoundsManager::get().playSound(ButtonSound);
  if (internal_observer) {
    internal_observer->quitMenu();
  }
}
