#include "view/sounds_manager.h"

#include "view/view_settings.h"
#include "SimpleAudioEngine.h"

#define EXTERNAL_SOUNS_PATH ""
#ifdef CC_TARGET_OS_IPHONE
#define MUSIC_EXTENSION "m4a"
#define SOUNDS_EXTENSION "aifc"
#elif WIN32
#define MUSIC_EXTENSION "wav"
#define SOUNDS_EXTENSION "wav"
#undef EXTERNAL_SOUNS_PATH
#define EXTERNAL_SOUNS_PATH "../WinResources/"
#else
#define MUSIC_EXTENSION "ogg"
#define SOUNDS_EXTENSION "ogg"
#endif

void preloadEffect(const char *path) {
  CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffect(path);
}

void preloadEffects() {
  if (CocosDenshion::SimpleAudioEngine::sharedEngine()->isGnusmas9100()) return;
  preloadEffect("sounds/bubbles/alone_click."SOUNDS_EXTENSION);
  preloadEffect("sounds/bubbles/decrease."SOUNDS_EXTENSION);
  preloadEffect("sounds/bubbles/disappear."SOUNDS_EXTENSION);
  preloadEffect("sounds/bubbles/explosion."SOUNDS_EXTENSION);
  char path[100];
  for (int i=4; i<=10; i+=2) {
    sprintf(path,"sounds/bubbles/collisions/bb_%i."SOUNDS_EXTENSION,i);
    preloadEffect(path);
    sprintf(path,"sounds/bubbles/collisions/bg_%i."SOUNDS_EXTENSION,i);
    preloadEffect(path);
    sprintf(path,"sounds/bubbles/collisions/rb_%i."SOUNDS_EXTENSION,i);
    preloadEffect(path);
    sprintf(path,"sounds/bubbles/collisions/rg_%i."SOUNDS_EXTENSION,i);
    preloadEffect(path);
  }
}

unsigned int playEffect(char *path) {
  if (CocosDenshion::SimpleAudioEngine::sharedEngine()->isGnusmas9100()) {
    return 0;
  }
  if (getViewSettings().getSoundsLevel() > 0) {
    return CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(path);
  }
  return 0;
} 

unsigned int playEffect(char *effect, int power) {
  power *= 2;
  if (power < 5) return 0;
  if (power > 10) power = 10;
  if (power % 2 == 1) --power;
  char path[100];
  sprintf(path,effect,power);
  return playEffect(path);
}

SoundsManager & SoundsManager::get() {
  static SoundsManager sm;
  return sm;
}

void SoundsManager::setSoundsVolume( int value ) {
  getViewSettings().setSoundsLevel(value);
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setEffectsVolume(value*0.01);
}

void SoundsManager::setMusicVolume( int value ) {
  getViewSettings().setMusicLevel(value);
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setBackgroundMusicVolume(value*0.01);
}

void SoundsManager::lowMusicVolume() {
  if (getMusicVolume() > 5) {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->
      setBackgroundMusicVolume(getSoundsVolume()*0.001);
  }
}

void SoundsManager::restoreMusicVolume() {
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setBackgroundMusicVolume(getMusicVolume()*0.01);
}
int SoundsManager::getSoundsVolume() {
  return getViewSettings().getSoundsLevel();
}

int SoundsManager::getMusicVolume() {
  return getViewSettings().getMusicLevel();
}

void SoundsManager::playSound( SoundEffect sound_id ) {
  switch(sound_id) {
  case BubbleExploded:
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bubbles/explosion."SOUNDS_EXTENSION);
    break;
  case BubbleDisappeared: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bubbles/disappear."SOUNDS_EXTENSION);
    break;
  case BubbleBecomeSmaller: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bubbles/decrease."SOUNDS_EXTENSION);
    break;
  case AloneBubbleClicked: 
    playEffect(
      EXTERNAL_SOUNS_PATH"sounds/bubbles/alone_click."SOUNDS_EXTENSION);
    break;
  case GamePausedSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/pause."SOUNDS_EXTENSION);
    break;
  case ButtonSound: 
    playEffect(
      EXTERNAL_SOUNS_PATH"sounds/menu/button_pressed."SOUNDS_EXTENSION);
    break;
  case BonusAppearedSound: 
    playEffect(
      EXTERNAL_SOUNS_PATH"sounds/bonuses/bonus_appeared."SOUNDS_EXTENSION);
    break;
  case BubbleRainSound: 
    playEffect(
      EXTERNAL_SOUNS_PATH"sounds/bonuses/bubble_rain."SOUNDS_EXTENSION);
    break;
  case CreatorSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bonuses/creator."SOUNDS_EXTENSION);
    break;
  case EarthquakeSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bonuses/earthquake."SOUNDS_EXTENSION);
    break;
  case ShufflerSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bonuses/shuffler."SOUNDS_EXTENSION);
    break;
  case SplitterSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/bonuses/splitter."SOUNDS_EXTENSION);
    break;
  case BackgroundSound0: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/do_nothing_0."SOUNDS_EXTENSION);
    break;
  case BackgroundSound1: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/do_nothing_1."SOUNDS_EXTENSION);
    break;
  case BackgroundSound2: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/do_nothing_2."SOUNDS_EXTENSION);
    break;
  case BackgroundSound3: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/do_nothing_3."SOUNDS_EXTENSION);
    break;
  case Countdown1: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/1."SOUNDS_EXTENSION);
    break;
  case Countdown2: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/2."SOUNDS_EXTENSION);
    break;
  case Countdown3: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/3."SOUNDS_EXTENSION);
    break;
  case Countdown4: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/4."SOUNDS_EXTENSION);
    break;
  case Countdown5: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/5."SOUNDS_EXTENSION);
    break;
  case Countdown6: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/6."SOUNDS_EXTENSION);
    break;
  case Countdown7: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/7."SOUNDS_EXTENSION);
    break;
  case Countdown8: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/8."SOUNDS_EXTENSION);
    break;
  case Countdown9: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/9."SOUNDS_EXTENSION);
    break;
  case Countdown10: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/countdown/10."SOUNDS_EXTENSION);
    break;
  case FirstStarSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/1star."SOUNDS_EXTENSION);
    break;
  case SecondStarSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/2star."SOUNDS_EXTENSION);
    break;
  case ThirdStarSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/3star."SOUNDS_EXTENSION);
    break;
  case GameOverSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/game_over."SOUNDS_EXTENSION);
    break;
  case GreatSound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/great."SOUNDS_EXTENSION);
    break;
  case VictorySound: 
    playEffect(EXTERNAL_SOUNS_PATH"sounds/menu/victory."SOUNDS_EXTENSION);
    break;
  }
}

void SoundsManager::collision( float power, bool rubber, bool with_ground ) {
  power *= 10;
  if (with_ground) {
    if (rubber) {
      playEffect(
        EXTERNAL_SOUNS_PATH"sounds/bubbles/collisions/rg_%i."SOUNDS_EXTENSION,
        power);
    } else {
      playEffect(
        EXTERNAL_SOUNS_PATH"sounds/bubbles/collisions/bg_%i."SOUNDS_EXTENSION,
        power);
    }
  } else {
    if (rubber) {
      playEffect(
        EXTERNAL_SOUNS_PATH"sounds/bubbles/collisions/rb_%i."SOUNDS_EXTENSION,
        power);
    } else {
      playEffect(
        EXTERNAL_SOUNS_PATH"sounds/bubbles/collisions/bb_%i."SOUNDS_EXTENSION,
        power);
    }
  }
}

void SoundsManager::playComicsMusic() {
  if (getViewSettings().getMusicLevel() > 0) {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(
      EXTERNAL_SOUNS_PATH "music/comics." MUSIC_EXTENSION,false);
    now_playing = ComicsMusic;
  }
}

void SoundsManager::playMainMenuMusic() {
  if (getViewSettings().getMusicLevel() > 0) {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(
      EXTERNAL_SOUNS_PATH "music/menu." MUSIC_EXTENSION,true);
    now_playing = MainMenuMusic;
  }
}

void SoundsManager::playMusic( int group, bool insane ) {
  if (group < 0) return;
  if (getViewSettings().getMusicLevel() > 0) {
    if (current_group == group && current_insane == insane && 
        now_playing == LevelMusic) {
      CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    } else {
      current_group = group;
      current_insane = insane;
      now_playing = LevelMusic;
      char path[100];
      sprintf(path,EXTERNAL_SOUNS_PATH "music/%i%s." MUSIC_EXTENSION,
              group,insane?"i":"");
      CocosDenshion::SimpleAudioEngine::sharedEngine()->
        playBackgroundMusic(path,true);
    }
  }
}

void SoundsManager::pauseMusic() {
  CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

void SoundsManager::restoreMenuOrComicsMusic() {
  if (now_playing == ComicsMusic || now_playing == MainMenuMusic) {
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
  }
}

SoundsManager::SoundsManager() : current_group(-1), now_playing(None) {
  preloadEffects();
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setBackgroundMusicVolume(getMusicVolume()*0.01);
  CocosDenshion::SimpleAudioEngine::sharedEngine()->
    setEffectsVolume(getSoundsVolume()*0.01);
}
