#include "view/global_info.h"

#include "view/resource_loader.h"
#include "view/translator.h"

GlobalInfo * GlobalInfo::get() {
  if (gi) return gi;
  return (gi = new GlobalInfo());
}

void GlobalInfo::queueText( const std::string &text ) {
  texts_to_show.push_back(text);
  show();
}

GlobalInfo::GlobalInfo() : is_showing(false) {
  CCSprite::init();
  autorelease();

  CCSize win_size = CCDirector::sharedDirector()->getWinSize();

  bg = CCSprite::create("images/menu/locked_bg_color.png");
  bg->setScaleX(win_size.width);
  bg->setScaleY(0.2*win_size.height);
  bg->setPositionX(win_size.width/2);
  bg->setPositionY(0.12*win_size.height);
  bg->setOpacity(0);
  addChild(bg);

  label = CCLabelTTF::create("", tr(FontName), win_size.height * 0.07);
  label->setColor(ccWHITE);
  label->setPosition(bg->getPosition());
  label->setOpacity(0);
  addChild(label);
}

void GlobalInfo::show() {
  if (!texts_to_show.empty() && !is_showing) {
    is_showing = true;
    label->setString((*texts_to_show.begin()).c_str());
    texts_to_show.pop_front();
    bg->runAction(CCEaseSineOut::create(CCFadeTo::create(0.3f,245)));
    label->runAction(CCEaseSineOut::create(CCFadeTo::create(0.3f,255)));
    // scheduleOnce doesn't work correct
    schedule(schedule_selector(GlobalInfo::showingFinished),1.3f);
  }
}

void GlobalInfo::showingFinished(float) {
  is_showing = false;
  unscheduleAllSelectors();
  if (texts_to_show.empty()) {
    bg->runAction(CCEaseSineOut::create(CCFadeTo::create(0.3f,0)));
    label->runAction(CCEaseSineOut::create(CCFadeTo::create(0.3f,0)));
  } else {
    show();
  }
}

GlobalInfo * GlobalInfo::gi = NULL;
