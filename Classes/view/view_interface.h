#ifndef BALLVIEWINTERFACE_H
#define BALLVIEWINTERFACE_H

#include "Box2D/Box2D.h"
#include "model/bubble_parameters.h"

namespace cocos2d {
  class CCSprite;
}
struct Spark {
  Spark (BubbleParameters *ball1, BubbleParameters *ball2) :
    ball1(ball1), ball2(ball2), bubble_sparks(NULL) {}
  BubbleParameters *ball1, *ball2;
  
  cocos2d::CCSprite *bubble_sparks;
  b2Vec2 getPosition() {
    return 0.5 * (ball1->position + ball2->position);
  }
  bool operator ==(Spark other) {
    return (ball1 == other.ball1 && ball2 == other.ball2) ||
           (ball1 == other.ball2 && ball2 == other.ball1);
  }
  bool operator !=(Spark other) {
    return !operator ==(other);
  }
};

class SoundsInterface {
public:
  virtual void bubbleTouchedBubble(float power, bool rubber) = 0;
  virtual void bubbleTouchedGround(float power, bool rubber) = 0;
  virtual void bubbleExploded() = 0;
  virtual void bubbleDisappeared() = 0;
  virtual void bubbleBecomeSmaller() = 0;
  virtual void aloneBubbleClicked(BubbleParameters *ball) = 0;
};

enum Bonus {
  ShufflerBonus,
  SplitterBonus,
  EarthquakeBonus,
  ExploserBonus,
  BubbleRainBonus,
  CreatorBonus,
  BonusesNumber
};

class ViewInterface {
public:
  virtual void ballCreated(BubbleParameters *ball) = 0;
  virtual void ballDestroyed(BubbleParameters *ball) = 0;
  enum BallUpdatedParameter {
    Color,
    Availability
  };
  virtual void ballUpdated(BubbleParameters *ball, 
                           BallUpdatedParameter param) = 0;
  // Spark means contact between two balls
  virtual void sparkCreated(Spark spark) = 0;
  virtual void sparkDestroyed(Spark spark) = 0;

  enum InfoMessage {
    BlackBubbleAppear = 0,
    WhiteBubbleAppear = 1,
    RubberBubbleAppear = 2,
    ExplodingBubbleAppear = 3,
    GradientBubbleAppear = 4,
    RedEyedBubbleAppear = 5,
    MagneticBubbleAppear = 6,

    BubbleAppearFinish = 99,
    FirstInfo = 100,
    BlackAndWhiteBubbles = 101,
    ClassicGameMod = 102,
    InsaneGameMod = 103
  };
  virtual void newInfoMessage(InfoMessage type) = 0;

  virtual void bonusAppeared(Bonus type) = 0;
  virtual void bonusNumberUpdated(bool *bonuses_number) = 0;
  virtual void leftTimeChanged(float time_left) = 0;
  virtual void leftBubblesChanged(int left_bubbles) = 0;

  virtual void worldChanged() = 0;
  virtual void gameFinished(int balls_left, int previous_stars_count) = 0;

  virtual void levelChanged(int new_group, int new_level) = 0;
  virtual void gameResumed() = 0;
};

#endif // BALLVIEWINTERFACE_H
