#ifndef BUBBLE_SPARKS_H
#define BUBBLE_SPARKS_H

#include "cocos2d.h"

using namespace cocos2d;

class BubbleSparks : public CCParticleSystemQuad
{
public:
  BubbleSparks(ccColor4F color, float start_size, bool exploding);
};

#endif  // BUBBLE_SPARKS_H