#ifndef MENU_INTERFACE_H
#define MENU_INTERFACE_H

#include "cocos2d.h"

using namespace cocos2d;

class MenuInterface : public CCLayer
{
public:
  // if 
  MenuInterface(CCNode *parent = NULL);
  class InternalObserver {
  public:
    virtual void quitMenu() = 0;
  };
protected:
  void setHeaderText(const char * text);
  void setInternalObserver(InternalObserver *internal_observer_) {
    internal_observer = internal_observer_;
  }
  float foreground_width, foreground_height;
  CCLayer *content;
private:
  CCSprite *foreground;
  friend class AppDelegate;

  virtual void keyBackClicked();
  void quitCallback(cocos2d::CCObject* );
  InternalObserver *internal_observer;
  CCLabelTTF* header_label;
};

#endif // MENU_INTERFACE_H