#!/bin/bash

sizes=(500 350 250 177 125 88 60 40)
name=`echo "$1" | cut -d'.' -f1`

for size in ${sizes[*]}
do
	percent=$(awk -v size=$size 'BEGIN { print size * 100 / 500 }')
	fullname=$name$size.png;
	echo "Processing $fullname"
	convert $1 -define png:include-chunk=none,trns,gama -define png:format=png32 \
		-channel rgba -resize $percent% $fullname
done

