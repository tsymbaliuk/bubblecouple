package org.cocos2dx.bubblecouple;

import java.lang.ref.WeakReference;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxGLSurfaceView;

import android.os.Handler;
import android.os.Message;

import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyNotifier;
import com.tapjoy.TapjoyViewNotifier;



public class TapjoyHelper
{
    protected static final int TAPJOY_HELPER_SHOW_OFFERWALL = 6;


    protected static Handler mHandler;

    public TapjoyHelper(Cocos2dxActivity activity)
    {
        // Init JNI handlers
        initJNI(new WeakReference<TapjoyHelper>(this));
        mHandler = new Handler()
        {
            public void handleMessage(Message msg)
            {
                switch (msg.what)
                {
                case TAPJOY_HELPER_SHOW_OFFERWALL:
                    {
                        TapjoyHelper.this.showOfferwall();
                    }
                    break;
                }
                super.handleMessage(msg);
            }
        };
    }


    public void showOfferwall()
    {
		TapjoyConnect.getTapjoyConnectInstance().showOffers();
		TapjoyConnect.getTapjoyConnectInstance().setTapjoyViewNotifier( new TapjoyViewNotifier() {
			@Override
			public void viewDidClose(int arg0) {}
			@Override
			public void viewDidOpen(int arg0) {}
			@Override
			public void viewWillClose(int arg0) {
				Cocos2dxGLSurfaceView.getInstance().queueEvent(new Runnable() {
                    public void run() {
                    	updateTapjoyStarsProcessStarted();
                    }
                 });
				TapjoyConnect.getTapjoyConnectInstance().getTapPoints( new TapjoyNotifier() {
					@Override
					public void getUpdatePointsFailed(String arg0) {
						Cocos2dxGLSurfaceView.getInstance().queueEvent(new Runnable() {
		                    public void run() {
								updateTapjoyStarsProcessFailed();
		                    }
		                 });
					}
					@Override
					public void getUpdatePoints(String arg0, int arg1) {
						final int member = arg1;
						Cocos2dxGLSurfaceView.getInstance().queueEvent(new Runnable() {
		                    public void run() {
								updateTapjoyStarsNumber(member);
		                    }
		                 });
					}
				});
			}
			@Override
			public void viewWillOpen(int arg0) {}
		} );
    }

    private native void initJNI(Object wadk_this);
    private native void updateTapjoyStarsNumber(int new_amount);
    private native void updateTapjoyStarsProcessFailed();
    private native void updateTapjoyStarsProcessStarted();

    private static void nativeShowOfferwall()
    {
        Message msg = new Message();
        msg.what = TAPJOY_HELPER_SHOW_OFFERWALL;
        mHandler.sendMessage(msg);
    }
}