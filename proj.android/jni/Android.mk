LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := game_shared

LOCAL_CFLAGS := -DCOCOS

LOCAL_MODULE_FILENAME := libgame

LOCAL_SRC_FILES := 	hellocpp/main.cpp \
					../../Classes/AppDelegate.cpp \
					../../Classes/Offerwall.cpp \
					../../Classes/model/main_model.cpp \
					../../Classes/model/score_manager.cpp \
					../../Classes/model/level_manager.cpp \
					../../Classes/model/ground.cpp \
					../../Classes/model/bubbles.cpp \
					../../Classes/model/bonus_manager.cpp \
					../../Classes/model/game_progress.cpp \
					../../Classes/model/bubble_parameters.cpp \
					../../Classes/pugixml/pugixml.cpp \
					../../Classes/view/bubble_sparks.cpp \
					../../Classes/view/game_view.cpp \
					../../Classes/view/main_menu.cpp \
					../../Classes/view/menu_interface.cpp \
					../../Classes/view/levels_menu.cpp \
					../../Classes/view/settings_menu.cpp \
					../../Classes/view/info_menu.cpp \
					../../Classes/view/game_menu.cpp \
					../../Classes/view/translator.cpp \
					../../Classes/view/bubble.cpp \
					../../Classes/view/colors.cpp \
					../../Classes/view/comics_scene.cpp \
					../../Classes/view/bubble_animations.cpp \
					../../Classes/view/resource_loader.cpp \
					../../Classes/view/view_settings.cpp \
					../../Classes/view/global_info.cpp \
					../../Classes/view/sounds_manager.cpp
                   
LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes                   

LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static
            
include $(BUILD_SHARED_LIBRARY)

$(call import-module,CocosDenshion/android) \
$(call import-module,cocos2dx) \
$(call import-module,extensions)
